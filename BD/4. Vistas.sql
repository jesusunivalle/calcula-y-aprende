-- Consulte el nombre, tipo de usuario y apellido 
-- del tipo de usuario igual a 1
CREATE VIEW nombretipousuario AS
SELECT usuario.tipo_usu,usuario.nombre,usuario.apellido
FROM usuario
Where usuario.tipo_usu=1;
SELECT * FROM nombretipousuario;
/*
consulte los nombre y las respuestas correctas de los usuarios que 
tengan codigo de plan igual a 2711
*/
CREATE VIEW puntuacion2711_ AS
SELECT usuario.nombre,puntuacion.respu_corre
FROM puntuacion,usuario
WHERE usuario.correo=puntuacion.correo_usu
AND usuario.cod_plan=2711;
SELECT * FROM  puntuacion2711_;

