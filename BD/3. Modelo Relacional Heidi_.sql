-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para relacional
CREATE DATABASE IF NOT EXISTS `relacional` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `relacional`;

-- Volcando estructura para tabla relacional.categoria_pregunta
CREATE TABLE IF NOT EXISTS `categoria_pregunta` (
  `Codigo_categoria` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Codigo_categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla relacional.categoria_pregunta: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria_pregunta` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria_pregunta` ENABLE KEYS */;

-- Volcando estructura para tabla relacional.jugador
CREATE TABLE IF NOT EXISTS `jugador` (
  `Cod_Jugador` int(11) NOT NULL,
  `Nombre_Jugador` varchar(50) DEFAULT '',
  `Rango_edad` varchar(50) DEFAULT '',
  `Puntuación_Jugador` varchar(50) DEFAULT '',
  `horaint` varchar(50) DEFAULT NULL,
  `horafin` varchar(50) DEFAULT NULL,
  `FK_cod_categoria` int(11) DEFAULT NULL,
  `FK_codigo_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`Cod_Jugador`),
  KEY `FK_jugador_categoria_pregunta` (`FK_cod_categoria`),
  KEY `FK_jugador_usuario` (`FK_codigo_usuario`),
  CONSTRAINT `FK_jugador_categoria_pregunta` FOREIGN KEY (`FK_cod_categoria`) REFERENCES `categoria_pregunta` (`Codigo_categoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jugador_usuario` FOREIGN KEY (`FK_codigo_usuario`) REFERENCES `usuario` (`Codigo_Usu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla relacional.jugador: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `jugador` DISABLE KEYS */;
/*!40000 ALTER TABLE `jugador` ENABLE KEYS */;

-- Volcando estructura para tabla relacional.jugador_puntuaciones
CREATE TABLE IF NOT EXISTS `jugador_puntuaciones` (
  `FK_Codigo_Puntuación` int(11) NOT NULL,
  `FK_Cod_Jugador` int(11) NOT NULL,
  PRIMARY KEY (`FK_Codigo_Puntuación`,`FK_Cod_Jugador`),
  KEY `FK_Codigo_Puntuación_FK_Cod_Jugador` (`FK_Codigo_Puntuación`,`FK_Cod_Jugador`),
  KEY `FK_jugador_puntuaciones_jugador` (`FK_Cod_Jugador`),
  CONSTRAINT `FK_jugador_puntuaciones_jugador` FOREIGN KEY (`FK_Cod_Jugador`) REFERENCES `jugador` (`Cod_Jugador`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jugador_puntuaciones_puntuaciones` FOREIGN KEY (`FK_Codigo_Puntuación`) REFERENCES `puntuaciones` (`Codigo_Puntuaciones`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla relacional.jugador_puntuaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `jugador_puntuaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `jugador_puntuaciones` ENABLE KEYS */;

-- Volcando estructura para tabla relacional.preguntas
CREATE TABLE IF NOT EXISTS `preguntas` (
  `Codigo_Preguntas` int(11) NOT NULL,
  `Clasificación` varchar(50) DEFAULT '',
  `Explicación` varchar(50) DEFAULT '',
  `Modificar_Pregunta` varchar(50) DEFAULT '',
  `Expliacación_Opcional` varchar(50) DEFAULT '',
  `Puntuación_Preguntas` varchar(50) DEFAULT '',
  `FK_Cod_Categoria` int(11) DEFAULT NULL,
  PRIMARY KEY (`Codigo_Preguntas`),
  KEY `FK_preguntas_categoria_pregunta` (`FK_Cod_Categoria`),
  CONSTRAINT `FK_preguntas_categoria_pregunta` FOREIGN KEY (`FK_Cod_Categoria`) REFERENCES `categoria_pregunta` (`Codigo_categoria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla relacional.preguntas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `preguntas` DISABLE KEYS */;
/*!40000 ALTER TABLE `preguntas` ENABLE KEYS */;

-- Volcando estructura para tabla relacional.puntuaciones
CREATE TABLE IF NOT EXISTS `puntuaciones` (
  `Codigo_Puntuaciones` int(11) NOT NULL,
  `Puntuaciones` int(11) DEFAULT NULL,
  PRIMARY KEY (`Codigo_Puntuaciones`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla relacional.puntuaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `puntuaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `puntuaciones` ENABLE KEYS */;

-- Volcando estructura para tabla relacional.respuesta
CREATE TABLE IF NOT EXISTS `respuesta` (
  `Cod_Respuesta` int(11) NOT NULL,
  `Respuesta_Buena` varchar(50) DEFAULT NULL,
  `Respesta_Mala` varchar(50) DEFAULT NULL,
  `FK_cod_Preguntas` int(11) DEFAULT NULL,
  PRIMARY KEY (`Cod_Respuesta`),
  KEY `FK_respuesta_preguntas` (`FK_cod_Preguntas`),
  CONSTRAINT `FK_respuesta_preguntas` FOREIGN KEY (`FK_cod_Preguntas`) REFERENCES `preguntas` (`Codigo_Preguntas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla relacional.respuesta: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `respuesta` DISABLE KEYS */;
/*!40000 ALTER TABLE `respuesta` ENABLE KEYS */;

-- Volcando estructura para tabla relacional.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `Codigo_Usu` int(11) NOT NULL,
  `Nombre_usuari` varchar(50) DEFAULT NULL,
  `Cedula_codigo_estudiantil` varchar(50) DEFAULT NULL,
  `Tipo_Usuario` varchar(50) DEFAULT NULL,
  `gmail` varchar(50) DEFAULT NULL,
  `Fecha_estudiante` varchar(50) DEFAULT NULL,
  `horainiseción` varchar(50) DEFAULT NULL,
  `horafinsesión` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Codigo_Usu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla relacional.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
