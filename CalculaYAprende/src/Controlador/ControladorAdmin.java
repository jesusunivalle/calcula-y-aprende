/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Singleton.UtilSingleton;
import Modelo.ModeloAdmi;
import Modelo.ModeloJuego;
import Vista.CategoriasAdmin;
import Vista.CrearAdmin;
import Vista.CrearLogin;
import Vista.CrearModificar;
import Vista.CrearModificarAdmin;
import Vista.CrearPreguntaAdmin;
import Vista.EligeCategoriaCrearAdmin;
import Vista.EligeCategoriaModiAdmin;
import Vista.ExplicaOpcAdmin;
import Vista.InstruccionesAdmin;
import Vista.MenuAdmin;
import Vista.ModificarPreguntaAdmin;
import Vista.NumPreguntaAdmin;
import Vista.PuntuacionAdmin;
import Vista.VisuPreguntasAdmin;
import Vista.Visu_EliAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ControladorAdmin implements ActionListener {

    int puto = 0;
    /////////////////////////////////////////////////////////////
    public CrearAdmin ca;

    public void ControladorCrearAdmin(CrearAdmin ca) {

        this.ca = ca;
        puto = 1;
    }

    /////////////////////////////////////////////////////////////
    public CrearModificarAdmin cma;

    public void ControladorCrearModiAdmin(CrearModificarAdmin cma) {
        this.cma = cma;
        puto = 2;
    }
    /////////////////////////////////////////////////////////////

    public CrearPreguntaAdmin crearpa;

    public void ControladorCrearPregAdmin(CrearPreguntaAdmin crearpa) {
        this.crearpa = crearpa;
        puto = 3;
    }
    /////////////////////////////////////////////////////////////

    public EligeCategoriaCrearAdmin ecca;

    public void ControladorEligeCateCrearAdmin(EligeCategoriaCrearAdmin ecca) {
        this.ecca = ecca;
        puto = 4;

    }
    /////////////////////////////////////////////////////////////

    public EligeCategoriaModiAdmin ecma;

    public void ControladorEligeCateModiAdmin(EligeCategoriaModiAdmin ecma) {
        this.ecma = ecma;
        puto = 5;
    }

    public InstruccionesAdmin inst;

    public void ControladorInstruccionesAdmin(InstruccionesAdmin inst) {
        this.inst = inst;
        puto = 6;

    }

    public MenuAdmin ma;

    public void ControladorMenuAdmin(MenuAdmin ma) {
        this.ma = ma;
        puto = 7;
    }

    public ModificarPreguntaAdmin modipa;

    public void ControladorModiPregAdmin(ModificarPreguntaAdmin modipa) {
        this.modipa = modipa;
        puto = 8;
    }

    public NumPreguntaAdmin npa;

    public void ControladorNumPregAdmin(NumPreguntaAdmin npa) {
        this.npa = npa;
        puto = 9;
    }

    public VisuPreguntasAdmin vpa;

    public void ControladorVisuPreguntasAdmin(VisuPreguntasAdmin vpa) {
        this.vpa = vpa;
        puto = 10;

    }

    public Visu_EliAdmin vea;

    public void ControladorVisu_EliAdmin(Visu_EliAdmin vea) {
        this.vea = vea;
        puto = 11;
    }

    public void actionPerformed(ActionEvent e) {

        if (puto == 1) {
            if (e.getSource().equals(ca.jbSiguiente)) {

                ModeloAdmi ma = new ModeloAdmi();
                ma.ModeloAdmin(ca, ca.tu);

                if (ca.puto == 0) {
                    JOptionPane.showMessageDialog(null, "Administrador creado con exito");
                    ca.setVisible(false);

                    CrearLogin cl = new CrearLogin(ca.tu.cl.cb);
                }

            }
        }

        //////////////////////////////////////////////////////////////////
        if (puto == 2) {
            if (e.getSource().equals(cma.jbAtras)) {
                cma.setVisible(false);
                MenuAdmin ma = new MenuAdmin(cma.ma.cda);
            }
            if (e.getSource().equals(cma.jbCrear)) {
                cma.setVisible(false);
                EligeCategoriaCrearAdmin ecca = new EligeCategoriaCrearAdmin(cma);
            }

            if (e.getSource().equals(cma.jbModificar)) {
                cma.setVisible(false);
                EligeCategoriaModiAdmin ecma = new EligeCategoriaModiAdmin(cma);
            }
        }
        /////////////////////////////////////////////////////////////////////////////////
        if (puto == 3) {
            if (e.getSource().equals(crearpa.jbAtras)) {
                crearpa.setVisible(false);
                EligeCategoriaCrearAdmin ecca = new EligeCategoriaCrearAdmin(crearpa.ecca.cma);
            }

            if (e.getSource().equals(crearpa.jbsiguiente)) {

                ModeloAdmi mcpa = new ModeloAdmi();
                mcpa.ModeloCrearPregAdmin(crearpa);
                if (crearpa.puto == 0) {
                    crearpa.setVisible(false);
                    MenuAdmin ma = new MenuAdmin(crearpa.ecca.cma.ma.cda);
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////
        if (puto == 4) {
            if (e.getSource().equals(ecca.jbatras)) {
                ecca.setVisible(false);
                CrearModificarAdmin cma = new CrearModificarAdmin(ecca.cma.ma);
            }

            if (e.getSource().equals(ecca.jbsiguiente)) {
                ecca.setVisible(false);
                if (ecca.jcTipoUsu.getSelectedItem().equals("1")) {
                    ecca.numCate = 1;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregAdminCrear(ecca);
                    CrearPreguntaAdmin creara = new CrearPreguntaAdmin(ecca);
                }

                if (ecca.jcTipoUsu.getSelectedItem().equals("2")) {
                    ecca.numCate = 2;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregAdminCrear(ecca);
                    CrearPreguntaAdmin creara = new CrearPreguntaAdmin(ecca);
                }

                if (ecca.jcTipoUsu.getSelectedItem().equals("3")) {
                    ecca.numCate = 3;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregAdminCrear(ecca);
                    CrearPreguntaAdmin creara = new CrearPreguntaAdmin(ecca);
                }

            }
        }
        ///////////////////////////////////////////////////////////////
        if (puto == 5) {
            if (e.getSource().equals(ecma.jbatras)) {
                ecma.setVisible(false);
                CrearModificarAdmin cm = new CrearModificarAdmin(ecma.cma.ma);
            }

            if (e.getSource().equals(ecma.jbsiguiente)) {
                ecma.setVisible(false);
                if (ecma.jcTipoUsu.getSelectedItem().equals("1")) {
                    ecma.numCate = 1;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregAdminpreguntas(ecma);
                    VisuPreguntasAdmin vpa = new VisuPreguntasAdmin(ecma);

                }

                if (ecma.jcTipoUsu.getSelectedItem().equals("2")) {
                    ecma.numCate = 2;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregAdminpreguntas(ecma);
                    VisuPreguntasAdmin vpa = new VisuPreguntasAdmin(ecma);
                }

                if (ecma.jcTipoUsu.getSelectedItem().equals("3")) {
                    ecma.numCate = 3;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregAdminpreguntas(ecma);
                    VisuPreguntasAdmin vpa = new VisuPreguntasAdmin(ecma);

                }

            }
        }
        ////////////////////////////////////////////////////////////////
        if (puto == 6) {
            if (e.getSource().equals(inst.jbatras)) {
                inst.setVisible(false);
                MenuAdmin ma = new MenuAdmin(inst.bd);

            }
        }
        ////////////////////////////////////////////////////////////////
        if (puto == 7) {
            if (e.getSource().equals(ma.jbcrearModi)) {
                ma.setVisible(false);
                CrearModificarAdmin cma = new CrearModificarAdmin(ma);
            }

            if (e.getSource().equals(ma.jbcerrarsesion)) {
                ma.setVisible(false);
                UtilSingleton instancia3 = UtilSingleton.getInstance();
                ma.cda.HorafinSesion = instancia3.getDateHour();
                System.out.println("Hora final: " + ma.cda.HorafinSesion);
                ModeloJuego mj = new ModeloJuego();
                mj.ModeloHoraSesion(ma.cda);
                CrearLogin cl = new CrearLogin(ma.cda);
                JOptionPane.showMessageDialog(null, "A cerrado sesion con exito en la marca temporal " + ma.cda.HorafinSesion);
                ma.cda.UsuActual = "";
                ma.cda.usuconectado = "";

            }

            if (e.getSource().equals(ma.jbpuntuAdmin)) {
                ma.setVisible(false);
                PuntuacionAdmin pa = new PuntuacionAdmin(ma);
            }

            if (e.getSource().equals(ma.jbvisuJuga)) {
                ma.setVisible(false);
                Visu_EliAdmin vea = new Visu_EliAdmin(ma);
            }

            if (e.getSource().equals(ma.jbinicioJu)) {
                ma.setVisible(false);
                CategoriasAdmin cja = new CategoriasAdmin(ma);
            }
            if (e.getSource().equals(ma.jbInstrucciones)) {
                ma.setVisible(false);
                InstruccionesAdmin insta = new InstruccionesAdmin(ma.cda);
            }
        }
        ////////////////////////////////////////////////////////////////
        if (puto == 8) {
            if (e.getSource().equals(modipa.jbAtras)) {
                modipa.setVisible(false);
                NumPreguntaAdmin npa = new NumPreguntaAdmin(modipa.npa.vpa);
            }
            if (e.getSource().equals(modipa.jbsiguiente)) {

                ModeloAdmi mmpa = new ModeloAdmi();
                mmpa.ModeloModificarPregAdmin(modipa);
                if (modipa.puto == 0) {
                    modipa.setVisible(false);
                    MenuAdmin ma = new MenuAdmin(modipa.npa.vpa.ecma.cma.ma.cda);
                }
            }
        }
        ////////////////////////////////////////////////////////////////
        if (puto == 9) {
            if (e.getSource().equals(npa.jbatras)) {
                npa.setVisible(false);
                VisuPreguntasAdmin vpa = new VisuPreguntasAdmin(npa.vpa.ecma);
            }

            if (e.getSource().equals(npa.jbsiguiente)) {

                if (npa.jtnumPreg.getText().equals("")) {
                    npa.puto = 1;
                    JOptionPane.showMessageDialog(null, "Porfavor ingrese un numero de pregunta valido");
                } else {
                    npa.Numpreg = (Integer.parseInt(npa.jtnumPreg.getText()));
                    System.out.println("numpreg:" + npa.Numpreg);
                    System.out.println("numpreg:" + npa.vpa.ecma.cma.ma.cda.maxjuego);
                }

                if (npa.Numpreg > npa.vpa.ecma.cma.ma.cda.maxjuego) {
                    JOptionPane.showMessageDialog(null, "Porfavor ingrese un numero de pregunta valido");
                } else {

                    npa.puto = 0;
                    npa.Numpreg = (Integer.parseInt(npa.jtnumPreg.getText()));
                    System.out.println("numpreg:" + npa.Numpreg);
                    npa.setVisible(false);
                    ModificarPreguntaAdmin modipa = new ModificarPreguntaAdmin(npa);

                }

            }
        }
        ///////////////////////////////////////////////////////////////////////////////////
        if (puto == 10) {
            if (e.getSource().equals(vpa.jbatras)) {
                vpa.setVisible(false);
                EligeCategoriaModiAdmin ecma = new EligeCategoriaModiAdmin(vpa.ecma.cma);
            }

            if (e.getSource().equals(vpa.jbsiguiente)) {
                vpa.setVisible(false);
                NumPreguntaAdmin npa = new NumPreguntaAdmin(vpa);
            }

            if (e.getSource().equals(vpa.jbMostrar)) {

                DefaultTableModel modelo = (DefaultTableModel) vpa.tabla.getModel();
                modelo.setRowCount(0);

                ModeloAdmi mvpa = new ModeloAdmi();
                mvpa.ModeloVisuPregAdmin(vpa);

            }
        }
        ////////////////////////////////////////////////////////////////////
        if (puto == 11) {
            if (e.getSource().equals(vea.jbatras)) {
                vea.setVisible(false);
                vea.Mostrar_Eliminar = 0;
                MenuAdmin ma = new MenuAdmin(vea.ma.cda);
            }

            if (e.getSource().equals(vea.jbTodos)) {
                vea.Mostrar_Eliminar = 1;

                DefaultTableModel modelo = (DefaultTableModel) vea.tabla.getModel();
                modelo.setRowCount(0);

                ModeloAdmi mvea = new ModeloAdmi();
                mvea.ModeloVisu_EliAdmin(vea);
            }

            if (e.getSource().equals(vea.jbEliminar)) {
                vea.Mostrar_Eliminar = 2;
                ModeloAdmi mvep = new ModeloAdmi();
                mvep.ModeloVisu_EliAdmin(vea);
            }
        }
        ////////////////////////////////////////////////////////////////////
    }
}
