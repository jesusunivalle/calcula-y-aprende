/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Singleton.UtilSingleton;
import Modelo.ModeloEstu;
import Modelo.ModeloJuego;
import Vista.CategoriasEstu;
import Vista.CrearEstudiante;
import Vista.CrearLogin;
import Vista.ExplicaOpcEstu;
import Vista.InstruccionesEstu;
import Vista.MenuEstudiante;
import Vista.Puntuaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ControladorEstudiante implements ActionListener {

    int puto = 0;

    public CrearEstudiante ce;

    public void ControladorCrearEstudiante(CrearEstudiante ce) {
        this.ce = ce;
        puto = 1;
    }

    public InstruccionesEstu inst;

    public void ControladorInstruccionesEstu(InstruccionesEstu inst) {
        this.inst = inst;
        puto = 2;

    }

    MenuEstudiante mp;

    public void ControladorMenuEstudiante(MenuEstudiante mp) {

        this.mp = mp;
        puto = 3;
    }

    public void actionPerformed(ActionEvent e) {

        if (puto == 1) {
            if (e.getSource().equals(ce.jbSiguiente)) {
                ModeloEstu me = new ModeloEstu();
                me.ModeloEstudiante(ce, ce.tu);

                if (ce.puto == 0) {
                    JOptionPane.showMessageDialog(null, "Estudiante creado con exito");

                    ce.setVisible(false);

                    CrearLogin cl = new CrearLogin(ce.tu.cl.cb);

                }

            }
        }
        /////////////////////////////////////////////////////////
        if (puto == 2) {
            if (e.getSource().equals(inst.jbatras)) {
                inst.setVisible(false);
                MenuEstudiante me = new MenuEstudiante(inst.bd);
            }
        }
        //////////////////////////////////////////
        if (puto == 3) {
            if (e.getSource().equals(mp.jbIniJuego)) {
                mp.setVisible(false);
                CategoriasEstu cj = new CategoriasEstu(mp);
            }

            if (e.getSource().equals(mp.jbPuntuacion)) {
                mp.setVisible(false);
                Puntuaciones p = new Puntuaciones(mp);
            }

            if (e.getSource().equals(mp.jbcerrarSc)) {
                mp.setVisible(false);
                UtilSingleton instancia3 = UtilSingleton.getInstance();
                mp.bd.HorafinSesion = instancia3.getDateHour();
                System.out.println("Hora final: " + mp.bd.HorafinSesion);
                ModeloJuego mj = new ModeloJuego();
                mj.ModeloHoraSesion(mp.bd);
                CrearLogin cl = new CrearLogin(mp.bd);
                JOptionPane.showMessageDialog(null, "A cerrado sesion con exito en la marca temporal " + mp.bd.HorafinSesion);
                mp.bd.UsuActual = "";
                mp.bd.usuconectado = "";

            }
            if (e.getSource().equals(mp.jbInstrucciones)) {
                mp.setVisible(false);
                InstruccionesEstu inst = new InstruccionesEstu(mp.bd);

            }
        }

    }

}
