/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Singleton.UtilSingleton;
import Modelo.ModeloJuego;
import Vista.CategoriasEstu;
import Vista.CategoriasAdmin;
import Vista.CrearAdmin;
import Vista.CrearBD;
import Vista.CrearEstudiante;
import Vista.CrearLogin;
import Vista.CrearProfesor;
import Vista.ExplicaObli;
import Vista.ExplicaObliAdmin;
import Vista.ExplicaOpcAdmin;
import Vista.ExplicaOpcEstu;
import Vista.InicioJuego;
import Vista.InicioJuegoAdmin;
import Vista.InstruccionesEstu;
import Vista.Logueo;
import Vista.MenuAdmin;
import Vista.MenuEstudiante;
import Vista.MenuProfesor;
import Vista.PuntajeFinal;
import Vista.PuntajeFinalAdmin;
import Vista.PuntuacionAdmin;
import Vista.PuntuacionProfesor;
import Vista.Puntuaciones;
import Vista.RespuCorre;
import Vista.RespuCorreAdmin;
import Vista.TipUsu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ControladorJuego implements ActionListener {

    public int puto = 0;

//////////////////////////////////////////////////////////////////////////7
    CrearBD cb;

    public void ControladorCrearBD(CrearBD cb) {
        puto = 1;
        this.cb = cb;

    }
////////////////////////////////////////////////////////////////////////
    CategoriasEstu c;

    public void ControladorCategoriasEstu(CategoriasEstu c) {

        this.c = c;
        puto = 2;
    }
    ////////////////////////////////////////////////////////////////////////
    CategoriasAdmin ca;

    public void ControladorCategoriasAdmin(CategoriasAdmin ca) {

        this.ca = ca;
        puto = 3;
    }
    ////////////////////////////////////////////////////////////////////////

    CrearLogin cl;

    public void ControladorCrearLogin(CrearLogin cl) {

        this.cl = cl;
        puto = 4;

    }
    ////////////////////////////////////////////////////////////////////////

    ExplicaObliAdmin eoba;
    InicioJuegoAdmin ija;
    CrearBD bd;

    public void ControladorExplicaObliAdmin(ExplicaObliAdmin eoba, InicioJuegoAdmin ija, CrearBD bd) {

        this.ija = ija;
        this.eoba = eoba;
        this.bd = bd;
        puto = 5;

        bd.numPreg += 1;

        System.out.println("ContCorre1:" + bd.resCorre);
        System.out.println("ContInCorre1:" + bd.resIncorre);

        if (bd.resIncorre == 1) {
            bd.puntuTempIncorre = 500;
        }

        if (bd.resIncorre == 2) {
            bd.puntuTempIncorre = 1000;
        }

        if (bd.resIncorre == 3) {
            bd.puntuTempIncorre = 1500;
        }

        if (bd.resIncorre == 4) {
            bd.puntuTempIncorre = 2000;
        }

        if (bd.resIncorre == 5) {
            bd.puntuTempIncorre = 2500;
        }

        if (bd.resIncorre == 6) {
            bd.puntuTempIncorre = 3000;
        }

        if (bd.resIncorre == 7) {
            bd.puntuTempIncorre = 3500;
        }

        if (bd.resIncorre == 8) {
            bd.puntuTempIncorre = 4000;
        }

        if (bd.resIncorre == 9) {
            bd.puntuTempIncorre = 4500;
        }

        if (bd.resIncorre == 10) {
            bd.puntuTempIncorre = 5000;
        }

        bd.puntuTemp = bd.puntuTempCorre + bd.puntuTempIncorre;
    }
    ////////////////////////////////////////////////////////////////////////

    ExplicaObli eob;
    InicioJuego ij;

    public void ControladorExplicaObliEstu(ExplicaObli eob, InicioJuego ij, CrearBD bd) {

        this.ij = ij;
        this.eob = eob;
        this.bd = bd;
        puto = 6;

        bd.numPreg += 1;

        if (bd.resIncorre == 1) {
            bd.puntuTempIncorre = 500;
        }

        if (bd.resIncorre == 2) {
            bd.puntuTempIncorre = 1000;
        }

        if (bd.resIncorre == 3) {
            bd.puntuTempIncorre = 1500;
        }

        if (bd.resIncorre == 4) {
            bd.puntuTempIncorre = 2000;
        }

        if (bd.resIncorre == 5) {
            bd.puntuTempIncorre = 2500;
        }

        if (bd.resIncorre == 6) {
            bd.puntuTempIncorre = 3000;
        }

        if (bd.resIncorre == 7) {
            bd.puntuTempIncorre = 3500;
        }

        if (bd.resIncorre == 8) {
            bd.puntuTempIncorre = 4000;
        }

        if (bd.resIncorre == 9) {
            bd.puntuTempIncorre = 4500;
        }

        if (bd.resIncorre == 10) {
            bd.puntuTempIncorre = 5000;
        }

        bd.puntuTemp = bd.puntuTempCorre + bd.puntuTempIncorre;
    }
////////////////////////////////////////////////////////////////////////
    ExplicaOpcAdmin eopca;

    public void ControladorExplicaOpcAdmin(ExplicaOpcAdmin eopca) {
        this.eopca = eopca;
        puto = 7;
    }
    ////////////////////////////////////////////////////////////////////////

    ExplicaOpcEstu eopc;

    public void ControladorExplicaOpcEstu(ExplicaOpcEstu eopc) {
        this.eopc = eopc;
        puto = 8;
    }

    ////////////////////////////////////////////////////////////////////////
    public void ControladorInicioJuego(InicioJuego ij) {

        this.ij = ij;
        puto = 9;

    }
    ////////////////////////////////////////////////////////////////////////

    public void ControladorInicioJuegoAdmin(InicioJuegoAdmin ija) {

        this.ija = ija;
        puto = 10;

    }
    ////////////////////////////////////////////////////////////////////////

    public Logueo log;

    public void ControladorLogueo(Logueo log) {
        this.log = log;
        puto = 11;

    }
    ////////////////////////////////////////////////////////////////////////

    public PuntajeFinal pj;

    public void ControladorPuntajeFinal(PuntajeFinal pj) {
        this.pj = pj;
        puto = 12;
    }
    ////////////////////////////////////////////////////////////////////////

    public PuntajeFinalAdmin pja;

    public void ControladorPuntajeFinalAdmin(PuntajeFinalAdmin pja) {
        this.pja = pja;
        puto = 13;
    }
////////////////////////////////////////////////////////////////////////
    PuntuacionAdmin pa;

    public void ControladorPuntuacionAdmin(PuntuacionAdmin pa) {
        this.pa = pa;
        puto = 14;
    }
////////////////////////////////////////////////////////////////////////
    Puntuaciones pu;

    public void ControladorPuntuacionEstu(Puntuaciones pu) {
        this.pu = pu;
        puto = 15;

    }
////////////////////////////////////////////////////////////////////////
    PuntuacionProfesor pp;

    public void ControladorPuntuacionProfesor(PuntuacionProfesor pp) {
        this.pp = pp;
        puto = 16;
    }
////////////////////////////////////////////////////////////////////////

    RespuCorre rc;

    public void ControladorResCorre(RespuCorre rc, InicioJuego ij, CrearBD bd) {

        this.ij = ij;
        this.rc = rc;
        this.bd = bd;
        puto = 17;

        bd.numPreg += 1;

        if (bd.resCorre == 1) {
            bd.puntuTempCorre = 1000;
        }

        if (bd.resCorre == 2) {
            bd.puntuTempCorre = 2000;
        }

        if (bd.resCorre == 3) {
            bd.puntuTempCorre = 3000;
        }

        if (bd.resCorre == 4) {
            bd.puntuTempCorre = 4000;
        }

        if (bd.resCorre == 5) {
            bd.puntuTempCorre = 5000;
        }

        if (bd.resCorre == 6) {
            bd.puntuTempCorre = 6000;
        }

        if (bd.resCorre == 7) {
            bd.puntuTempCorre = 7000;
        }

        if (bd.resCorre == 8) {
            bd.puntuTempCorre = 8000;
        }

        if (bd.resCorre == 9) {
            bd.puntuTempCorre = 9000;
        }

        if (bd.resCorre == 10) {
            bd.puntuTempCorre = 10000;
        }

        bd.puntuTemp = bd.puntuTempCorre + bd.puntuTempIncorre;

    }

    ////////////////////////////////////////////////////////////////////////
    public RespuCorreAdmin rca;

    public void ControladorResCorreAdmin(RespuCorreAdmin rca, InicioJuegoAdmin ija, CrearBD bd) {

        this.ija = ija;
        this.rca = rca;
        this.bd = bd;
        puto = 18;

        bd.numPreg += 1;

        if (bd.resCorre == 1) {
            bd.puntuTempCorre = 1000;
        }

        if (bd.resCorre == 2) {
            bd.puntuTempCorre = 2000;
        }

        if (bd.resCorre == 3) {
            bd.puntuTempCorre = 3000;
        }

        if (bd.resCorre == 4) {
            bd.puntuTempCorre = 4000;
        }

        if (bd.resCorre == 5) {
            bd.puntuTempCorre = 5000;
        }

        if (bd.resCorre == 6) {
            bd.puntuTempCorre = 6000;
        }

        if (bd.resCorre == 7) {
            bd.puntuTempCorre = 7000;
        }

        if (bd.resCorre == 8) {
            bd.puntuTempCorre = 8000;
        }

        if (bd.resCorre == 9) {
            bd.puntuTempCorre = 9000;
        }

        if (bd.resCorre == 10) {
            bd.puntuTempCorre = 10000;
        }

        bd.puntuTemp = bd.puntuTempCorre + bd.puntuTempIncorre;

    }
////////////////////////////////////////////////////////////////////////
    public TipUsu tu;

    public void ControladorTipUsu(TipUsu tu) {
        this.tu = tu;
        puto = 19;

    }
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    public void actionPerformed(ActionEvent e) {

        if (puto == 1) {
            if (e.getSource().equals(cb.guardarxd)) {
                cb.setVisible(false);
                ModeloJuego mcbd = new ModeloJuego();
                mcbd.ModeloCrearBd(cb);
                CrearLogin cl = new CrearLogin(cb);
            }
        }

////////////////////////////////////////////////////////////////////////
        if (puto == 2) {
            if (e.getSource().equals(c.jbCategoria1)) {
                c.Numcategoria = 1;
                c.setVisible(false);
                InicioJuego ij = new InicioJuego(c);
                UtilSingleton instancia2 = UtilSingleton.getInstance();
                c.mp.bd.HoraIni = instancia2.getDateHour2();
                System.out.println("Hora inicio: " + c.mp.bd.HoraIni);

            }

            if (e.getSource().equals(c.jbCategoria2)) {
                c.Numcategoria = 2;
                c.setVisible(false);
                InicioJuego ij = new InicioJuego(c);
                UtilSingleton instancia2 = UtilSingleton.getInstance();
                c.mp.bd.HoraIni = instancia2.getDateHour2();
                System.out.println("Hora inicio: " + c.mp.bd.HoraIni);
            }

            if (e.getSource().equals(c.jbCategoria3)) {
                c.Numcategoria = 3;
                c.setVisible(false);
                InicioJuego ij = new InicioJuego(c);
                UtilSingleton instancia2 = UtilSingleton.getInstance();
                c.mp.bd.HoraIni = instancia2.getDateHour2();
                System.out.println("Hora inicio: " + c.mp.bd.HoraIni);
            }

            if (e.getSource().equals(c.jbatras)) {
                c.setVisible(false);
                MenuEstudiante cya = new MenuEstudiante(c.mp.bd);
            }
        }
        /////////////////////////////////////////////////////////////////////////////////
        if (puto == 3) {
            if (e.getSource().equals(ca.jbCategoria1)) {
                ca.Numcategoria = 1;
                ca.setVisible(false);
                InicioJuegoAdmin ija = new InicioJuegoAdmin(ca);
                UtilSingleton instancia2 = UtilSingleton.getInstance();
                ca.ma.cda.HoraIni = instancia2.getDateHour2();
            }

            if (e.getSource().equals(ca.jbCategoria2)) {
                ca.Numcategoria = 2;
                ca.setVisible(false);
                InicioJuegoAdmin ija = new InicioJuegoAdmin(ca);
                UtilSingleton instancia2 = UtilSingleton.getInstance();
                ca.ma.cda.HoraIni = instancia2.getDateHour2();
            }

            if (e.getSource().equals(ca.jbCategoria3)) {
                ca.Numcategoria = 3;
                ca.setVisible(false);
                InicioJuegoAdmin ija = new InicioJuegoAdmin(ca);
                UtilSingleton instancia2 = UtilSingleton.getInstance();
                ca.ma.cda.HoraIni = instancia2.getDateHour2();
            }

            if (e.getSource().equals(ca.jbatras)) {
                ca.setVisible(false);
                MenuAdmin ma = new MenuAdmin(ca.ma.cda);
            }
        }
////////////////////////////////////////////////////////////////
        if (puto == 4) {
            if (e.getSource().equals(cl.jbCrearU)) {
                cl.setVisible(false);
                TipUsu tu = new TipUsu(cl);
            }

            if (e.getSource().equals(cl.jbLogin)) {
                cl.setVisible(false);
                Logueo log = new Logueo(cl);
            }

            if (e.getSource().equals(cl.jbSalir)) {
                int res = JOptionPane.showConfirmDialog(cl,
                        "Desea realmente salir de la aplicación?",
                        "Confirmación",
                        JOptionPane.YES_NO_OPTION);

                if (res == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        }
        //////////////////////////////////////////////////////////////////
        if (puto == 5) {
            if (e.getSource().equals(eoba.jbOblisiguiente)) {
                eoba.setVisible(false);

                if (bd.numPreg <= 9) {

                    InicioJuegoAdmin inija = new InicioJuegoAdmin(ija.ca);
                } else {
                    UtilSingleton instancia3 = UtilSingleton.getInstance();
                    bd.Horafin = instancia3.getDateHour2();
                    System.out.println("Hora final: " + bd.Horafin);

                    bd.puntuFi = bd.puntuTempCorre + bd.puntuTempIncorre;
                    PuntajeFinalAdmin pfa = new PuntajeFinalAdmin(ija);

                }

            }
        }
        //////////////////////////////////////////////////////////////////////////
        if (puto == 6) {
            if (e.getSource().equals(eob.jbOblisiguiente)) {
                eob.setVisible(false);
                if (bd.numPreg <= 9) {

                    InicioJuego inij = new InicioJuego(ij.c);

                } else {
                    UtilSingleton instancia3 = UtilSingleton.getInstance();
                    bd.Horafin = instancia3.getDateHour2();
                    System.out.println("Hora final: " + bd.Horafin);

                    bd.puntuFi = bd.puntuTempCorre + bd.puntuTempIncorre;
                    PuntajeFinal pf = new PuntajeFinal(ij);

                }

            }
        }
        /////////////////////////////////////////////////////////////////
        if (puto == 7) {
            if (e.getSource().equals(eopca.jbOpcOpc)) {
                eopca.setVisible(false);
            }
        }
        /////////////////////////////////////////////////////////////////
        if (puto == 8) {
            if (e.getSource().equals(eopc.jbOpcOpc)) {
                eopc.setVisible(false);
            }
        }
        ////////////////////////////////////////////////////////////////////////////

        if (puto == 9) {
            ModeloJuego mij = new ModeloJuego();
            mij.ModeloIniJuego(ij);

            if (e.getSource().equals(ij.jbOpcA)) {

                if (ij.c.mp.bd.SopcionA.equals(ij.c.mp.bd.Srespuesta)) {
                    ij.c.mp.bd.Respuesta = 1;

                    ij.c.mp.bd.resCorre = ij.c.mp.bd.resCorre + 1;

                    ij.setVisible(false);

                    RespuCorre rc = new RespuCorre(ij);
                } else {
                    ij.c.mp.bd.resIncorre = ij.c.mp.bd.resIncorre + 1;
                    ij.setVisible(false);
                    ExplicaObli eo = new ExplicaObli(ij);
                }
            }

            if (e.getSource().equals(ij.jbOpcB)) {

                if (ij.c.mp.bd.SopcionB.equals(ij.c.mp.bd.Srespuesta)) {
                    ij.c.mp.bd.Respuesta = 2;

                    ij.c.mp.bd.resCorre = ij.c.mp.bd.resCorre + 1;

                    ij.setVisible(false);
                    RespuCorre rc = new RespuCorre(ij);

                } else {
                    ij.c.mp.bd.resIncorre = ij.c.mp.bd.resIncorre + 1;
                    ij.setVisible(false);
                    ExplicaObli eo = new ExplicaObli(ij);
                }
            }

            if (e.getSource().equals(ij.jbOpcC)) {

                if (ij.c.mp.bd.SopcionC.equals(ij.c.mp.bd.Srespuesta)) {
                    ij.c.mp.bd.Respuesta = 3;

                    ij.c.mp.bd.resCorre = ij.c.mp.bd.resCorre + 1;

                    ij.setVisible(false);
                    RespuCorre rc = new RespuCorre(ij);

                } else {
                    ij.c.mp.bd.resIncorre = ij.c.mp.bd.resIncorre + 1;
                    ij.setVisible(false);
                    ExplicaObli eo = new ExplicaObli(ij);
                }
            }

            if (e.getSource().equals(ij.jbOpcD)) {

                if (ij.c.mp.bd.SopcionD.equals(ij.c.mp.bd.Srespuesta)) {
                    ij.c.mp.bd.Respuesta = 4;

                    ij.c.mp.bd.resCorre = ij.c.mp.bd.resCorre + 1;

                    ij.setVisible(false);

                    RespuCorre rc = new RespuCorre(ij);

                } else {
                    ij.c.mp.bd.resIncorre = ij.c.mp.bd.resIncorre + 1;
                    ij.setVisible(false);
                    ExplicaObli eo = new ExplicaObli(ij);
                }
            }

            if (e.getSource().equals(ij.jbAyuda)) {

                ExplicaOpcEstu eop = new ExplicaOpcEstu(ij);

            }

        }
        ////////////////////////////////////////////////////////////////////////////
        if (puto == 10) {
            ModeloJuego mija = new ModeloJuego();
            mija.ModeloIniJuegoAdmin(ija);

            if (e.getSource().equals(ija.jbOpcA)) {

                if (ija.ca.ma.cda.SopcionA.equals(ija.ca.ma.cda.Srespuesta)) {
                    ija.ca.ma.cda.Respuesta = 1;

                    ija.ca.ma.cda.resCorre = ija.ca.ma.cda.resCorre + 1;

                    ija.setVisible(false);

                    RespuCorreAdmin rc = new RespuCorreAdmin(ija);

                } else {
                    ija.ca.ma.cda.resIncorre = ija.ca.ma.cda.resIncorre + 1;
                    ija.setVisible(false);
                    ExplicaObliAdmin eoa = new ExplicaObliAdmin(ija);
                }
            }

            if (e.getSource().equals(ija.jbOpcB)) {

                if (ija.ca.ma.cda.SopcionB.equals(ija.ca.ma.cda.Srespuesta)) {
                    ija.ca.ma.cda.Respuesta = 2;

                    ija.ca.ma.cda.resCorre = ija.ca.ma.cda.resCorre + 1;

                    ija.setVisible(false);
                    RespuCorreAdmin rca = new RespuCorreAdmin(ija);

                } else {
                    ija.ca.ma.cda.resIncorre = ija.ca.ma.cda.resIncorre + 1;
                    ija.setVisible(false);
                    ExplicaObliAdmin eoa = new ExplicaObliAdmin(ija);
                }
            }

            if (e.getSource().equals(ija.jbOpcC)) {

                if (ija.ca.ma.cda.SopcionC.equals(ija.ca.ma.cda.Srespuesta)) {
                    ija.ca.ma.cda.Respuesta = 3;

                    ija.ca.ma.cda.resCorre = ija.ca.ma.cda.resCorre + 1;

                    ija.setVisible(false);
                    RespuCorreAdmin rca = new RespuCorreAdmin(ija);

                } else {
                    ija.ca.ma.cda.resIncorre = ija.ca.ma.cda.resIncorre + 1;
                    ija.setVisible(false);
                    ExplicaObliAdmin eoa = new ExplicaObliAdmin(ija);
                }
            }

            if (e.getSource().equals(ija.jbOpcD)) {

                if (ija.ca.ma.cda.SopcionD.equals(ija.ca.ma.cda.Srespuesta)) {
                    ija.ca.ma.cda.Respuesta = 4;

                    ija.ca.ma.cda.resCorre = ija.ca.ma.cda.resCorre + 1;

                    ija.setVisible(false);

                    RespuCorreAdmin rca = new RespuCorreAdmin(ija);

                } else {
                    ija.ca.ma.cda.resIncorre = ija.ca.ma.cda.resIncorre + 1;
                    ija.setVisible(false);
                    ExplicaObliAdmin eoa = new ExplicaObliAdmin(ija);
                }
            }

            if (e.getSource().equals(ija.jbAyuda)) {

                ExplicaOpcAdmin eop = new ExplicaOpcAdmin(ija);

            }

        }
        ////////////////////////////////////////////////////////
        if (puto == 11) {
            if (e.getSource().equals(log.jbatras)) {
                log.setVisible(false);
                CrearLogin cl = new CrearLogin(log.cl.cb);
            }

            if (e.getSource().equals(log.jbSiguiente)) {
                UtilSingleton instancia2 = UtilSingleton.getInstance();
                log.cl.cb.HoraIniSesion = instancia2.getDateHour();
                System.out.println("Hora inicio: " + log.cl.cb.HoraIniSesion);
                ModeloJuego mlog = new ModeloJuego();
                mlog.ModeloLogueo(log);

                if (log.nivel == 1) {
                    log.setVisible(false);

                    MenuAdmin ma = new MenuAdmin(log.cl.cb);
                }

                if (log.nivel == 2) {
                    log.setVisible(false);

                    MenuProfesor mp = new MenuProfesor(log.cl.cb);
                }

                if (log.nivel == 3) {
                    log.setVisible(false);

                    MenuEstudiante cya = new MenuEstudiante(log.cl.cb);
                }

                if (log.nivel == 0) {
                    JOptionPane.showMessageDialog(null, "Porfavor ingrese un usuario valido");
                }

            }
        }

        ///////////////////////////////////////////////////////////////
        if (puto == 12) {
            ModeloJuego mpf = new ModeloJuego();
            mpf.ModeloPuntajeFinal(pj);
            if (e.getSource().equals(pj.jbSiguiente)) {
                pj.setVisible(false);
                MenuEstudiante cya = new MenuEstudiante(pj.ij.c.mp.bd);
                pj.ij.c.mp.bd.numPreg = 0;
                pj.ij.c.mp.bd.resCorre = 0;
                pj.ij.c.mp.bd.resIncorre = 0;
                pj.ij.c.mp.bd.puntuFi = 0;
                pj.ij.c.mp.bd.puntuTemp = 0;
                pj.ij.c.mp.bd.puntuTempCorre = 0;
                pj.ij.c.mp.bd.puntuTempIncorre = 0;
                pj.ij.c.mp.bd.HoraIni = "";
                pj.ij.c.mp.bd.Horafin = "";

            }
        }
        /////////////////////////////////////////////////////////////
        if (puto == 13) {
            ModeloJuego mpfa = new ModeloJuego();
            mpfa.ModeloPuntajeFinalAdmin(pja);
            if (e.getSource().equals(pja.jbSiguiente)) {
                pja.setVisible(false);
                MenuAdmin ma = new MenuAdmin(pja.ija.ca.ma.cda);
                pja.ija.ca.ma.cda.numPreg = 0;
                pja.ija.ca.ma.cda.resCorre = 0;
                pja.ija.ca.ma.cda.resIncorre = 0;
                pja.ija.ca.ma.cda.puntuFi = 0;
                pja.ija.ca.ma.cda.puntuTemp = 0;
                pja.ija.ca.ma.cda.puntuTempCorre = 0;
                pja.ija.ca.ma.cda.puntuTempIncorre = 0;
            }
        }
        /////////////////////////////////////////////////////////////////////
        if (puto == 14) {
            if (e.getSource().equals(pa.jbatras)) {
                pa.setVisible(false);
                MenuAdmin ma = new MenuAdmin(pa.ma.cda);
            }

            if (e.getSource().equals(pa.jbCategoria1)) {

                pa.Numcategoria = 1;

                DefaultTableModel modelo = (DefaultTableModel) pa.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mpa = new ModeloJuego();
                mpa.ModeloPuntuaAdmin(pa);
            }

            if (e.getSource().equals(pa.jbCategoria2)) {

                pa.Numcategoria = 2;

                DefaultTableModel modelo = (DefaultTableModel) pa.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mpa = new ModeloJuego();
                mpa.ModeloPuntuaAdmin(pa);
            }

            if (e.getSource().equals(pa.jbCategoria3)) {

                pa.Numcategoria = 3;

                DefaultTableModel modelo = (DefaultTableModel) pa.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mpa = new ModeloJuego();
                mpa.ModeloPuntuaAdmin(pa);
            }

            if (e.getSource().equals(pa.jbTodos)) {

                pa.Numcategoria = 4;

                DefaultTableModel modelo = (DefaultTableModel) pa.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mpa = new ModeloJuego();
                mpa.ModeloPuntuaAdmin(pa);
            }
        }
        //////////////////////////////////////////////////////////////////////
        if (puto == 15) {
            if (e.getSource().equals(pu.jbatras)) {
                pu.setVisible(false);
                MenuEstudiante cya = new MenuEstudiante(pu.mp.bd);
            }

            if (e.getSource().equals(pu.jbCategoria1)) {

                pu.Numcategoria = 1;

                DefaultTableModel modelo = (DefaultTableModel) pu.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mp = new ModeloJuego();
                mp.ModeloPuntuacionEstu(pu);
            }

            if (e.getSource().equals(pu.jbCategoria2)) {

                pu.Numcategoria = 2;

                DefaultTableModel modelo = (DefaultTableModel) pu.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mp = new ModeloJuego();
                mp.ModeloPuntuacionEstu(pu);
            }

            if (e.getSource().equals(pu.jbCategoria3)) {

                pu.Numcategoria = 3;

                DefaultTableModel modelo = (DefaultTableModel) pu.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mp = new ModeloJuego();
                mp.ModeloPuntuacionEstu(pu);
            }
        }
        /////////////////////////////////////////////////////////////////////
        if (puto == 16) {
            if (e.getSource().equals(pp.jbatras)) {
                pp.setVisible(false);
                MenuProfesor mp = new MenuProfesor(pp.menup.cb);
            }

            if (e.getSource().equals(pp.jbCategoria1)) {

                pp.Numcategoria = 1;

                DefaultTableModel modelo = (DefaultTableModel) pp.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mp = new ModeloJuego();
                mp.ModeloPuntuaProfe(pp);
            }

            if (e.getSource().equals(pp.jbCategoria2)) {

                pp.Numcategoria = 2;

                DefaultTableModel modelo = (DefaultTableModel) pp.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mp = new ModeloJuego();
                mp.ModeloPuntuaProfe(pp);
            }

            if (e.getSource().equals(pp.jbCategoria3)) {

                pp.Numcategoria = 3;

                DefaultTableModel modelo = (DefaultTableModel) pp.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mp = new ModeloJuego();
                mp.ModeloPuntuaProfe(pp);
            }

            if (e.getSource().equals(pp.jbTodos)) {

                pp.Numcategoria = 4;

                DefaultTableModel modelo = (DefaultTableModel) pp.tabla.getModel();
                modelo.setRowCount(0);

                ModeloJuego mp = new ModeloJuego();
                mp.ModeloPuntuaProfe(pp);
            }
        }

        /////////////////////////////////////////////////////////////////////
        if (puto == 17) {
            if (e.getSource().equals(rc.jbResCorre)) {
                System.out.println("oblihpta");
                rc.setVisible(false);

                if (bd.numPreg <= 9) {

                    InicioJuego inij = new InicioJuego(ij.c);
                } else {
                    UtilSingleton instancia3 = UtilSingleton.getInstance();
                    bd.Horafin = instancia3.getDateHour();
                    System.out.println("dafin" + bd.Horafin);

                    bd.puntuFi = bd.puntuTempCorre + bd.puntuTempIncorre;
                    PuntajeFinal pf = new PuntajeFinal(ij);
                }

            }
        }
        //////////////////////////////////////////////////////////////////////
        if (puto == 18) {
            if (e.getSource().equals(rca.jbResCorre)) {
                rca.setVisible(false);

                if (bd.numPreg <= 9) {

                    InicioJuegoAdmin inija = new InicioJuegoAdmin(ija.ca);
                } else {
                    bd.puntuFi = bd.puntuTempCorre + bd.puntuTempIncorre;
                    PuntajeFinalAdmin pfa = new PuntajeFinalAdmin(ija);
                }

            }
        }
        /////////////////////////////////////////////////////////////////////
        if (puto == 19) {
            if (e.getSource().equals(tu.jbAtras)) {
                tu.setVisible(false);
                CrearLogin cl = new CrearLogin(tu.cl.cb);
            }

            if (e.getSource().equals(tu.jbSiguiente)) {
                tu.setVisible(false);
                if (tu.jcTipoUser.getSelectedItem().equals("Admin")) {
                    CrearAdmin ca = new CrearAdmin(tu);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Profesor")) {
                    CrearProfesor cp = new CrearProfesor(tu);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Estudiante")) {
                    CrearEstudiante ce = new CrearEstudiante(tu);
                }

            }
        }
        //////////////////////////////////////////////////////////////////////
    }

}
