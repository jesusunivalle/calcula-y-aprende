/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Singleton.UtilSingleton;
import Modelo.ModeloJuego;
import Modelo.ModeloProfe;
import Vista.CrearLogin;
import Vista.CrearModificar;
import Vista.CrearPregunta;
import Vista.CrearProfesor;
import Vista.EligeCategoriaCrearProf;
import Vista.EligeCategoriaModi;
import Vista.InstruccionesProfe;
import Vista.MenuProfesor;
import Vista.ModificarPreguntaProf;
import Vista.NumPreguntaProf;
import Vista.PuntuacionProfesor;
import Vista.VisuPreguntasProf;
import Vista.Visu_EliProfe;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControladorProfesor implements ActionListener {

    int puto = 0;

    //////////////////////////////////////////////////////////
    CrearProfesor cp;

    public void ControladorCrearProfesor(CrearProfesor cp) {
        this.cp = cp;
        puto = 1;

    }
    //////////////////////////////////////////////////////////

    public CrearModificar cm;

    public void ControladorCrearModiProfe(CrearModificar cm) {
        this.cm = cm;
        puto = 2;
    }

    //////////////////////////////////////////////////////////
    public CrearPregunta crearp;

    public void ControladorCrearPregProf(CrearPregunta crearp) {
        this.crearp = crearp;
        puto = 3;
    }

    public EligeCategoriaCrearProf ecc;

    public void ControladorEligeCateCrearProf(EligeCategoriaCrearProf ecc) {
        this.ecc = ecc;
        puto = 4;
    }

    public EligeCategoriaModi ecm;

    public void ControladorEligeCateModiProf(EligeCategoriaModi ecm) {
        this.ecm = ecm;
        puto = 5;
    }

    public InstruccionesProfe inst;

    public void ControladorInstruccionesProfe(InstruccionesProfe inst) {
        this.inst = inst;
        puto = 6;

    }

    public MenuProfesor menup;

    public void ControladorMenuProfesor(MenuProfesor menup) {
        this.menup = menup;
        puto = 7;
    }

    public ModificarPreguntaProf modip;

    public void ControladorModiPregPRo(ModificarPreguntaProf modip) {
        this.modip = modip;
        puto = 8;
    }

    public NumPreguntaProf np;

    public void ControladorNumPregProf(NumPreguntaProf np) {
        this.np = np;
        puto = 9;
    }

    public VisuPreguntasProf vp;

    public void ControladorVisuPreguntasProf(VisuPreguntasProf vp) {
        this.vp = vp;
        puto = 10;

    }

    public Visu_EliProfe vep;

    public void ControladorVisu_EliProfe(Visu_EliProfe vep) {
        this.vep = vep;
        puto = 11;
    }

    public void actionPerformed(ActionEvent e) {
        if (puto == 1) {
            if (e.getSource().equals(cp.jbSiguiente)) {

                ModeloProfe mp = new ModeloProfe();
                mp.ModeloProfesor(cp, cp.tu);

                if (cp.puto == 0) {
                    JOptionPane.showMessageDialog(null, "Profesor creado con exito");

                    cp.setVisible(false);

                    CrearLogin cl = new CrearLogin(cp.tu.cl.cb);
                }

            }
        }
        //////////////////////////////////////////////////////////////////////////////////////
        if (puto == 2) {
            if (e.getSource().equals(cm.jbAtras)) {
                cm.setVisible(false);
                MenuProfesor menup = new MenuProfesor(cm.menup.cb);
            } else if (e.getSource().equals(cm.jbCrear)) {
                cm.setVisible(false);
                EligeCategoriaCrearProf ecc = new EligeCategoriaCrearProf(cm);
            } else if (e.getSource().equals(cm.jbModificar)) {
                cm.setVisible(false);
                EligeCategoriaModi ecm = new EligeCategoriaModi(cm);
            }
        }
        //////////////////////////////////////////////////////////////////
        if (puto == 3) {
            if (e.getSource().equals(crearp.jbAtras)) {
                crearp.setVisible(false);
                EligeCategoriaCrearProf ecc = new EligeCategoriaCrearProf(crearp.ecc.cm);
            }

            if (e.getSource().equals(crearp.jbsiguiente)) {

                ModeloProfe mcpp = new ModeloProfe();
                mcpp.ModeloCrearPregProfe(crearp);
                if (crearp.puto == 0) {
                    crearp.setVisible(false);
                    MenuProfesor menup = new MenuProfesor(crearp.ecc.cm.menup.cb);
                }

            }
        }
        ///////////////////////////////////////////////////////////////////////

        if (puto == 4) {
            if (e.getSource().equals(ecc.jbatras)) {
                ecc.setVisible(false);
                CrearModificar cm = new CrearModificar(ecc.cm.menup);
            }

            if (e.getSource().equals(ecc.jbsiguiente)) {
                ecc.setVisible(false);
                if (ecc.jcTipoUsu.getSelectedItem().equals("1")) {
                    ecc.numCate = 1;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregProfCrear(ecc);
                    CrearPregunta crearp = new CrearPregunta(ecc);
                }

                if (ecc.jcTipoUsu.getSelectedItem().equals("2")) {
                    ecc.numCate = 2;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregProfCrear(ecc);
                    CrearPregunta crearp = new CrearPregunta(ecc);
                }

                if (ecc.jcTipoUsu.getSelectedItem().equals("3")) {
                    ecc.numCate = 3;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregProfCrear(ecc);
                    CrearPregunta crearp = new CrearPregunta(ecc);
                }

            }
        }

        ///////////////////////////////////////////////////////////////////////
        if (puto == 5) {
            if (e.getSource().equals(ecm.jbatras)) {
                ecm.setVisible(false);
                CrearModificar cm = new CrearModificar(ecm.cm.menup);
            }

            if (e.getSource().equals(ecm.jbsiguiente)) {
                ecm.setVisible(false);
                if (ecm.jcTipoUsu.getSelectedItem().equals("1")) {
                    ecm.numCate = 1;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregProfe(ecm);
                    VisuPreguntasProf vp = new VisuPreguntasProf(ecm);

                }

                if (ecm.jcTipoUsu.getSelectedItem().equals("2")) {
                    ecm.numCate = 2;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregProfe(ecm);
                    VisuPreguntasProf vp = new VisuPreguntasProf(ecm);
                }

                if (ecm.jcTipoUsu.getSelectedItem().equals("3")) {
                    ecm.numCate = 3;
                    ModeloJuego mmpp = new ModeloJuego();
                    mmpp.ModeloMaxPregProfe(ecm);
                    VisuPreguntasProf vp = new VisuPreguntasProf(ecm);

                }

            }
        }

        //////////////////////////////////////////////////////
        if (puto == 6) {
            if (e.getSource().equals(inst.jbatras)) {
                inst.setVisible(false);
                MenuProfesor menup = new MenuProfesor(inst.bd);

            }
        }
        /////////////////////////////////////////////////////////////
        if (puto == 7) {
            if (e.getSource().equals(menup.jbvisuEstu)) {
                menup.setVisible(false);
                Visu_EliProfe vep = new Visu_EliProfe(menup);
            }

            if (e.getSource().equals(menup.jbpuntuacion)) {
                menup.setVisible(false);
                PuntuacionProfesor pp = new PuntuacionProfesor(menup);
            }

            if (e.getSource().equals(menup.jbcrearModi)) {
                menup.setVisible(false);
                CrearModificar cm = new CrearModificar(menup);
            }
            if (e.getSource().equals(menup.jbInstrucciones)) {
                menup.setVisible(false);
                InstruccionesProfe instp = new InstruccionesProfe(menup.cb);
            }

            if (e.getSource().equals(menup.jbcerrarSesion)) {
                menup.setVisible(false);
                UtilSingleton instancia3 = UtilSingleton.getInstance();
                menup.cb.HorafinSesion = instancia3.getDateHour();
                System.out.println("Hora final: " + menup.cb.HorafinSesion);
                ModeloJuego mj = new ModeloJuego();
                mj.ModeloHoraSesion(menup.cb);
                CrearLogin cl = new CrearLogin(menup.cb);
                JOptionPane.showMessageDialog(null, "A cerrado sesion con exito en la marca temporal " + menup.cb.HorafinSesion);
                menup.cb.UsuActual = "";
                menup.cb.usuconectado = "";
            }
        }
        ////////////////////////////////////////////////////////
        if (puto == 8) {
            if (e.getSource().equals(modip.jbAtras)) {
                modip.setVisible(false);
                NumPreguntaProf np = new NumPreguntaProf(modip.np.vp);
            }
            if (e.getSource().equals(modip.jbsiguiente)) {

                ModeloProfe mmpp = new ModeloProfe();
                mmpp.ModeloModificarPregPro(modip);
                if (modip.puto == 0) {
                    modip.setVisible(false);
                    MenuProfesor menup = new MenuProfesor(modip.np.vp.ecm.cm.menup.cb);
                }
            }
        }
        ///////////////////////////////////////////////////////////
        if (puto == 9) {
            if (e.getSource().equals(np.jbatras)) {
                np.setVisible(false);
                VisuPreguntasProf vp = new VisuPreguntasProf(np.vp.ecm);
            }

            if (e.getSource().equals(np.jbsiguiente)) {

                if (np.jtnumPreg.getText().equals("")) {
                    np.puto = 1;
                    JOptionPane.showMessageDialog(null, "Porfavor ingrese un numero de pregunta valido");
                } else {
                    np.Numpreg = (Integer.parseInt(np.jtnumPreg.getText()));
                    System.out.println("numpreg:" + np.Numpreg);
                    System.out.println("numpreg:" + np.vp.ecm.cm.menup.cb.maxjuego);
                }

                if (np.Numpreg > np.vp.ecm.cm.menup.cb.maxjuego) {
                    JOptionPane.showMessageDialog(null, "Porfavor ingrese un numero de pregunta valido");
                } else {

                    np.puto = 0;
                    np.Numpreg = (Integer.parseInt(np.jtnumPreg.getText()));
                    System.out.println("numpreg:" + np.Numpreg);
                    np.setVisible(false);
                    ModificarPreguntaProf modip = new ModificarPreguntaProf(np);

                }
            }
        }
        //////////////////////////////////////////////////////////////
        if (puto == 10) {
            if (e.getSource().equals(vp.jbatras)) {
                vp.setVisible(false);
                EligeCategoriaModi ecm = new EligeCategoriaModi(vp.ecm.cm);
            }

            if (e.getSource().equals(vp.jbsiguiente)) {
                vp.setVisible(false);
                NumPreguntaProf np = new NumPreguntaProf(vp);
            }

            if (e.getSource().equals(vp.jbMostrar)) {

                DefaultTableModel modelo = (DefaultTableModel) vp.tabla.getModel();
                modelo.setRowCount(0);

                ModeloProfe mvpp = new ModeloProfe();
                mvpp.ModeloVisuPregPro(vp);

            }
        }
        /////////////////////////////////////////////////////////////////////////////////7
        if (puto == 11) {
            if (e.getSource().equals(vep.jbatras)) {
                vep.setVisible(false);
                vep.Mostrar_Eliminar = 0;
                MenuProfesor mp = new MenuProfesor(vep.menup.cb);
            }

            if (e.getSource().equals(vep.jbTodos)) {
                vep.Mostrar_Eliminar = 1;

                DefaultTableModel modelo = (DefaultTableModel) vep.tabla.getModel();
                modelo.setRowCount(0);

                ModeloProfe mvep = new ModeloProfe();
                mvep.ModeloVisu_EliPro(vep);
            }

            if (e.getSource().equals(vep.jbEliminar)) {
                vep.Mostrar_Eliminar = 2;
                ModeloProfe mvep = new ModeloProfe();
                mvep.ModeloVisu_EliPro(vep);
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////
    }

}
