/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilSingleton {

    private static UtilSingleton instance = null;
    private static int usos;
    public String fecha;

    public UtilSingleton(int usos) {
        //instancia por defecto
        this.usos = usos;
    }

    public static UtilSingleton getInstance() {
        if (instance == null) {
            instance = new UtilSingleton(usos);
        }

        return instance;
    }

    public String getDateHour() {
        usos++;
        Date fechaActual = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss:mm yyyy-MM-dd");
        fecha = formato.format(fechaActual);
        return formato.format(fechaActual);

    }

    public String getDateHour2() {
        usos++;
        Date fechaActual = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss:mm");
        fecha = formato.format(fechaActual);
        return formato.format(fechaActual);

    }

    public int getUsos() {
        return usos;
    }
}
