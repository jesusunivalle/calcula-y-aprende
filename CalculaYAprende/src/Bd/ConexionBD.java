package Bd;

import Vista.CrearBD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConexionBD {

    public static Connection connection;
    public static PreparedStatement statement;

    public CrearBD cb;

    public boolean conectar(CrearBD cb) {
        this.cb = cb;
        try {
            String url = "jdbc:mysql://localhost/" + cb.nombre.getText();
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                    url, "root", "");

            return true;
        } catch (ClassNotFoundException | SQLException e) {
            return false;
        }
    }

    public boolean crearbd() {
        try {
            String url = "jdbc:mysql://localhost/";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(
                    url, "root", "");

            return true;
        } catch (ClassNotFoundException | SQLException e) {
            return false;
        } catch (InstantiationException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void desconectar() {

        try {
            if (statement != null) {
                statement.close();
            }

            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
