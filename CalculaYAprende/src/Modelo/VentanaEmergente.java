/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class VentanaEmergente extends JDialog {

    public VentanaEmergente(JDialog d, boolean modal, String xd) {
        super(d, modal);

        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        JLabel l1 = new JLabel(xd);
        JButton aceptar = new JButton("Aceptar");

        c.add(l1);
        c.add(aceptar);

        aceptar.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();

            }

        }
        );

        setSize(300, 200);
        setLocationRelativeTo(null);
        setVisible(true);

    }

}
