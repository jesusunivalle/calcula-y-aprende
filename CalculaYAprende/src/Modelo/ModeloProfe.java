/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Bd.ConexionBD;
import Vista.CrearPregunta;
import Vista.CrearProfesor;
import Vista.ModificarPreguntaProf;
import Vista.TipUsu;
import Vista.VisuPreguntasProf;
import Vista.Visu_EliProfe;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ModeloProfe {

    public CrearPregunta crearp;
    ConexionBD mn = new ConexionBD();

    public void ModeloCrearPregProfe(CrearPregunta crearp) {

        this.crearp = crearp;

        if (crearp.ecc.numCate == 1) {

            if (!crearp.jtpreg.getText().equals("") && !crearp.jtrespuA.getText().equals("") && !crearp.jtrespuB.getText().equals("")
                    && !crearp.jtrespuC.getText().equals("") && !crearp.jtrespuD.getText().equals("")
                    && !crearp.jtrespuCorre.getText().equals("") && !crearp.jtexplicaOpc.getText().equals("")
                    && !crearp.jtexplicaObli.getText().equals("")) {
                crearp.puto = 0;

                try {

                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sql = "INSERT INTO preguntas(categoria, Nopregunta,pregunta)"
                            + "VALUES       (?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, crearp.jtpreg.getText());

                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ds) {
                    System.out.println("1 dsa " + ds);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "0");
                    mn.statement.setString(4, (crearp.jtrespuA.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcA" + ex);
                }

                try {

                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq2 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq2);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "1");
                    mn.statement.setString(4, (crearp.jtrespuB.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcB" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq3 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq3);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "2");
                    mn.statement.setString(4, (crearp.jtrespuC.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcC" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "3");
                    mn.statement.setString(4, (crearp.jtrespuD.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcD" + ex);
                }

                if (crearp.jtrespuA.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuB.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuC.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuD.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (!crearp.jtrespuA.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuB.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuC.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuD.getText().equals(crearp.jtrespuCorre.getText())) {
                    crearp.puto = 2;
                    JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
                }
            } else {
                new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
                crearp.puto = 1;
            }
        }

        if (crearp.ecc.numCate == 2) {

            if (!crearp.jtpreg.getText().equals("") && !crearp.jtrespuA.getText().equals("") && !crearp.jtrespuB.getText().equals("")
                    && !crearp.jtrespuC.getText().equals("") && !crearp.jtrespuD.getText().equals("")
                    && !crearp.jtrespuCorre.getText().equals("") && !crearp.jtexplicaOpc.getText().equals("")
                    && !crearp.jtexplicaObli.getText().equals("")) {
                crearp.puto = 0;

                try {

                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO preguntas(categoria, Nopregunta,pregunta)"
                            + "VALUES       (?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, crearp.jtpreg.getText());

                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ds) {
                    System.out.println("1 dsa " + ds);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "0");
                    mn.statement.setString(4, (crearp.jtrespuA.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcA" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "1");
                    mn.statement.setString(4, (crearp.jtrespuB.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcB" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "2");
                    mn.statement.setString(4, (crearp.jtrespuC.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcC" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "3");
                    mn.statement.setString(4, (crearp.jtrespuD.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcD" + ex);
                }

                if (crearp.jtrespuA.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuB.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuC.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuD.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (!crearp.jtrespuA.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuB.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuC.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuD.getText().equals(crearp.jtrespuCorre.getText())) {
                    crearp.puto = 2;
                    JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
                }
            } else {
                new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
                crearp.puto = 1;
            }
        }

        if (crearp.ecc.numCate == 3) {

            if (!crearp.jtpreg.getText().equals("") && !crearp.jtrespuA.getText().equals("") && !crearp.jtrespuB.getText().equals("")
                    && !crearp.jtrespuC.getText().equals("") && !crearp.jtrespuD.getText().equals("")
                    && !crearp.jtrespuCorre.getText().equals("") && !crearp.jtexplicaOpc.getText().equals("")
                    && !crearp.jtexplicaObli.getText().equals("")) {
                crearp.puto = 0;

                try {

                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO preguntas(categoria, Nopregunta,pregunta)"
                            + "VALUES       (?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, crearp.jtpreg.getText());

                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ds) {
                    System.out.println("1 dsa " + ds);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "0");
                    mn.statement.setString(4, (crearp.jtrespuA.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcA" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "1");
                    mn.statement.setString(4, (crearp.jtrespuB.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcB" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "2");
                    mn.statement.setString(4, (crearp.jtrespuC.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcC" + ex);
                }

                try {
                    mn.conectar(crearp.ecc.cm.menup.cb);
                    String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sq4);

                    mn.statement.setString(1, "" + crearp.ecc.numCate);
                    mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
                    mn.statement.setString(3, "3");
                    mn.statement.setString(4, (crearp.jtrespuD.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcD" + ex);
                }

                if (crearp.jtrespuA.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuB.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuC.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (crearp.jtrespuD.getText().equals(crearp.jtrespuCorre.getText())) {
                    rescorre();
                }

                if (!crearp.jtrespuA.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuB.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuC.getText().equals(crearp.jtrespuCorre.getText()) && !crearp.jtrespuD.getText().equals(crearp.jtrespuCorre.getText())) {
                    crearp.puto = 2;
                    JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
                }
            } else {
                new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
                crearp.puto = 1;
            }
        }

    }

    private void rescorre() {
        try {
            mn.conectar(crearp.ecc.cm.menup.cb);
            String sq4 = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                    + " VALUES     (?,?,?,?,?,?)";

            mn.statement = mn.connection.prepareStatement(sq4);

            mn.statement.setString(1, "" + crearp.ecc.numCate);
            mn.statement.setString(2, "" + (crearp.ecc.maxjuego + 1));
            mn.statement.setString(3, "4");
            mn.statement.setString(4, (crearp.jtrespuCorre.getText()));
            mn.statement.setString(5, crearp.jtexplicaObli.getText());
            mn.statement.setString(6, crearp.jtexplicaOpc.getText());
            mn.statement.executeUpdate();
            mn.desconectar();
        } catch (Exception ex) {
            System.out.println("opcD" + ex);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    public ModificarPreguntaProf modip;

    public void ModeloModificarPregPro(ModificarPreguntaProf modip) {
        this.modip = modip;

        if (modip.np.vp.ecm.numCate == 1) {
            PonerpregProfe();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
        if (modip.np.vp.ecm.numCate == 2) {
            PonerpregProfe();
        }

        if (modip.np.vp.ecm.numCate == 3) {
            PonerpregProfe();
        }

        if (modip.np.vp.ecm.cm.menup.cb.nombre.getText().equals("")) {

            new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
        }

    }

    private void PonerpregProfe() {

        if (!modip.jtpreg.getText().equals("") && !modip.jtrespuA.getText().equals("") && !modip.jtrespuB.getText().equals("")
                && !modip.jtrespuC.getText().equals("") && !modip.jtrespuD.getText().equals("")
                && !modip.jtrespuCorre.getText().equals("") && !modip.jtexplicaOpc.getText().equals("")
                && !modip.jtexplicaObli.getText().equals("")) {
            modip.puto = 0;

            try {

                mn.conectar(modip.np.vp.ecm.cm.menup.cb);
                String sql = "Update preguntas set pregunta=? WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modip.jtpreg.getText());

                mn.statement.executeUpdate();
                mn.desconectar();

                System.out.println("actu p");
            } catch (Exception ex) {
                System.out.println(ex);
                new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

            }

            //////////////////////////////////////////Opcion A////////////////////////////////////////////////////
            try {
                mn.conectar(modip.np.vp.ecm.cm.menup.cb);
                String sql = "Update respuestas set respuesta=? WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 0 + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modip.jtrespuA.getText());

                mn.statement.executeUpdate();
                mn.desconectar();
                System.out.println("actu A");

            } catch (Exception ex) {
                System.out.println("opcA" + ex);
            }

            //////////////////////////////////////////Opcion B////////////////////////////////////////////////////
            try {
                mn.conectar(modip.np.vp.ecm.cm.menup.cb);
                String sql = "Update respuestas set respuesta=? WHERE Nopregunta LIKE'" + modip.np.Numpreg
                        + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 1 + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modip.jtrespuB.getText());

                mn.statement.executeUpdate();
                mn.desconectar();

                System.out.println("actu b");
            } catch (Exception ex) {
                System.out.println("opcB" + ex);
            }

            //////////////////////////////////////////Opcion C////////////////////////////////////////////////////
            try {
                mn.conectar(modip.np.vp.ecm.cm.menup.cb);
                String sql = "Update respuestas set respuesta=? WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 2 + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modip.jtrespuC.getText());

                mn.statement.executeUpdate();
                mn.desconectar();
                System.out.println("actu c");
            } catch (Exception ex) {
                System.out.println("opcC" + ex);
            }

            //////////////////////////////////////////Opcion D////////////////////////////////////////////////////
            try {
                String url2 = "jdbc:mysql://localhost/" + modip.np.vp.ecm.cm.menup.cb.nombre.getText();

                Class.forName("com.mysql.jdbc.Driver").newInstance();
                Connection c = DriverManager.getConnection(url2, "root", "");
                PreparedStatement s = c.prepareStatement("Update respuestas set respuesta=? WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 3 + "'");

                s.setString(1, modip.jtrespuD.getText());

                s.executeUpdate();
                System.out.println("actu d");
            } catch (Exception ex) {
                System.out.println("opcD" + ex);
            }

            //////////////////////////////////////////Opcion Corre////////////////////////////////////////////////////
            if (modip.jtrespuA.getText().equals(modip.jtrespuCorre.getText())) {
                modip.puto = 0;
                rescorreProfe();
            }

            if (modip.jtrespuB.getText().equals(modip.jtrespuCorre.getText())) {
                modip.puto = 0;
                rescorreProfe();
            }

            if (modip.jtrespuC.getText().equals(modip.jtrespuCorre.getText())) {
                modip.puto = 0;
                rescorreProfe();
            }

            if (modip.jtrespuD.getText().equals(modip.jtrespuCorre.getText())) {
                modip.puto = 0;
                rescorreProfe();
            }

            if (!modip.jtrespuA.getText().equals(modip.jtrespuCorre.getText()) && !modip.jtrespuB.getText().equals(modip.jtrespuCorre.getText()) && !modip.jtrespuC.getText().equals(modip.jtrespuCorre.getText()) && !modip.jtrespuD.getText().equals(modip.jtrespuCorre.getText())) {
                modip.puto = 2;
                JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
            }

        } else {
            new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
            modip.puto = 1;
        }
    }

    private void rescorreProfe() {
        try {
            mn.conectar(modip.np.vp.ecm.cm.menup.cb);
            String sql = "Update respuestas set respuesta=?, explicacion=?,explicacion_opc=? WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 4 + "'";

            mn.statement = mn.connection.prepareStatement(sql);

            mn.statement.setString(1, modip.jtrespuCorre.getText());
            mn.statement.setString(2, modip.jtexplicaObli.getText());
            mn.statement.setString(3, modip.jtexplicaOpc.getText());

            mn.statement.executeUpdate();
            mn.desconectar();
            System.out.println("actu cor");
        } catch (Exception ex) {
            System.out.println("opcCorre" + ex);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    public void ModeloPonerPregPro(ModificarPreguntaProf modip) {
        this.modip = modip;

        if (modip.np.vp.ecm.numCate == 1) {
            Ponerpreg();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
        if (modip.np.vp.ecm.numCate == 2) {
            Ponerpreg();
        }

        if (modip.np.vp.ecm.numCate == 3) {
            Ponerpreg();
        }

        if (modip.np.vp.ecm.cm.menup.cb.nombre.getText().equals("")) {

            new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
        }

    }

    private void Ponerpreg() {

        try {
            modip.jtpreg.setText("");
            modip.jtrespuA.setText("");
            modip.jtrespuB.setText("");
            modip.jtrespuC.setText("");
            modip.jtrespuD.setText("");
            modip.jtrespuCorre.setText("");
            modip.jtexplicaOpc.setText("");
            modip.jtexplicaObli.setText("");

            mn.conectar(modip.np.vp.ecm.cm.menup.cb);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'");

            while (s.next()) {
                modip.jtpreg.setText(s.getObject(3) + "");

            }
            mn.desconectar();
            if (modip.np.vp.ecm.cm.menup.cb.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el codigo1");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

        }
        //////////////////////////////////////////Opcion A////////////////////////////////////////////////////
        try {
            mn.conectar(modip.np.vp.ecm.cm.menup.cb);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 0 + "'");

            while (s.next()) {
                modip.jtrespuA.setText(s.getObject(4) + "");
            }
            mn.desconectar();
        } catch (Exception ex) {
            System.out.println("opcA" + ex);
        }

        //////////////////////////////////////////Opcion B////////////////////////////////////////////////////
        try {
            mn.conectar(modip.np.vp.ecm.cm.menup.cb);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 1 + "'");

            while (s.next()) {
                modip.jtrespuB.setText(s.getObject(4) + "");
            }
            mn.desconectar();
        } catch (Exception ex) {
            System.out.println("opcB" + ex);
        }

        //////////////////////////////////////////Opcion C////////////////////////////////////////////////////
        try {
            mn.conectar(modip.np.vp.ecm.cm.menup.cb);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 2 + "'");

            while (s.next()) {
                modip.jtrespuC.setText(s.getObject(4) + "");
            }
            mn.desconectar();
        } catch (Exception ex) {
            System.out.println("opcC" + ex);
        }

        //////////////////////////////////////////Opcion D////////////////////////////////////////////////////
        try {
            mn.conectar(modip.np.vp.ecm.cm.menup.cb);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 3 + "'");

            while (s.next()) {
                modip.jtrespuD.setText(s.getObject(4) + "");
            }
            mn.desconectar();
        } catch (Exception ex) {
            System.out.println("opcD" + ex);
        }

        //////////////////////////////////////////Opcion Corre////////////////////////////////////////////////////
        try {
            mn.conectar(modip.np.vp.ecm.cm.menup.cb);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modip.np.Numpreg + "'and categoria LIKE '" + modip.np.vp.ecm.numCate + "'and opciones LIKE'" + 4 + "'");

            while (s.next()) {
                modip.jtrespuCorre.setText(s.getObject(4) + "");
                modip.jtexplicaObli.setText(s.getObject(5) + "");
                modip.jtexplicaOpc.setText(s.getObject(6) + "");
            }
            mn.desconectar();
        } catch (Exception ex) {
            System.out.println("opcCorre" + ex);
        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public CrearProfesor cp;
    public TipUsu tu;

    public void ModeloProfesor(CrearProfesor cp, TipUsu tu) {
        this.cp = cp;
        this.tu = tu;

        if (!cp.jtCorreoProfe.getText().equals("") && !cp.jtCodProfe.getText().equals("") && !cp.jtNomProfe.getText().equals("") && !cp.jtApeProfe.getText().equals("")) {
            cp.puto = 0;
            try {
                cp.puto = 0;

                mn.conectar(cp.tu.cl.cb);
                String sql = "INSERT INTO usuario(correo,cod_cedu, nombre,"
                        + "apellido, tipo_usu)"
                        + " VALUES(?,?,?,?,?)";

                mn.statement = mn.connection.prepareStatement(sql);

                cp.e = cp.jtCorreoProfe.getText();
                cp.f = cp.jtCorreoProfe.getText();

                Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                Matcher mather = pattern.matcher(cp.e);

                if (mather.find() == true) {
                    mn.statement.setString(1, cp.f);
                    cp.puto = 0;

                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese un correo valido");
                    cp.puto = 1;
                }

                mn.statement.setString(2, cp.jtCodProfe.getText());
                mn.statement.setString(3, cp.jtNomProfe.getText());
                mn.statement.setString(4, cp.jtApeProfe.getText());

                if (tu.jcTipoUser.getSelectedItem().equals("Admin")) {
                    mn.statement.setString(5, "" + 1);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Profesor")) {
                    mn.statement.setString(5, "" + 2);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Estudiante")) {
                    mn.statement.setString(5, "" + 3);
                }

                mn.statement.executeUpdate();
                mn.desconectar();

            } catch (Exception ex) {
                cp.puto = 1;
                System.out.println(ex);

            }
        } else {
            new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
            cp.puto = 1;
        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public VisuPreguntasProf vp;

    public void ModeloVisuPregPro(VisuPreguntasProf vp) {
        this.vp = vp;

        ////////////////////////////////////////////////////////////////////////////////////////////////////                
        if (vp.ecm.numCate == 1) {
            EscogePreg();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
        if (vp.ecm.numCate == 2) {
            EscogePreg();
        }

        if (vp.ecm.numCate == 3) {
            EscogePreg();
        }

        if (vp.ecm.cm.menup.cb.nombre.getText().equals("")) {

            new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
        }

    }

    private void EscogePreg() {

        //String url = "jdbc:mysql://localhost/" + vp.ecm.cm.menup.cb.nombre.getText();
        try {
            mn.conectar(vp.ecm.cm.menup.cb);

            Statement s = mn.connection.createStatement();
            ResultSet rs = s.executeQuery("select Nopregunta, pregunta from preguntas where categoria = '" + vp.ecm.numCate + "'");

            //aqui añadir denuevo el modelo, con columna etc
            Object fila[] = new Object[2];
            while (rs.next()) {
                for (int i = 0; i < 2; i++) {
                    fila[i] = rs.getObject(i + 1);
                }

                vp.modelo.addRow(fila);
            }

            while (rs.next()) {
                for (int i = 1; i <= 9; i++) //sin el bucle//
                //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                {
                    System.out.println(rs.getObject(i) + " ");
                }

                System.out.println();
            }

        } catch (Exception e) {
            System.out.println("Error " + e);
        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public Visu_EliProfe vep;

    public void ModeloVisu_EliPro(Visu_EliProfe vep) {
        this.vep = vep;

        if (vep.Mostrar_Eliminar == 1) {

            // ubicacion url, user, password, nombre base de datos
            //String url = "jdbc:mysql://localhost/" + vep.menup.cb.nombre.getText();
            try {
                mn.conectar(vep.menup.cb);

                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select correo, cod_cedu, nombre, apellido, cod_plan, nom_plan, fecha_naci from usuario where tipo_usu = '" + 3 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[7];
                while (rs.next()) {
                    for (int i = 0; i < 7; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    vep.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (vep.Mostrar_Eliminar == 2) {

            String k;
            String p = "";

            for (int i = 0; i < vep.modelo.getRowCount(); i++) {

                k = vep.modelo.getValueAt(i, 7) + "";

                if (k.equals("true")) {
                    p = "" + (vep.modelo.getValueAt(i, 0));
                    System.out.println("correo: " + p);
                    vep.modelo.removeRow(i);

                    try {
                        mn.conectar(modip.np.vp.ecm.cm.menup.cb);
                        String sql = "Delete from usuario where correo LIKE '" + p + "'";

                        mn.statement = mn.connection.prepareStatement(sql);

                        System.out.println("casi");
                        mn.statement.executeUpdate();
                        mn.desconectar();
                        System.out.println("borro");
                    } catch (Exception cv) {

                    }
                    i--;
                }
            }

        }

    }

    ////////////////////////////////////////////////////////////////////////////
}
