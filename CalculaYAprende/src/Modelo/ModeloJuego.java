package Modelo;

import Bd.ConexionBD;
import Vista.CrearBD;
import Vista.EligeCategoriaCrearAdmin;
import Vista.EligeCategoriaCrearProf;
import Vista.EligeCategoriaModi;
import Vista.EligeCategoriaModiAdmin;
import Vista.InicioJuego;
import Vista.InicioJuegoAdmin;
import Vista.Logueo;
import Vista.PuntajeFinal;
import Vista.PuntajeFinalAdmin;
import Vista.PuntuacionAdmin;
import Vista.PuntuacionProfesor;
import Vista.Puntuaciones;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ModeloJuego {

    CrearBD cb;
    ConexionBD mn = new ConexionBD();

    public void ModeloCrearBd(CrearBD cb) {
        this.cb = cb;

        if (cb.nombre.getText().equals("")) {
            new VentanaEmergente(null, true, "No ha digitado el nombre");
        } else {

            if (cb.guardar.exists()) {
                cb.guardar.delete();

            }
            try {

                mn.crearbd();
                String sq21 = "Create database " + cb.nombre.getText();
                mn.statement = mn.connection.prepareStatement(sq21);
                mn.statement.executeUpdate();
                mn.desconectar();

                mn.conectar(cb);
                String sql = "CREATE TABLE respuestas(categoria int(1) not null, Nopregunta int(2) not null, opciones int (1) not null, respuesta varchar (50) not null,"
                        + "explicacion varchar (500), explicacion_opc varchar (200),primary key(Nopregunta, categoria, opciones))";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.executeUpdate();

                String sq2 = "CREATE TABLE preguntas(categoria int (1) not null, Nopregunta int (2) not null, pregunta varchar (200) not null, primary key (categoria, Nopregunta))";

                mn.statement = mn.connection.prepareStatement(sq2);
                mn.statement.executeUpdate();

                String sq3 = "CREATE TABLE usuario(correo varchar(50) not null, cod_cedu int(11) not null,tipo_usu int(11) not null, nombre varchar(50) not null, apellido varchar(50)not null,"
                        + "cod_plan int(11), nom_plan varchar(50), fecha_naci date, horainisesion varchar(50) , horafinsesion varchar(50), primary key(correo,cod_cedu), unique key(correo),unique key(cod_cedu))";

                mn.statement = mn.connection.prepareStatement(sq3);

                mn.statement.executeUpdate();

                String sq4 = "CREATE TABLE puntuacion(codigo int(11) not null auto_increment, puntuacion int(11) not null, rango_edad int(1) not null, respu_corre int(11) not null, respu_incorre int(11)not null,"
                        + "correo_usu varchar(50) not null, horaini varchar(50)not null , horafin varchar (50) not null, primary key(codigo), foreign key(correo_usu) references usuario (correo) \n"
                        + "on delete cascade on update cascade)";

                mn.statement = mn.connection.prepareStatement(sq4);

                mn.statement.executeUpdate();

                System.out.println("la cree" + cb.nombre.getText());
                mn.desconectar();

                mn.conectar(cb);
                String sq5 = "INSERT INTO preguntas(categoria, Nopregunta,pregunta)"
                        + "VALUES       (1,1, 'Si tengo 5 peras y me como 2 peras ¿Cuantas peras quedan?'),"
                        + "(1,2, 'Si tengo 10 galletas y me como 3 galletas ¿Cuantas galletas quedan?'),"
                        + "(1,3, 'Si tengo 7 mariquitas y me encuentro 3 mariquitas ¿Cuantas mariquitas tengo?'),"
                        + "(1,4, 'Si tengo 5 flores y me encuentro 3 flores ¿Cuantas flores tengo en total?'),"
                        + "(1,5, 'Si tengo 4 perras y 5 gatos ¿Cuantos animales tengo en total?'),"
                        + "(1,6, 'En mi colegio hay 14 balones de futbol y 15 de baloncesto ¿Cuantos balones hay en total en mi colegio?'),"
                        + "(1,7, 'Si tengo 5 tartas y me como 3 tartas ¿Cuantas tartas me quedan?'),"
                        + "(1,8, 'En un parqueadero hay 10 carros y llegan 12 carros mas ¿Cuantos carros hay en total en el parqueadero?'),"
                        + "(1,9, 'Juanita esta vendiendo rosas, si tiene 12 rosas y le compran 2 rosas ¿Cuantas rosas le quedan a Juanita?'),"
                        + "(1,10, 'Juaquin tiene 5 carros y sus padres le compran 3 Carros ¿Cuantos carros tiene juaquin en total?'),"
                        + "(1,11, 'Andres tiene en la nevera 12 helados y en la semana se ha comido 7 helados ¿Cuantos helados le quedan a Andres?'),"
                        + "(1,12, 'Juan tiene 4 galletas y le regala 3 a pablo ¿Cuantas galletas le quedan a Juan?'),"
                        + "(1,13, 'Si Rosa tiene 9 globos y se le explotan 5 ¿Cuantos globos le quedan a Rosa?'),"
                        + "(1,14, '¿Cuanto es 2 + 5?'),"
                        + "(1,15, '¿Cuanto es 5 - 2?'),"
                        + "(1,16, 'Si tengo dos elefantes y le sumo otros dos elefantes ¿Cuantos elefantes tengo en total?'),"
                        + "(1,17, '¿Que signo es este + ?'),"
                        + "(1,18, 'En palabras ¿Cual es el numero 12?'),"
                        + "(1,19, 'En numeros ¿Cual es el numero dieciseis?'),"
                        + "(1,20, '¿Cuanto es 8 - 3?'),"
                        + "(1,21, 'Si tengo 20 marranos y se llevan 2 marranos ¿Cuantos marranos me quedan en total?'),"
                        + "(1,22, '¿Cuanto es 1 - 1?'),"
                        + "(1,23, 'Si Juanita tiene 5 manzanas y Juaquin 5 peras ¿Cuantas frutas tienen en total?'),"
                        + "(1,24, 'En numeros ¿Cual es el numero tres?'),"
                        + "(1,25, 'Si tengo 5 pedazos de pizza y le regalo 4 a mi familia ¿Cuantos pedazos de pizza me quedan?'),"
                        + "(1,26, '¿Cuanto es 10 - 9?'),"
                        + "(1,27, 'Juan tiene una docena de rosas ¿Cuantas rosas tiene en total?'),"
                        + "(1,28, 'Si Carla tiene 5 gatos y se le pierden 3 gatos ¿Cuantos gatos le quedan en total a Carla?'),"
                        + "(1,29, '¿Cuanto es 2 + 2?'),"
                        + "(1,30, '¿Cuanto  es 4 + 4?'),"
                        //////////////////////////////////////////////////////////////////////////////////////////
                        + "(2,1, '¿Cuántas unidades hay en una docena?'),"
                        + "(2,2, '¿Cuánto es (8x4)+1?'),"
                        + "(2,3, '¿Cuántos lados tiene un hexagono?'),"
                        + "(2,4, 'Si marcos tiene 16 años y su hermano tiene la mitad de la edad de marcos, ¿Cuantos años tendra el hermano de marcos en 4 años?'),"
                        + "(2,5, '¿Cuál es la representación gráfica del número nueve mil treinta y seis?'),"
                        + "(2,6, '¿Qué número resulta si divides 56 entre 7?'),"
                        + "(2,7, '¿Qué cantidad expresa el número romano V?'),"
                        + "(2,8, '¿Cuál es el número anterior a 1000?'),"
                        + "(2,9, '¿Si en una carrera vas de tercer puestos y pasas al segundo, ¿en qué puesto vas?'),"
                        + "(2,10, '¿Para calcular cuánto es un tercio de 3996, ¿qué tienes que hacer?'),"
                        + "(2,11, 'Una división exacta es aquella que:'),"
                        + "(2,12, '¿Cómo se llama el polígono de siete lados?'),"
                        + "(2,13, 'Cuál es el nombre del triángulo que tiene dos lados iguales y uno desigual?'),"
                        + "(2,14, '¿Qué hora es si, tanto la manilla pequeña del reloj como la grande están en las 3?'),"
                        + "(2,15, '¿Cuál es el resultado de multiplicar 7x9?'),"
                        + "(2,16, '¿cuánto es 12 menos 7?'),"
                        + "(2,17, '¿cuánto es 140 entre 10?'),"
                        + "(2,18, '¿Cómo puedes comprobar si has hecho bien una división?'),"
                        + "(2,19, '¿Como se lee este número: 3 DM+ 6UM + 8C?'),"
                        + "(2,20, '¿como se representan cinco séptimos?'),"
                        + "(2,21, '¿Cual es el valor de la potencia de exponente cero?'),"
                        + "(2,22, '¿cuánto es 6x8 menos cuatro?'),"
                        + "(2,23, '¿Cuantos meses tienen 28 dias?'),"
                        + "(2,24, '¿Cuantos dias tiene un año?'),"
                        + "(2,25,'si tienes un billete de 100, dos monedas 40 y otro billete de 200 ¿Cuanto tendrias?'),"
                        + "(2,26, '¿Que numero se aproxima mas a 0?'),"
                        + "(2,27, '¿Cálculo mental rápido: cuánto es 100x49?'),"
                        + "(2,28, 'Si tengo 6 huevos, rompo 2, frito 2 y me como 2 ¿cuantos huevos quedan?'),"
                        + "(2,29, '<html>Pablo tiene 4 canicas, su mamá le compro 6, su primo le presto 2 y su <br> papá le quito 3 ¿Cuantas canicas tiene Pablo?</html>'),"
                        + "(2,30, 'Cuanto es la mitad de 3870?'),"
                        ////////////////////////////////////////////////////////////////////////////////////
                        + "(3,1,'¿Que hay que hacer para introducir un factor bajo radical?'),"
                        + "(3,2,'¿Como se denominan una recta y un plano con ningun punto comun?'),"
                        + "(3,3,'¿Como se llama un diedro cuyas caras son prolongacion una de la otra?'),"
                        + "(3,4,'¿Como se llama el poliedro que al prolongar una cara todas quedan en el mismo espacio?'),"
                        + "(3,5,'¿Cual es el valor de la potencia de exponente cero?'),"
                        + "(3,6,'¿Cual es el valor de la cotangente de 30 grados?'),"
                        + "(3,7,'¿Cuantos radianes tiene un cuadrante?'),"
                        + "(3,8,'¿Cuanto vale el seno de 60 grados?'),"
                        + "(3,9,'¿A que se llama genero de un poligono?'),"
                        + "(3,10,'¿A que es igual la suma de los N primeros numeros impares?'),"
                        + "(3,11,'¿Cuanto vale la cotangente de 90 grados?'),"
                        + "(3,12,'¿Cual es la probabilidad de sacar un número sobre mil en una extracción?'),"
                        + "(3,13,'¿Que da el cociente de una fraccion por su reciproco?'),"
                        + "(3,14,'¿Como se llama la desigualdad entre terminos desconocidos?'),"
                        + "(3,15,'En todo sistema de logaritmos se verifica el mismo valor para el logaritmo de la base ¿Cual es?'),"
                        + "(3,16,'¿Cual es la grafica del trinomio de segundo grado?'),"
                        + "(3,17,'¿Cual es el valor de logaritmo de cero?'),"
                        + "(3,18,'¿Cual es el valor de logaritmo de 10?'),"
                        + "(3,19,'¿Cuanto valen las permutaciones de m elementos?'),"
                        + "(3,20,'Valor del seno de 45 grados'),"
                        + "(3,21,'En las ecuaciones de segundo grado que nombre recibe el binomio: b cuadrado -4ac'),"
                        + "(3,22,'¿Los segmentos de rectas paralelas que distancia conservan?'),"
                        + "(3,23,'¿Como se conoce la seccion normal de un diedro?'),"
                        + "(3,24,'En la ecuacion y= mx + b que nombre recibe b'),"
                        + "(3,25,'¿Que determina una recta y un punto?'),"
                        + "(3,26,'El mcm de 6 y 8'),"
                        + "(3,27,'MCM(16,24y32)'),"
                        + "(3,28,'MCD(16,24)'),"
                        + "(3,29,'MCM(6,9)'),"
                        + "(3,30,'Raiz cubica de 8')";
                mn.statement = mn.connection.prepareStatement(sq5);
                mn.statement.executeUpdate();
                mn.desconectar();
            } catch (Exception ds) {
                System.out.println("1 " + ds);
            }

            try {
                mn.conectar(cb);
                String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                        + "VALUES     (1,1, 0, '3', '',''),"
                        + "(1,1, 1, '8', '',''),"//NO SE LE OLVIDE LA COMA AL FINAL
                        + "(1,1, 2, '2', '',''),"
                        + "(1,1, 3, '4', '',''),"
                        + "(1,1, 4, '3', '5-2=3','Si tenemos 5 peras y le restamos 2'),"
                        + "(1,2, 0, '1', '',''),"
                        + "(1,2, 1, '10', '',''),"
                        + "(1,2, 2, '9', '',''),"
                        + "(1,2, 3, '7', '',''),"
                        + "(1,2, 4, '7', '10-3=7','Tenemos 10 galletas y le restamos 3'),"
                        + "(1,3, 0, '8', '',''),"
                        + "(1,3, 1, '9', '',''),"
                        + "(1,3, 2, '6', '',''),"
                        + "(1,3, 3, '10', '',''),"
                        + "(1,3, 4, '10', '7+3=10','Tenemos 7 mariquitas y le sumamos 3'),"
                        + "(1,4, 0, '8', '',''),"
                        + "(1,4, 1, '4', '',''),"
                        + "(1,4, 2, '3', '',''),"
                        + "(1,4, 3, '2', '',''),"
                        + "(1,4, 4, '8', '5+3=8','Tenemos 5 flores y le sumamos 3'),"
                        + "(1,5, 0, '6', '',''),"
                        + "(1,5, 1, '9', '',''),"
                        + "(1,5, 2, '5', '',''),"
                        + "(1,5, 3, '7', '',''),"
                        + "(1,5, 4, '9', '4+5=9','Debemos de sumar todos los animales'),"
                        + "(1,6, 0, '29', '',''),"
                        + "(1,6, 1, '15', '',''),"
                        + "(1,6, 2, '30', '',''),"
                        + "(1,6, 3, '8', '',''),"
                        + "(1,6, 4, '29', '14+15=29','Debemos de sumar todas las cantidades'),"
                        + "(1,7, 0, '3', '',''),"
                        + "(1,7, 1, '2', '',''),"
                        + "(1,7, 2, '4', '',''),"
                        + "(1,7, 3, '7', '',''),"
                        + "(1,7, 4, '2', '5-3=2','Debemos de restar las cantidades'),"
                        + "(1,8, 0, '12', '',''),"
                        + "(1,8, 1, '10', '',''),"
                        + "(1,8, 2, '22', '',''),"
                        + "(1,8, 3, '15', '',''),"
                        + "(1,8, 4, '22', '10+12=22','Se deben de sumar las cantidades'),"
                        + "(1,9, 0, '8', '',''),"
                        + "(1,9, 1, '7', '',''),"
                        + "(1,9, 2, '9', '',''),"
                        + "(1,9, 3, '10', '',''),"
                        + "(1,9, 4, '10', '12-2=10','Se restan las cantidades'),"
                        + "(1,10, 0, '8', '',''),"
                        + "(1,10, 1, '5', '',''),"
                        + "(1,10, 2, '6', '',''),"
                        + "(1,10, 3, '12', '',''),"
                        + "(1,10, 4, '8', '5+3=8','Se suman las cantidades'),"
                        + "(1,11, 0, '0', '',''),"
                        + "(1,11, 1, '5', '',''),"
                        + "(1,11, 2, '7', '',''),"
                        + "(1,11, 3, '4', '',''),"
                        + "(1,11, 4, '5', '12-7=5','Se deben de restar las cantidades '),"
                        + "(1,12, 0, '5', '',''),"
                        + "(1,12, 1, '1', '',''),"
                        + "(1,12, 2, '4', '',''),"
                        + "(1,12, 3, '2', '',''),"
                        + "(1,12, 4, '1', '4-3=1','Se deben de restar las cantidades'),"
                        + "(1,13, 0, '8', '',''),"
                        + "(1,13, 1, '3', '',''),"
                        + "(1,13, 2, '4', '',''),"
                        + "(1,13, 3, '0', '',''),"
                        + "(1,13, 4, '4', '9-5=4','Se restan las cantidades'),"
                        + "(1,14, 0, '7', '',''),"
                        + "(1,14, 1, '5', '',''),"
                        + "(1,14, 2, '4', '',''),"
                        + "(1,14, 3, '3', '',''),"
                        + "(1,14, 4, '7', '2+5=7','Aplicar la suma'),"
                        + "(1,15, 0, '2', '',''),"
                        + "(1,15, 1, '1', '',''),"
                        + "(1,15, 2, '3', '',''),"
                        + "(1,15, 3, '4', '',''),"
                        + "(1,15, 4, '3', '5-2=3','Aplicar la resta'),"
                        + "(1,16, 0, '5', '',''),"
                        + "(1,16, 1, '4', '',''),"
                        + "(1,16, 2, '3', '',''),"
                        + "(1,16, 3, '2', '',''),"
                        + "(1,16, 4, '4', '2+2=4','Se debe de sumar los elefantes'),"
                        + "(1,17, 0, 'Ninguna de las anteriores', '',''),"
                        + "(1,17, 1, 'Signo de la resta', '',''),"
                        + "(1,17, 2, 'Cualquier signo', '',''),"
                        + "(1,17, 3, 'Signo de la suma', '',''),"
                        + "(1,17, 4, 'Signo de la suma', '+','Miramos el signo de la suma'),"
                        + "(1,18, 0, 'Doce', '',''),"
                        + "(1,18, 1, 'Trece', '',''),"
                        + "(1,18, 2, 'Quince', '',''),"
                        + "(1,18, 3, 'Dieciseis', '',''),"
                        + "(1,18, 4, 'Doce', 'Doce','Escribir en palabras 12'),"
                        + "(1,19, 0, '11', '',''),"
                        + "(1,19, 1, '14', '',''),"
                        + "(1,19, 2, '15', '',''),"
                        + "(1,19, 3, '16', '',''),"
                        + "(1,19, 4, '16', '16','Escribir de palabras a numeros'),"
                        + "(1,20, 0, '16', '',''),"
                        + "(1,20, 1, '5', '',''),"
                        + "(1,20, 2, '7', '',''),"
                        + "(1,20, 3, '4', '',''),"
                        + "(1,20, 4, '5', '8-3=5','Se debe de restar'),"
                        + "(1,21, 0, '14', '',''),"
                        + "(1,21, 1, '16', '',''),"
                        + "(1,21, 2, '1', '',''),"
                        + "(1,21, 3, '18', '',''),"
                        + "(1,21, 4, '18', '20-2','Se deben de restar las cantidades'),"
                        + "(1,22, 0, '0', '',''),"
                        + "(1,22, 1, '1', '',''),"
                        + "(1,22, 2, '5', '',''),"
                        + "(1,22, 3, '9', '',''),"
                        + "(1,22, 4, '0', '1-1=0','Se hace una resta'),"
                        + "(1,23, 0, '7', '',''),"
                        + "(1,23, 1, '6', '',''),"
                        + "(1,23, 2, '10', '',''),"
                        + "(1,23, 3, '4', '',''),"
                        + "(1,23, 4, '10', '5+5=10','Se suman las cantidades'),"
                        + "(1,24, 0, '5', '',''),"
                        + "(1,24, 1, '3', '',''),"
                        + "(1,24, 2, '8', '',''),"
                        + "(1,24, 3, '6', '',''),"
                        + "(1,24, 4, '3', '3','Escribir el numero'),"
                        + "(1,25, 0, '5', '',''),"
                        + "(1,25, 1, '1', '',''),"
                        + "(1,25, 2, '3', '',''),"
                        + "(1,25, 3, '2', '',''),"
                        + "(1,25, 4, '1', '4-1=1','Resto los pedasos'),"
                        + "(1,26, 0, '3', '',''),"
                        + "(1,26, 1, '8', '',''),"
                        + "(1,26, 2, '1', '',''),"
                        + "(1,26, 3, '2', '',''),"
                        + "(1,26, 4, '1', '10-9=1','Utilizamos la resta'),"
                        + "(1,27, 0, '12', '',''),"
                        + "(1,27, 1, '14', '',''),"
                        + "(1,27, 2, '13', '',''),"
                        + "(1,27, 3, '10', '',''),"
                        + "(1,27, 4, '12', 'Una Docena es 12','Pasamos de letras a numeros'),"
                        + "(1,28, 0, '7', '',''),"
                        + "(1,28, 1, '6', '',''),"
                        + "(1,28, 2, '2', '',''),"
                        + "(1,28, 3, '4', '',''),"
                        + "(1,28, 4, '2', '5-3=2','Usamos la resta'),"
                        + "(1,29, 0, '5', '',''),"
                        + "(1,29, 1, '4', '',''),"
                        + "(1,29, 2, '8', '',''),"
                        + "(1,29, 3, '6', '',''),"
                        + "(1,29, 4, '4', '2+2=4','Aplicamos la suma'),"
                        + "(1,30, 0, '5', '',''),"
                        + "(1,30, 1, '4', '',''),"
                        + "(1,30, 2, '8', '',''),"
                        + "(1,30, 3, '2', '',''),"
                        + "(1,30, 4, '8', '4+4=8','Aplicamos la suma'),"
                        ///////////////////////////////////////////
                        + "(2,1, 0, '12.000', '',''),"
                        + "(2,1, 1, '12', '',''),"
                        + "(2,1, 2, '6', '',''),"
                        + "(2,1, 3, '120', '',''),"
                        + "(2,1, 4, '12', 'Si sumas una unidad (1) y llegas hasta la docena en total tendrias 12','Una unidad se compone por 1'),"
                        + "(2,2, 0, '30', '',''),"
                        + "(2,2, 1, '31', '',''),"
                        + "(2,2, 2, '32', '',''),"
                        + "(2,2, 3, '33', '',''),"
                        + "(2,2, 4, '33', '8x4 es igual a 32, mas 1 igual 33','Primero multiplica y despues suma'),"
                        + "(2,3, 0, '16', '',''),"
                        + "(2,3, 1, '7', '',''),"
                        + "(2,3, 2, '6', '',''),"
                        + "(2,3, 3, 'Ninguna', '',''),"
                        + "(2,3, 4, '6', 'Hexa es 6','Pon mucha atencion a el nombre de la figura'),"
                        + "(2,4, 0, '10', '',''),"
                        + "(2,4, 1, '12', '',''),"
                        + "(2,4, 2, '15', '',''),"
                        + "(2,4, 3, '11', '',''),"
                        + "(2,4, 4, '12', 'El hermano de marcos tendra 12 años','Ten en cuenta la edad de marcos'),"
                        + "(2,5, 0, '90036', '',''),"
                        + "(2,5, 1, '936', '',''),"
                        + "(2,5, 2, '9036', '',''),"
                        + "(2,5, 3, '9306', '',''),"
                        + "(2,5, 4, '9036', 'Nueve mil (9000) treinta y seis (36), y quedaria asi 9036','Lee cuidadosamente el numero'),"
                        + "(2,6, 0, '9', '',''),"
                        + "(2,6, 1, '8', '',''),"
                        + "(2,6, 2, '7', '',''),"
                        + "(2,6, 3, '6', '',''),"
                        + "(2,6, 4, '8', '56 entre 7 da como resultado 8','Ten en cuanta las tablas de multiplicar'),"
                        + "(2,7, 0, 'Uno', '',''),"
                        + "(2,7, 1, 'Cinco', '',''),"
                        + "(2,7, 2, 'Diez', '',''),"
                        + "(2,7, 3, 'Cien', '',''),"
                        + "(2,7, 4, 'Cinco', 'El número romano V es igual a cinco','El numero cuatro es IV y el seis es VI'),"
                        + "(2,8, 0, '1001', '',''),"
                        + "(2,8, 1, '990', '',''),"
                        + "(2,8, 2, '999', '',''),"
                        + "(2,8, 3, '998', '',''),"
                        + "(2,8, 4, '999', 'El número anterior a 1000 es 999','Restale uno a mil (1000-1)'),"
                        + "(2,9, 0, 'Tercero', '',''),"
                        + "(2,9, 1, 'Segundo', '',''),"
                        + "(2,9, 2, 'Primero', '',''),"
                        + "(2,9, 3, 'Ninguna', '',''),"
                        + "(2,9, 4, 'Segundo', 'Vas en segundo puesto','1 2 3 si el 3 pasa al 2 que pasaria'),"
                        + "(2,10, 0, 'Multiplicar por tres', '',''),"
                        + "(2,10, 1, 'Restar tres', '',''),"
                        + "(2,10, 2, 'Sumar por tres', '',''),"
                        + "(2,10, 3, 'Dividir entre tres', '',''),"
                        + "(2,10, 4, 'Dividir entre tres', 'Tienes que dividir la cifra entre tres','Pon mucha atencion al texto'),"
                        + "(2,11, 0, 'Da cero', '',''),"
                        + "(2,11, 1, 'Da uno', '',''),"
                        + "(2,11, 2, 'Da dos', '',''),"
                        + "(2,11, 3, 'Da tres', '',''),"
                        + "(2,11, 4, 'Da cero', 'Cuando la división da cero hablamos de una división exacta','Divide 2 entre 2'),"
                        + "(2,12, 0, 'Heptágono', '',''),"
                        + "(2,12, 1, 'Hexágono', '',''),"
                        + "(2,12, 2, 'Septágono', '',''),"
                        + "(2,12, 3, 'Dodecaedro', '',''),"
                        + "(2,12, 4, 'Heptágono', 'Se llama heptágono','Imagina una figura de 7 lados y ten encuenta los nombres de las opciones'),"
                        + "(2,13, 0, 'Escaleno', '',''),"
                        + "(2,13, 1, 'Equilátero', '',''),"
                        + "(2,13, 2, 'Isósceles', '',''),"
                        + "(2,13, 3, 'Ninguna', '',''),"
                        + "(2,13, 4, 'Isósceles', 'El triángulo isósceles tiene dos lados iguales y uno desigual','Solo tiene dos lados iguales'),"
                        + "(2,14, 0, 'Las tres  y media', '',''),"
                        + "(2,14, 1, 'Las tres y cuarto', '',''),"
                        + "(2,14, 2, 'Las tres en punto', '',''),"
                        + "(2,14, 3, 'Un cuarto para las tres', '',''),"
                        + "(2,14, 4, 'Las tres y cuarto', 'Son las 3:15 o las tres y cuarto','La manilla grande cada vez que se hace en un numero se puede contar como 5'),"
                        + "(2,15, 0, '58', '',''),"
                        + "(2,15, 1, '72', '',''),"
                        + "(2,15, 2, '81', '',''),"
                        + "(2,15, 3, '63', '',''),"
                        + "(2,15, 4, '63', '63 es la respuesta correcta','Suma nueve veces el siete'),"
                        + "(2,16, 0, '7', '',''),"
                        + "(2,16, 1, '6', '',''),"
                        + "(2,16, 2, '5', '',''),"
                        + "(2,16, 3, '4', '',''),"
                        + "(2,16, 4, '5', '5 es la respuesta correcta','Resta 12-7'),"
                        + "(2,17, 0, '14', '',''),"
                        + "(2,17, 1, '1400', '',''),"
                        + "(2,17, 2, '140', '',''),"
                        + "(2,17, 3, '1.4', '',''),"
                        + "(2,17, 4, '14', '14 es la respuesta correcta','Divide 140/10'),"
                        + "(2,18, 0, 'Volviéndola a hacer', '',''),"
                        + "(2,18, 1, 'Multiplicando y sumando', '',''),"
                        + "(2,18, 2, 'Sumando y restando', '',''),"
                        + "(2,18, 3, 'Restando y multiplicando', '',''),"
                        + "(2,18, 4, 'Multiplicando y sumando', 'Multiplicando el cociente por el divisor y sumando el resto si lo hay','primero M... y luego Su...'),"
                        + "(2,19, 0, '360800', '',''),"
                        + "(2,19, 1, '3680', '',''),"
                        + "(2,19, 2, '36800', '',''),"
                        + "(2,19, 3, '360000800', '',''),"
                        + "(2,19, 4, '36800', '36800 es la respuesta correcta','Unidades, decenas, centenas, ...'),"
                        + "(2,20, 0, '1/7', '',''),"
                        + "(2,20, 1, '2/7', '',''),"
                        + "(2,20, 2, '7/5', '',''),"
                        + "(2,20, 3, '5/7', '',''),"
                        + "(2,20, 4, '5/7', '5/7 es la respuesta correcta','Pon mucha atencion a la pregunta'),"
                        + "(2,21, 0, '24', '',''),"
                        + "(2,21, 1, '60', '',''),"
                        + "(2,21, 2, '1', '',''),"
                        + "(2,21, 3, '120', '',''),"
                        + "(2,21, 4, '60', '60 es la respuesta correcta','30 minutos es media hora'),"
                        + "(2,22, 0, '54', '',''),"
                        + "(2,22, 1, '60', '',''),"
                        + "(2,22, 2, '44', '',''),"
                        + "(2,22, 3, '64', '',''),"
                        + "(2,22, 4, '44', '6x8=48-4= 44','Si 6x10 es 60 y 6x9 es 54'),"
                        + "(2,23, 0, 'Febrero', '',''),"
                        + "(2,23, 1, 'Febrero - agosto', '',''),"
                        + "(2,23, 2, 'Todos', '',''),"
                        + "(2,23, 3, 'Ninguno', '',''),"
                        + "(2,23, 4, 'Todos', 'Todos los meses tienen 28','Cuantos meses pasan por 28'),"
                        + "(2,24, 0, '520', '',''),"
                        + "(2,24, 1, '360', '',''),"
                        + "(2,24, 2, '244', '',''),"
                        + "(2,24, 3, '365', '',''),"
                        + "(2,24, 4, '365', '365 es la respuesta correcta','Suma el numero maximo de los dias que tiene cada mes'),"
                        + "(2,25, 0, '380', '',''),"
                        + "(2,25, 1, '340', '',''),"
                        + "(2,25, 2, '280', '',''),"
                        + "(2,25, 3, '390', '',''),"
                        + "(2,25, 4, '380', '380 es la respuesta correcta','Suma toda la cantidad que tienes'),"
                        + "(2,26, 0, '-1', '',''),"
                        + "(2,26, 1, '1', '',''),"
                        + "(2,26, 2, '-0.5', '',''),"
                        + "(2,26, 3, '2', '',''),"
                        + "(2,26, 4, '-0.5', '-0.5 es la respuesta correcta','Ubicando los numeros en la recta numerica, encontraras la respuesta'),"
                        + "(2,27, 0, '0.049', '',''),"
                        + "(2,27, 1, '4900', '',''),"
                        + "(2,27, 2, '490', '',''),"
                        + "(2,27, 3, '1490', '',''),"
                        + "(2,27, 4, '4900', '4900 es la respuesta correcta','49x10 es 490 cuanto es por 100'),"
                        + "(2,28, 0, '0', '',''),"
                        + "(2,28, 1, '6', '',''),"
                        + "(2,28, 2, '4', '',''),"
                        + "(2,28, 3, '2', '',''),"
                        + "(2,28, 4, '4', '4 porque solo utilizo 2 huevos','Solo resta la cantidad que se utilizo'),"
                        + "(2,29, 0, '10', '',''),"
                        + "(2,29, 1, '9', '',''),"
                        + "(2,29, 2, '12', '',''),"
                        + "(2,29, 3, '8', '',''),"
                        + "(2,29, 4, '9', '9 es la respuesta correcta','Suma y resta los dijitos mencionados'),"
                        + "(2,30, 0, '1935', '',''),"
                        + "(2,30, 1, '1876', '',''),"
                        + "(2,30, 2, '2235', '',''),"
                        + "(2,30, 3, '1834', '',''),"
                        + "(2,30, 4, '1935', '1935 es la respuesta correcta','Divide entre 2'),"
                        //////////////////////////////////////////////////////////////////////////     
                        + "(3,1, 0, 'Elevar al exponente 0', '',''),"
                        + "(3,1, 1, 'Elevar a N', '',''),"
                        + "(3,1, 2, 'Dividir entre N', '',''),"
                        + "(3,1, 3, 'Logaritmo base N', '',''),"
                        + "(3,1, 4, 'Elevar a N', 'Para introducir un factor dentro de un radical"
                        + " se eleva el factor a la potencia que indica el indice"
                        + " y se escribe dentro. Si algún factor del radicando tiene por exponente un numero mayor "
                        + "que el indice,se puede extraer fuera "
                        + "del radical dividiendo el exponente del "
                        + " radicando entre el indice.','Recuerda los indices, así pues en base de N'),"
                        + "(3,2, 0, 'Perpendicular', '',''),"
                        + "(3,2, 1, 'Oblicuas', '',''),"
                        + "(3,2, 2, 'Paralelas', '',''),"
                        + "(3,2, 3, 'Bipetas', '',''),"
                        + "(3,2, 4, 'Paralelas', 'Dos rectas son paralelas cuando no "
                        + "tienen ningun punto en comun.','Cuando no existe ningun punto en comun indica que no se tocan'),"
                        + "(3,3, 0, 'Cóncavo', '',''),"
                        + "(3,3, 1, 'Convexo', '',''),"
                        + "(3,3, 2, 'Llano', '',''),"
                        + "(3,3, 3, 'Recto', '',''),"
                        + "(3,3, 4, 'Llano', 'Diedro llano es aquel que tiene sus caras, una prolongacion de la otra.','Poliedro de caras regulares pero no regular en si mismo'),"
                        + "(3,4, 0, 'Cóncavo', '',''),"
                        + "(3,4, 1, 'Agudo', '',''),"
                        + "(3,4, 2, 'Convexo', '',''),"
                        + "(3,4, 3, 'Suplementario', '',''),"
                        + "(3,4, 4, 'Convexo', 'Se llama diedro convexo a la interseccion de dos semiespacios cuyos bordes se cortan o coinciden.','Si las caras de un diedro son semiplanos opuestos'),"
                        + "(3,5, 0, '0', '',''),"
                        + "(3,5, 1, 'x', '',''),"
                        + "(3,5, 2, '1', '',''),"
                        + "(3,5, 3, 'Indeterminacion', '',''),"
                        + "(3,5, 4, '1', 'Cualquier numero diferente de cero elevado a la potencia cero es igual a uno.','La potencia se define como producto de factores iguales, donde la base se multiplica tantas veces como indica el exponente'),"
                        + "(3,6, 0, '1', '',''),"
                        + "(3,6, 1, '2', '',''),"
                        + "(3,6, 2, 'Raiz cuadrada de 3', '',''),"
                        + "(3,6, 3, '30', '',''),"
                        + "(3,6, 4, 'Raiz cuadrada de 3', 'el valor de la cotangente de 30° esCotg 30° =√3','Razon trigonometrica de un angulo equivalente a la inversa de la tangente del mismo angulo'),"
                        + "(3,7, 0, 'π', '',''),"
                        + "(3,7, 1, 'π/2', '',''),"
                        + "(3,7, 2, 'π+1', '',''),"
                        + "(3,7, 3, 'π/3', '',''),"
                        + "(3,7, 4, 'π/2', 'Un radian es la amplitud de un angulo central de una circunferencia que abarca un arco de la misma longitud que el radio.','4 cuadrantes y se define el radian como el angulo que en una circunferencia.'),"
                        + "(3,8, 0, 'π/3', '',''),"
                        + "(3,8, 1, 'π/2', '',''),"
                        + "(3,8, 2, 'Raíz cuadrada de tres partidos por dos', '',''),"
                        + "(3,8, 3, 'π', '',''),"
                        + "(3,8, 4, 'Raíz cuadrada de tres partidos por dos', 'El seno de 60 grados o π/3 radianes es exactamente igual a la mitad de la raiz cuadrada de tres.','El seno (sin o sen) es el cociente entre el cateto opuesto al angulo y la hipotenusa'),"
                        + "(3,9, 0, 'El numero de puntos', '',''),"
                        + "(3,9, 1, 'El valor del area', '',''),"
                        + "(3,9, 2, 'El doble del numero de lados', '',''),"
                        + "(3,9, 3, 'Al numero de lados', '',''),"
                        + "(3,9, 4, 'Al numero de lados', 'GENERO Se denomina así al numero de cuerdas o lados del polígono','Denominado asi el numero de cuerdas inscritos en un poligono'),"
                        + "(3,10, 0, 'NxN', '',''),"
                        + "(3,10, 1, 'N elvado a la 3', '',''),"
                        + "(3,10, 2, 'N', '',''),"
                        + "(3,10, 3, '2N+1', '',''),"
                        + "(3,10, 4, 'NxN', ' La suma de los n primeros numeros impares, equivale al area de un cuadrado de lado n','Para saber la cantidad de una cantidad N, tiene que haber una multiplicacion'),"
                        + "(3,11, 0, '0', '',''),"
                        + "(3,11, 1, '1', '',''),"
                        + "(3,11, 2, '90', '',''),"
                        + "(3,11, 3, '10', '',''),"
                        + "(3,11, 4, '0', 'Para obtener la cotangente, debemos saber la expresión para el coseno y el seno de 90 grados: Cotang90 = Cos90/Sen90. (0/1 = 0)','La cotangente es el inverso de la tangente : cotan(x)=1tan(x)=cos(x)sin(x).'),"
                        + "(3,12, 0, '1000', '',''),"
                        + "(3,12, 1, '1/100', '',''),"
                        + "(3,12, 2, '1/1000', '',''),"
                        + "(3,12, 3, '0', '',''),"
                        + "(3,12, 4, '1/1000', 'Para obtener la probalidad sobre un conjunto es necesario hacer una division, y en este caso seria 1 entre 1000','Probabilidad de 1 entre 1000, division.'),"
                        + "(3,13, 0, 'El cuadrado de n', '',''),"
                        + "(3,13, 1, 'La mitad de la primera', '',''),"
                        + "(3,13, 2, 'El cuadrado de la primera', '',''),"
                        + "(3,13, 3, 'Diez veces la primera', '',''),"
                        + "(3,13, 4, 'El cuadrado de la primera', 'El cociente de dos fracciones es otra fraccion que tiene como "
                        + " numerador el producto del numerador del dividendo por el denominador del divisor','Si multiplicas una fracción por su recíproca siempre sale 1'),"
                        + "(3,14, 0, 'Ecuacion', '',''),"
                        + "(3,14, 1, 'Resolucion', '',''),"
                        + "(3,14, 2, 'Inecuación', '',''),"
                        + "(3,14, 3, 'Ecuacion sin incognita', '',''),"
                        + "(3,14, 4, 'Inecuación', 'Una inecuacion algebraica es una desigualdad en la cual aparece al menos un termino desconocido','se determinan los valores reales que satisfacen la condicion dada'),"
                        + "(3,15, 0, 'e', '',''),"
                        + "(3,15, 1, '0', '',''),"
                        + "(3,15, 2, 'log 1', '',''),"
                        + "(3,15, 3, '1', '',''),"
                        + "(3,15, 4, '1', 'El conjunto de logaritmos de una misma base se denomina sistema de y La base de todo sistema de logaritmos es siempre un numero positivo mayor que 1','Buscar una base para el logaritmo que cumpla la condicion'),"
                        + "(3,16, 0, 'Indeterminada', '',''),"
                        + "(3,16, 1, 'Parábola', '',''),"
                        + "(3,16, 2, 'Lineal', '',''),"
                        + "(3,16, 3, 'Rectas', '',''),"
                        + "(3,16, 4, 'Parábola', 'La gráfica de una ecuación de segundo grado es una parábola','la seccion conica de excentricidad igual a 1'),"
                        + "(3,17, 0, '2', '',''),"
                        + "(3,17, 1, 'x', '',''),"
                        + "(3,17, 2, 'Indefinido', '',''),"
                        + "(3,17, 3, '1', '',''),"
                        + "(3,17, 4, 'Indefinido', 'El logaritmo de 0 significa que x es cero, lo cual no esta definido,"
                        + " o esta prohibido, debido a que x necesariamente, tiene que ser mayor que cero. ¡El logaritmo de 0 no existe!','ya que no hay forma de elevar un numero a algo que de cero'),"
                        + "(3,18, 0, 'Indefinido', '',''),"
                        + "(3,18, 1, '0', '',''),"
                        + "(3,18, 2, '100', '',''),"
                        + "(3,18, 3, '1', '',''),"
                        + "(3,18, 4, '1', 'log10 (10) = 1, Ahora ya sabemos que el logaritmo de diez en base diez = 1.','El logaritmo de un numero, en una base dada, es el exponente al cual se debe elevar la base para obtener el numero.'),"
                        + "(3,19, 0, 'M*0', '',''),"
                        + "(3,19, 1, 'M!', '',''),"
                        + "(3,19, 2, 'M/2', '',''),"
                        + "(3,19, 3, 'M', '',''),"
                        + "(3,19, 4, 'M!', 'Se llama permutaciones de m elementos (m = n) a las diferentes agrupaciones de esos m elementos de forma que: la cantidad de elementos en su forma factorial','Se llama permutaciones de m elementos (m = n).'),"
                        + "(3,20, 0, 'Raíz cuadrada de dos dividido por 10', '',''),"
                        + "(3,20, 1, 'Raíz cuadrada de dos dividido por 1', '',''),"
                        + "(3,20, 2, 'Raíz cuadrada de dos dividido por 2', '',''),"
                        + "(3,20, 3, 'Raíz cuadrada de dos dividido por 3', '',''),"
                        + "(3,20, 4, 'Raíz cuadrada de dos dividido por 2', 'El seno de 45 grados o π/4 radianes es exactamente igual a la mitad de la raiz cuadrada de dos.','Se entiende como Seno (abreviado como sen, o sin) la relacion que existe entre el cateto opuesto al angulo y la hipotenusa.'),"
                        + "(3,21, 0, 'El valor ideal', '',''),"
                        + "(3,21, 1, 'x', '',''),"
                        + "(3,21, 2, 'Discriminante', '',''),"
                        + "(3,21, 3, 'La matriz', '',''),"
                        + "(3,21, 4, 'Discriminante', 'En algebra, el discriminante de un polinomio es una cierta expresion de los coeficientes de dicho polinomio que es igual a cero si y solo si el polinomio tiene raices multiples en el plano complejo','En álgebra, el discriminante de un polinomio es una cierta expresión de los coeficientes de dicho polinomio'),"
                        + "(3,22, 0, 'La mitad de la distancia', '',''),"
                        + "(3,22, 1, 'La misma distancia', '',''),"
                        + "(3,22, 2, 'Diferente a la distancia en 2 unidades', '',''),"
                        + "(3,22, 3, 'Diferente a la distancia', '',''),"
                        + "(3,22, 4, 'La misma distancia', 'Los segmentos de recta paralelos siempre conservan la misma distancia entre si, y nunca se unen aun si se extendieran infinitamente en cualquiera de las dos direcciones.','Imagina una recta cualquiera y un punto exterior a ella'),"
                        + "(3,23, 0, 'Angulo sin medida', '',''),"
                        + "(3,23, 1, 'Angulo semirecto', '',''),"
                        + "(3,23, 2, 'Angulo rectilineo', '',''),"
                        + "(3,23, 3, 'Angulo curvo', '',''),"
                        + "(3,23, 4, 'Angulo rectilineo', 'El plano que forman las dos perpendiculares se llama seccion normal o rectilineo del diedro y en PLANOS PERPENDICULARES, dos planos son perpendiculares, cuando determinan un diedro que mide 90°','a normal se llama arista y esta en la itad de los dos planos que forman el diedro'),"
                        + "(3,24, 0, '0', '',''),"
                        + "(3,24, 1, 'Ordenada en el origen', '',''),"
                        + "(3,24, 2, 'Una interseccion cualquiera', '',''),"
                        + "(3,24, 3, 'x', '',''),"
                        + "(3,24, 4, 'Ordenada en el origen', 'Recuerda que en la forma pendiente-interseccion de una ecuacion y = mx + b, m = pendiente y b = el valor y de la interseccion en y','Mientras que la ordenada al origen se encuentra pasando las variables al lado izquierdo de la ecuación y dándole valor a las x.'),"
                        + "(3,25, 0, 'El mcm', '',''),"
                        + "(3,25, 1, 'El valor absoluto', '',''),"
                        + "(3,25, 2, 'Una inclinacion ', '',''),"
                        + "(3,25, 3, 'Un plano', '',''),"
                        + "(3,25, 4, 'Un plano', 'Dos puntos determinan una recta. Dos rectas que se cortan determinan un punto. Una recta indica una direccion y dos sentidos contrarios','Una recta tiene una dimension,longitud'),"
                        + "(3,26, 0, '64', '',''),"
                        + "(3,26, 1, '24', '',''),"
                        + "(3,26, 2, '6', '',''),"
                        + "(3,26, 3, '8', '',''),"
                        + "(3,26, 4, '24', 'Necesitamos encontrar el minimo común multiplo de 6 y 8. Para hacer esto, podemos enlistar los multiplos: 6, 12, 18, 24 , 30, 36, 42, 48, ... El primer numero que aperece en ambas listas es el 24, asi que 24 es el MCM.','descomponer esos números en factores primos.'),"
                        + "(3,27, 0, '65', '',''),"
                        + "(3,27, 1, '48', '',''),"
                        + "(3,27, 2, '2212', '',''),"
                        + "(3,27, 3, '12.288', '',''),"
                        + "(3,27, 4, '48', 'MCM(16,24y32)=(2)(2)(2)(2)(3) ; MCM(16,24y32)=(16)(3) ; MCM(16,24y32)=48','descomponer esos números en factores primos.'),"
                        + "(3,28, 0, '12', '',''),"
                        + "(3,28, 1, '8', '',''),"
                        + "(3,28, 2, '9', '',''),"
                        + "(3,28, 3, '20', '',''),"
                        + "(3,28, 4, '8', 'El Máximo Común Divisor de 16 y 24,Los factores de 16 son 1,2,4,8,16; Los factores de 24 son:1,2,3,4,6,8,12,24;, es 8.','1 Se descomponen los números en factores primos. 2 Se toman los factores comunes con menor exponente.   3 Se multiplican dichos factores y el resultado obtenido es el mcd.'),"
                        + "(3,29, 0, '28', '',''),"
                        + "(3,29, 1, '18', '',''),"
                        + "(3,29, 2, '16', '',''),"
                        + "(3,29, 3, '10', '',''),"
                        + "(3,29, 4, '18', 'El mcm de 6 y 9 se puede obtener de la siguiente manera: Los múltiplos de 6 son … , 12, 18, 24, ….;;; Los múltiplos de 9 son …, 9, 18, 27, …"
                        + "Los múltiplos comunes de 6 y 9 son n x 18, intersectando los dos conjuntos mencionados arriba,"
                        + " En la intersección de múltiplos de 6 ∩ múltiplos de 9 el menor elemento positivo es 18."
                        + "   Por lo tanto, el mínimo común múltiplo de 6 y 9 es 18.','descomponer esos números en factores primos.'),"
                        + "(3,30, 0, '16', '',''),"
                        + "(3,30, 1, '2', '',''),"
                        + "(3,30, 2, '1', '',''),"
                        + "(3,30, 3, '8', '',''),"
                        + "(3,30, 4, '2', 'La raíz cubica de 8, y que se escribe , significa ¿qué número multiplicado tres veces por si mismo da 8? 2 al cubo o 2 a la tercera potencia (2 × 2 × 2) es 8','La raiz cubica de un número es el factor que necesitamos multiplicar tres veces por si mismo para obtener ese numero.')";
                mn.statement = mn.connection.prepareStatement(sql);
                mn.statement.executeUpdate();
                mn.desconectar();
                /*  PrintWriter escribir=new PrintWriter(new  FileWriter(base,true));
                            escribir.println(cb.nombre.getText());
                            escribir.close();
                 */
            } catch (Exception ex) {
                System.out.println("2" + ex);
            }

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    InicioJuego ij;
    int num0 = 0;

    public void ModeloIniJuego(InicioJuego ij) {
        this.ij = ij;

        try {
            mn.conectar(ij.c.mp.bd);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (ij.c.Numcategoria == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ij.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ij.maxjuego);
                }

                ResultSet preg = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE '" + ij.num + "'and categoria like '" + 1 + "'");

                while (preg.next()) {
                    ij.c.mp.bd.Spregunta = (preg.getObject(3) + "");
                    //System.out.println("AMI: "+ ij.c.mp.bd.Spregunta);
                }

                ////////////////////////////////OPCIONES//////////////////////////////////////////////
                ResultSet opca = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE '" + 0 + "' and categoria like '" + 1 + "'");
                while (opca.next()) {
                    ij.c.mp.bd.SopcionA = (opca.getObject(4) + "");

                }

                ResultSet opcb = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE '" + 1 + "' and categoria like '" + 1 + "'");
                while (opcb.next()) {
                    ij.c.mp.bd.SopcionB = (opcb.getObject(4) + "");

                }

                ResultSet opcc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE '" + 2 + "' and categoria like '" + 1 + "'");
                while (opcc.next()) {
                    ij.c.mp.bd.SopcionC = (opcc.getObject(4) + "");

                }

                ResultSet opcd = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE'" + 3 + "' and categoria like '" + 1 + "'");
                while (opcd.next()) {
                    ij.c.mp.bd.SopcionD = (opcd.getObject(4) + "");

                }

                ResultSet respuesta = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 1 + "'");
                while (respuesta.next()) {
                    ij.c.mp.bd.Srespuesta = (respuesta.getObject(4) + "");

                }

                ResultSet explicaobli = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 1 + "'");
                while (explicaobli.next()) {
                    ij.c.mp.bd.SExplicaObli = (explicaobli.getObject(5) + "");

                }

                ResultSet explicaopc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 1 + "'");
                while (explicaopc.next()) {
                    ij.c.mp.bd.SExplicaOpc = (explicaopc.getObject(6) + "");

                }

                preg.close();
                opca.close();
                opcb.close();
                opcc.close();
                opcd.close();
                respuesta.close();
                explicaobli.close();
                explicaopc.close();

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (ij.c.Numcategoria == 2) {

                ResultSet preg = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE '" + ij.num + "'and categoria like '" + 2 + "'");

                while (preg.next()) {
                    ij.c.mp.bd.Spregunta = (preg.getObject(3) + "");
                    //System.out.println("AMI: "+ ij.c.mp.bd.Spregunta);
                }

                ////////////////////////////////OPCIONES//////////////////////////////////////////////
                ResultSet opca = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE '" + 0 + "' and categoria like '" + 2 + "'");
                while (opca.next()) {
                    ij.c.mp.bd.SopcionA = (opca.getObject(4) + "");

                }

                ResultSet opcb = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE '" + 1 + "' and categoria like '" + 2 + "'");
                while (opcb.next()) {
                    ij.c.mp.bd.SopcionB = (opcb.getObject(4) + "");

                }

                ResultSet opcc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE '" + 2 + "' and categoria like '" + 2 + "'");
                while (opcc.next()) {
                    ij.c.mp.bd.SopcionC = (opcc.getObject(4) + "");

                }

                ResultSet opcd = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE'" + 3 + "' and categoria like '" + 2 + "'");
                while (opcd.next()) {
                    ij.c.mp.bd.SopcionD = (opcd.getObject(4) + "");

                }

                ResultSet respuesta = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 2 + "'");
                while (respuesta.next()) {
                    ij.c.mp.bd.Srespuesta = (respuesta.getObject(4) + "");

                }

                ResultSet explicaobli = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 2 + "'");
                while (explicaobli.next()) {
                    ij.c.mp.bd.SExplicaObli = (explicaobli.getObject(5) + "");

                }

                ResultSet explicaopc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 2 + "'");
                while (explicaopc.next()) {
                    ij.c.mp.bd.SExplicaOpc = (explicaopc.getObject(6) + "");

                }

                preg.close();
                opca.close();
                opcb.close();
                opcc.close();
                opcd.close();
                respuesta.close();
                explicaobli.close();
                explicaopc.close();

            }

            if (ij.c.Numcategoria == 3) {

                ResultSet preg = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE '" + ij.num + "'and categoria like '" + 3 + "'");

                while (preg.next()) {
                    ij.c.mp.bd.Spregunta = (preg.getObject(3) + "");
                    //System.out.println("AMI: "+ ij.c.mp.bd.Spregunta);
                }

                ////////////////////////////////OPCIONES//////////////////////////////////////////////
                ResultSet opca = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE '" + 0 + "' and categoria like '" + 3 + "'");
                while (opca.next()) {
                    ij.c.mp.bd.SopcionA = (opca.getObject(4) + "");

                }

                ResultSet opcb = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE '" + 1 + "' and categoria like '" + 3 + "'");
                while (opcb.next()) {
                    ij.c.mp.bd.SopcionB = (opcb.getObject(4) + "");

                }

                ResultSet opcc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE '" + 2 + "' and categoria like '" + 3 + "'");
                while (opcc.next()) {
                    ij.c.mp.bd.SopcionC = (opcc.getObject(4) + "");

                }

                ResultSet opcd = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ij.num + "'and opciones LIKE'" + 3 + "' and categoria like '" + 3 + "'");
                while (opcd.next()) {
                    ij.c.mp.bd.SopcionD = (opcd.getObject(4) + "");

                }

                ResultSet respuesta = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 3 + "'");
                while (respuesta.next()) {
                    ij.c.mp.bd.Srespuesta = (respuesta.getObject(4) + "");

                }

                ResultSet explicaobli = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 3 + "'");
                while (explicaobli.next()) {
                    ij.c.mp.bd.SExplicaObli = (explicaobli.getObject(5) + "");

                }

                ResultSet explicaopc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ij.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 3 + "'");
                while (explicaopc.next()) {
                    ij.c.mp.bd.SExplicaOpc = (explicaopc.getObject(6) + "");

                }
                preg.close();
                opca.close();
                opcb.close();
                opcc.close();
                opcd.close();
                respuesta.close();
                explicaobli.close();
                explicaopc.close();
            }

            mn.desconectar();

            if (ij.c.mp.bd.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el codigo1");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    InicioJuegoAdmin ija;

    public void ModeloIniJuegoAdmin(InicioJuegoAdmin ija) {
        this.ija = ija;

        try {
            mn.conectar(ija.ca.ma.cda);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (ija.ca.Numcategoria == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ija.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ija.maxjuego);
                }

                ResultSet preg = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE '" + ija.num + "'and categoria like '" + 1 + "'");

                while (preg.next()) {
                    ija.ca.ma.cda.Spregunta = (preg.getObject(3) + "");
                    //System.out.println("AMI: "+ ij.c.mp.bd.Spregunta);
                }

                ////////////////////////////////OPCIONES//////////////////////////////////////////////
                ResultSet opca = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE '" + 0 + "' and categoria like '" + 1 + "'");
                while (opca.next()) {
                    ija.ca.ma.cda.SopcionA = (opca.getObject(4) + "");

                }

                ResultSet opcb = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE '" + 1 + "' and categoria like '" + 1 + "'");
                while (opcb.next()) {
                    ija.ca.ma.cda.SopcionB = (opcb.getObject(4) + "");

                }

                ResultSet opcc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE '" + 2 + "' and categoria like '" + 1 + "'");
                while (opcc.next()) {
                    ija.ca.ma.cda.SopcionC = (opcc.getObject(4) + "");

                }

                ResultSet opcd = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE'" + 3 + "' and categoria like '" + 1 + "'");
                while (opcd.next()) {
                    ija.ca.ma.cda.SopcionD = (opcd.getObject(4) + "");

                }

                ResultSet respuesta = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 1 + "'");
                while (respuesta.next()) {
                    ija.ca.ma.cda.Srespuesta = (respuesta.getObject(4) + "");

                }

                ResultSet explicaobli = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 1 + "'");
                while (explicaobli.next()) {
                    ija.ca.ma.cda.SExplicaObli = (explicaobli.getObject(5) + "");

                }

                ResultSet explicaopc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 1 + "'");
                while (explicaopc.next()) {
                    ija.ca.ma.cda.SExplicaOpc = (explicaopc.getObject(6) + "");

                }

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (ija.ca.Numcategoria == 2) {

                ResultSet preg = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE '" + ija.num + "'and categoria like '" + 2 + "'");

                while (preg.next()) {
                    ija.ca.ma.cda.Spregunta = (preg.getObject(3) + "");
                    //System.out.println("AMI: "+ ij.c.mp.bd.Spregunta);
                }

                ////////////////////////////////OPCIONES//////////////////////////////////////////////
                ResultSet opca = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE '" + 0 + "' and categoria like '" + 2 + "'");
                while (opca.next()) {
                    ija.ca.ma.cda.SopcionA = (opca.getObject(4) + "");

                }

                ResultSet opcb = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE '" + 1 + "' and categoria like '" + 2 + "'");
                while (opcb.next()) {
                    ija.ca.ma.cda.SopcionB = (opcb.getObject(4) + "");

                }

                ResultSet opcc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE '" + 2 + "' and categoria like '" + 2 + "'");
                while (opcc.next()) {
                    ija.ca.ma.cda.SopcionC = (opcc.getObject(4) + "");

                }

                ResultSet opcd = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE'" + 3 + "' and categoria like '" + 2 + "'");
                while (opcd.next()) {
                    ija.ca.ma.cda.SopcionD = (opcd.getObject(4) + "");

                }

                ResultSet respuesta = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 2 + "'");
                while (respuesta.next()) {
                    ija.ca.ma.cda.Srespuesta = (respuesta.getObject(4) + "");

                }

                ResultSet explicaobli = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 2 + "'");
                while (explicaobli.next()) {
                    ija.ca.ma.cda.SExplicaObli = (explicaobli.getObject(5) + "");

                }

                ResultSet explicaopc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 2 + "'");
                while (explicaopc.next()) {
                    ija.ca.ma.cda.SExplicaOpc = (explicaopc.getObject(6) + "");

                }

            }

            if (ija.ca.Numcategoria == 3) {

                ResultSet preg = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE '" + ija.num + "'and categoria like '" + 3 + "'");

                while (preg.next()) {
                    ija.ca.ma.cda.Spregunta = (preg.getObject(3) + "");
                    //System.out.println("AMI: "+ ij.c.mp.bd.Spregunta);
                }

                ////////////////////////////////OPCIONES//////////////////////////////////////////////
                ResultSet opca = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE '" + 0 + "' and categoria like '" + 3 + "'");
                while (opca.next()) {
                    ija.ca.ma.cda.SopcionA = (opca.getObject(4) + "");

                }

                ResultSet opcb = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE '" + 1 + "' and categoria like '" + 3 + "'");
                while (opcb.next()) {
                    ija.ca.ma.cda.SopcionB = (opcb.getObject(4) + "");

                }

                ResultSet opcc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE '" + 2 + "' and categoria like '" + 3 + "'");
                while (opcc.next()) {
                    ija.ca.ma.cda.SopcionC = (opcc.getObject(4) + "");

                }

                ResultSet opcd = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + ija.num + "'and opciones LIKE'" + 3 + "' and categoria like '" + 3 + "'");
                while (opcd.next()) {
                    ija.ca.ma.cda.SopcionD = (opcd.getObject(4) + "");

                }

                ResultSet respuesta = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 3 + "'");
                while (respuesta.next()) {
                    ija.ca.ma.cda.Srespuesta = (respuesta.getObject(4) + "");

                }

                ResultSet explicaobli = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 3 + "'");
                while (explicaobli.next()) {
                    ija.ca.ma.cda.SExplicaObli = (explicaobli.getObject(5) + "");

                }

                ResultSet explicaopc = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE '" + ija.num + "'and opciones LIKE'" + 4 + "' and categoria like '" + 3 + "'");
                while (explicaopc.next()) {
                    ija.ca.ma.cda.SExplicaOpc = (explicaopc.getObject(6) + "");

                }

            }

            mn.desconectar();
            if (ija.ca.ma.cda.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el codigo1");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public Logueo log;

    public void ModeloLogueo(Logueo log) {
        this.log = log;

        if (log.jtUsu.getText() != null && log.jtContra.getText() != null) {

            try {
                mn.conectar(log.cl.cb);
                Statement d = mn.connection.createStatement();
                ResultSet s = d.executeQuery("SELECT * FROM usuario WHERE correo = '" + log.jtUsu.getText() + "' and cod_cedu = '" + log.jtContra.getText() + "'");

                while (s.next()) {

                    log.nivel = Integer.parseInt("" + s.getObject(3));
                    System.out.println("nivel11: " + log.nivel);
                    log.cl.cb.UsuActual = "" + log.jtUsu.getText();
                }
                mn.desconectar();
                s.close();
            } catch (Exception ex) {
                log.puto = 1;
                System.out.println(ex);
                new VentanaEmergente(null, true, "No se ha encontrado el usuario");
                //Colocar error de codigo

            }

            try {
                mn.conectar(log.cl.cb);
                Statement d = mn.connection.createStatement();
                ResultSet s = d.executeQuery("SELECT * FROM usuario WHERE correo = '" + log.jtUsu.getText() + "' and cod_cedu = '" + log.jtContra.getText() + "'");

                while (s.next()) {

                    if (log.nivel == 1) {
                        JOptionPane.showMessageDialog(null, "Esta Logueado El Administrador: " + "" + s.getObject(4) + " en la marca temporal " + log.cl.cb.HoraIniSesion);
                        log.cl.cb.usuconectado = "" + log.jtUsu.getText();
                        System.out.println("usuario es " + log.cl.cb.usuconectado);
                        System.out.println("putin " + log.cl.cb.usuconectado);
                    }

                    if (log.nivel == 2) {
                        JOptionPane.showMessageDialog(null, "Esta Logueado El Profesor: " + "" + s.getObject(4) + " en la marca temporal " + log.cl.cb.HoraIniSesion);
                        log.cl.cb.usuconectado = "" + log.jtUsu.getText();
                        System.out.println("putin " + log.cl.cb.usuconectado);
                    }

                    if (log.nivel == 3) {
                        JOptionPane.showMessageDialog(null, "Esta Logueado El Estudiante: " + "" + s.getObject(4) + " en la marca temporal " + log.cl.cb.HoraIniSesion);
                        log.cl.cb.usuconectado = "" + log.jtUsu.getText();
                        System.out.println("putin " + log.cl.cb.usuconectado);
                    }
                }
                mn.desconectar();
                s.close();
            } catch (Exception ex) {
                log.puto = 1;
                System.out.println(ex);
                new VentanaEmergente(null, true, "No se ha encontrado el usuario");
                //Colocar error de codigo

            }

        } else {
            log.puto = 1;
            new VentanaEmergente(null, true, "No se ha digitado algun dato");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public void ModeloMaxPregAdmin(InicioJuegoAdmin ija) {
        this.ija = ija;

        try {
            mn.conectar(ija.ca.ma.cda);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (ija.ca.Numcategoria == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ija.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ija.maxjuego);
                }

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (ija.ca.Numcategoria == 2) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 2 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ija.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ija.maxjuego);
                }

            }

            if (ija.ca.Numcategoria == 3) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 3 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ija.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ija.maxjuego);
                }

            }

            mn.desconectar();
            if (ija.ca.ma.cda.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo6");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public EligeCategoriaCrearAdmin ecca;

    public void ModeloMaxPregAdminCrear(EligeCategoriaCrearAdmin ecca) {
        this.ecca = ecca;

        try {
            mn.conectar(ecca.cma.ma.cda);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (ecca.numCate == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ecca.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMEROpr: " + ecca.maxjuego);
                }

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (ecca.numCate == 2) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 2 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ecca.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMEROpr: " + ecca.maxjuego);
                }

            }

            if (ecca.numCate == 3) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 3 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ecca.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMEROpr: " + ecca.maxjuego);
                }

            }

            mn.desconectar();
            if (ecca.cma.ma.cda.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public EligeCategoriaModiAdmin eda;

    public void ModeloMaxPregAdminpreguntas(EligeCategoriaModiAdmin eda) {
        this.eda = eda;

        try {
            mn.conectar(eda.cma.ma.cda);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (eda.numCate == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    eda.cma.ma.cda.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + eda.cma.ma.cda.maxjuego);
                }

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (eda.numCate == 2) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 2 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    eda.cma.ma.cda.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + eda.cma.ma.cda.maxjuego);
                }

            }

            if (eda.numCate == 3) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 3 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    eda.cma.ma.cda.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + eda.cma.ma.cda.maxjuego);
                }

            }

            mn.desconectar();
            if (eda.cma.ma.cda.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo5");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public void ModeloMaxPregEstu(InicioJuego ij) {
        this.ij = ij;

        try {
            mn.conectar(ij.c.mp.bd);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (ij.c.Numcategoria == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ij.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ij.maxjuego);
                }

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (ij.c.Numcategoria == 2) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 2 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ij.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ij.maxjuego);
                }

            }

            if (ij.c.Numcategoria == 3) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 3 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ij.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + ij.maxjuego);
                }

            }

            mn.desconectar();
            if (ij.c.mp.bd.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public EligeCategoriaCrearProf ecc;

    public void ModeloMaxPregProfCrear(EligeCategoriaCrearProf ecc) {
        this.ecc = ecc;

        try {
            mn.conectar(ecc.cm.menup.cb);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (ecc.numCate == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ecc.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMEROpr: " + ecc.maxjuego);
                }

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (ecc.numCate == 2) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 2 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ecc.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMEROpr: " + ecc.maxjuego);
                }

            }

            if (ecc.numCate == 3) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 3 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    ecc.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMEROpr: " + ecc.maxjuego);
                }

            }

            mn.desconectar();
            if (ecc.cm.menup.cb.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public EligeCategoriaModi cda;

    public void ModeloMaxPregProfe(EligeCategoriaModi cda) {
        this.cda = cda;

        try {
            mn.conectar(cda.cm.menup.cb);
            Statement d = mn.connection.createStatement();

            ////////////////////////////////////////////////////////////////////////////////////////////////////                
            if (cda.numCate == 1) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 1 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    cda.cm.menup.cb.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + cda.cm.menup.cb.maxjuego);
                }

            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
            if (cda.numCate == 2) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 2 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    cda.cm.menup.cb.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + cda.cm.menup.cb.maxjuego);
                }

            }

            if (cda.numCate == 3) {

                ResultSet numPreg1 = d.executeQuery("select count(Nopregunta)\n"
                        + "from preguntas\n"
                        + "where preguntas.categoria LIKE '" + 3 + "'");

                while (numPreg1.next()) {
                    //    ij.num =Integer.parseInt(""+numPreg1);
                    cda.cm.menup.cb.maxjuego = Integer.parseInt(numPreg1.getObject(1) + "");
                    System.out.println("NUMERO: " + cda.cm.menup.cb.maxjuego);
                }

            }

            mn.desconectar();
            if (cda.cm.menup.cb.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo4");

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public PuntajeFinal pf;

    public void ModeloPuntajeFinal(PuntajeFinal pf) {
        this.pf = pf;

        try {

            mn.conectar(pf.ij.c.mp.bd);
            String sql = "INSERT INTO puntuacion("
                    + " puntuacion,respu_corre,respu_incorre,correo_usu, rango_edad, horaini, horafin)"
                    + " VALUES(?,?,?,?,?,?,?)";
            mn.statement = mn.connection.prepareStatement(sql);
            mn.statement.setString(1, "" + pf.ij.c.mp.bd.puntuFi);
            mn.statement.setString(2, "" + pf.ij.c.mp.bd.resCorre);
            mn.statement.setString(3, "" + pf.ij.c.mp.bd.resIncorre);
            mn.statement.setString(4, "" + pf.ij.c.mp.bd.UsuActual);
            System.out.println("usu: " + pf.ij.c.mp.bd.UsuActual);
            mn.statement.setString(5, "" + pf.ij.c.Numcategoria);
            System.out.println("usu: " + pf.ij.c.Numcategoria);
            mn.statement.setString(6, "" + pf.ij.c.mp.bd.HoraIni);
            mn.statement.setString(7, "" + pf.ij.c.mp.bd.Horafin);
            mn.statement.executeUpdate();
            mn.desconectar();

        } catch (Exception ex) {

            System.out.println(ex);

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public PuntajeFinalAdmin pfa;

    public void ModeloPuntajeFinalAdmin(PuntajeFinalAdmin pfa) {
        this.pfa = pfa;

        try {

            mn.conectar(pfa.ija.ca.ma.cda);
            String sql = "INSERT INTO puntuacion("
                    + " puntuacion,respu_corre,respu_incorre,correo_usu, rango_edad, horaini, horafin)"
                    + " VALUES(?,?,?,?,?,?,?)";

            mn.statement = mn.connection.prepareStatement(sql);

            mn.statement.setString(1, "" + pfa.ija.ca.ma.cda.puntuFi);
            mn.statement.setString(2, "" + pfa.ija.ca.ma.cda.resCorre);
            mn.statement.setString(3, "" + pfa.ija.ca.ma.cda.resIncorre);
            mn.statement.setString(4, "" + pfa.ija.ca.ma.cda.UsuActual);
            System.out.println("usu: " + pfa.ija.ca.ma.cda.UsuActual);
            mn.statement.setString(5, "" + pfa.ija.ca.Numcategoria);
            System.out.println("usu: " + pfa.ija.ca.Numcategoria);
            mn.statement.setString(6, "" + pfa.ija.ca.ma.cda.HoraIni);
            mn.statement.setString(7, "" + pfa.ija.ca.ma.cda.Horafin);
            mn.statement.executeUpdate();
            mn.desconectar();

        } catch (Exception ex) {

            System.out.println(ex);

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public PuntuacionAdmin pa;

    public void ModeloPuntuaAdmin(PuntuacionAdmin pa) {
        this.pa = pa;

        if (pa.Numcategoria == 1) {

            try {
                // ubicacion url, user, password, nombre base de datos
                mn.conectar(pa.ma.cda);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 1 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pa.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pa.Numcategoria == 2) {

            try {
                mn.conectar(pa.ma.cda);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 2 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pa.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pa.Numcategoria == 3) {

            try {
                mn.conectar(pa.ma.cda);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 3 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pa.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pa.Numcategoria == 4) {

            try {
                mn.conectar(pa.ma.cda);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion ");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pa.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public PuntuacionProfesor pp;

    public void ModeloPuntuaProfe(PuntuacionProfesor pp) {
        this.pp = pp;

        if (pp.Numcategoria == 1) {

            try {
                mn.conectar(pp.menup.cb);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 1 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pp.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pp.Numcategoria == 2) {

            try {
                mn.conectar(pp.menup.cb);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 2 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pp.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pp.Numcategoria == 3) {

            try {
                mn.conectar(pp.menup.cb);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 3 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pp.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pp.Numcategoria == 4) {

            try {
                mn.conectar(pp.menup.cb);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion ");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pp.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }

                mn.desconectar();
            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public Puntuaciones pu;

    public void ModeloPuntuacionEstu(Puntuaciones pu) {
        this.pu = pu;

        if (pu.Numcategoria == 1) {

            try {
                mn.conectar(pu.mp.bd);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 1 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pu.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 8; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }
                mn.desconectar();
            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pu.Numcategoria == 2) {

            try {
                mn.conectar(pu.mp.bd);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 2 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pu.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }
                mn.desconectar();
            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (pu.Numcategoria == 3) {

            try {
                mn.conectar(pu.mp.bd);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select codigo, puntuacion, rango_edad, respu_corre, respu_incorre, correo_usu, horaini, horafin from puntuacion where rango_edad = '" + 3 + "'");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[8];
                while (rs.next()) {
                    for (int i = 0; i < 8; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    pu.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }
                mn.desconectar();
            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public void ModeloHoraSesion(CrearBD bd) {

        try {
            mn.conectar(bd);
            String sql = "Update usuario set horainisesion=?, horafinsesion=? where correo like '" + bd.usuconectado + "'";

            mn.statement = mn.connection.prepareStatement(sql);
            mn.statement.setString(1, "" + bd.HoraIniSesion);
            mn.statement.setString(2, "" + bd.HorafinSesion);

            mn.statement.executeUpdate();
            mn.desconectar();
        } catch (Exception ds) {
            System.out.println("1 dsa " + ds);
        }

    }

}
