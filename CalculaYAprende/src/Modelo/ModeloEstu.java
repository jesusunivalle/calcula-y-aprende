/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Bd.ConexionBD;
import Vista.CrearEstudiante;
import Vista.TipUsu;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ModeloEstu {

    public CrearEstudiante ce;
    public TipUsu tu;

    public void ModeloEstudiante(CrearEstudiante ce, TipUsu tu) {
        this.ce = ce;
        this.tu = tu;

        if (!ce.jtCorreoEstu.getText().equals("") && !ce.jtCodEstu.getText().equals("") && !ce.jtNom.getText().equals("") && !ce.jtApe.getText().equals("")
                && !ce.jtCodPlan.getText().equals("") && !ce.jtNomPlan.getText().equals("") && !ce.jtFechaNaci.getText().equals("")) {
            ce.puto = 0;
            try {
                ce.puto = 0;

                ConexionBD mn = new ConexionBD();
                mn.conectar(ce.tu.cl.cb);
                String sql = "INSERT INTO usuario(correo,cod_cedu, nombre,"
                        + "apellido, cod_plan, nom_plan,fecha_naci, tipo_usu)"
                        + " VALUES(?,?,?,?,?,?,?,?)";
                mn.statement = mn.connection.prepareStatement(sql);

                ce.e = ce.jtCorreoEstu.getText();
                ce.f = ce.jtCorreoEstu.getText();

                Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                Matcher mather = pattern.matcher(ce.e);

                if (mather.find() == true) {
                    mn.statement.setString(1, ce.f);
                    ce.puto = 0;

                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese un correo valido");
                    ce.puto = 1;
                }

                mn.statement.setString(2, ce.jtCodEstu.getText());
                mn.statement.setString(3, ce.jtNom.getText());
                mn.statement.setString(4, ce.jtApe.getText());
                mn.statement.setString(5, ce.jtCodPlan.getText());
                mn.statement.setString(6, ce.jtNomPlan.getText());

                mn.statement.setString(7, ce.jtFechaNaci.getText());

                if (tu.jcTipoUser.getSelectedItem().equals("Admin")) {
                    mn.statement.setString(8, "" + 1);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Profesor")) {
                    mn.statement.setString(8, "" + 2);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Estudiante")) {
                    mn.statement.setString(8, "" + 3);
                }

                mn.statement.executeUpdate();
                mn.desconectar();

            } catch (com.mysql.jdbc.MysqlDataTruncation ex) {
                ce.puto = 1;
                JOptionPane.showMessageDialog(null, "Porfavor ingrese correctemente la fecha, asi xxxx(año)-xx(mes)-xx(dia)");
            } catch (Exception ex) {
                ce.puto = 1;
                System.out.println(ex);

            }
        } else {
            new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
            ce.puto = 1;
        }
    }
}
