/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Bd.ConexionBD;
import Vista.CrearAdmin;
import Vista.CrearPreguntaAdmin;
import Vista.ModificarPreguntaAdmin;
import Vista.TipUsu;
import Vista.VisuPreguntasAdmin;
import Vista.Visu_EliAdmin;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ModeloAdmi {

    public CrearAdmin ca;
    public TipUsu tu;
    ConexionBD mn = new ConexionBD();

    public void ModeloAdmin(CrearAdmin ca, TipUsu tu) {
        this.ca = ca;
        this.tu = tu;

        if (!ca.jtCorreoAdmin.getText().equals("") && !ca.jtCodAdmin.getText().equals("") && !ca.jtNomAdmin.getText().equals("") && !ca.jtApeAdmin.getText().equals("")) {
            ca.puto = 0;
            try {
                ca.puto = 0;

                mn.conectar(ca.tu.cl.cb);
                String sql = "INSERT INTO usuario(correo,cod_cedu, nombre,"
                        + "apellido, tipo_usu)"
                        + " VALUES(?,?,?,?,?)";

                mn.statement = mn.connection.prepareStatement(sql);

                ca.e = ca.jtCorreoAdmin.getText();
                ca.f = ca.jtCorreoAdmin.getText();

                Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                Matcher mather = pattern.matcher(ca.e);

                if (mather.find() == true) {
                    mn.statement.setString(1, ca.f);
                    ca.puto = 0;
                    System.out.println("dsda");
                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese un correo valido");
                    ca.puto = 1;
                }

                mn.statement.setString(2, ca.jtCodAdmin.getText());
                mn.statement.setString(3, ca.jtNomAdmin.getText());
                mn.statement.setString(4, ca.jtApeAdmin.getText());

                if (tu.jcTipoUser.getSelectedItem().equals("Admin")) {
                    mn.statement.setString(5, "" + 1);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Profesor")) {
                    mn.statement.setString(5, "" + 2);
                }

                if (tu.jcTipoUser.getSelectedItem().equals("Estudiante")) {
                    mn.statement.setString(5, "" + 3);
                }

                mn.statement.executeUpdate();
                mn.desconectar();

            } catch (com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException cv) {
                ca.puto = 1;
                JOptionPane.showMessageDialog(null, "Ingresar un correo o cedula diferente");

            } catch (Exception ex) {
                ca.puto = 1;
                System.out.println(ex);
            }

        } else {
            new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
            ca.puto = 1;
        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public CrearPreguntaAdmin crearpa;

    public void ModeloCrearPregAdmin(CrearPreguntaAdmin crearpa) {

        this.crearpa = crearpa;

        if (crearpa.ecca.numCate == 1) {

            if (!crearpa.jtpreg.getText().equals("") && !crearpa.jtrespuA.getText().equals("") && !crearpa.jtrespuB.getText().equals("")
                    && !crearpa.jtrespuC.getText().equals("") && !crearpa.jtrespuD.getText().equals("")
                    && !crearpa.jtrespuCorre.getText().equals("") && !crearpa.jtexplicaOpc.getText().equals("")
                    && !crearpa.jtexplicaObli.getText().equals("")) {
                crearpa.puto = 0;

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO preguntas(categoria, Nopregunta,pregunta)"
                            + "VALUES       (?,?,?)";
                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, crearpa.jtpreg.getText());

                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ds) {
                    System.out.println("1 dsa " + ds);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";
                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "0");
                    mn.statement.setString(4, (crearpa.jtrespuA.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcA" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";
                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "1");
                    mn.statement.setString(4, (crearpa.jtrespuB.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcB" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";
                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "2");
                    mn.statement.setString(4, (crearpa.jtrespuC.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcC" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "1");
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "3");
                    mn.statement.setString(4, (crearpa.jtrespuD.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcD" + ex);
                }

                if (crearpa.jtrespuA.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuB.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuC.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuD.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (!crearpa.jtrespuA.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuB.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuC.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuD.getText().equals(crearpa.jtrespuCorre.getText())) {
                    crearpa.puto = 2;
                    JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
                }
            } else {
                new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
                crearpa.puto = 1;
            }
        }

        if (crearpa.ecca.numCate == 2) {

            if (!crearpa.jtpreg.getText().equals("") && !crearpa.jtrespuA.getText().equals("") && !crearpa.jtrespuB.getText().equals("")
                    && !crearpa.jtrespuC.getText().equals("") && !crearpa.jtrespuD.getText().equals("")
                    && !crearpa.jtrespuCorre.getText().equals("") && !crearpa.jtexplicaOpc.getText().equals("")
                    && !crearpa.jtexplicaObli.getText().equals("")) {
                crearpa.puto = 0;

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO preguntas(categoria, Nopregunta,pregunta)"
                            + "VALUES       (?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, crearpa.jtpreg.getText());

                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ds) {
                    System.out.println("1 dsa " + ds);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "0");
                    mn.statement.setString(4, (crearpa.jtrespuA.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcA" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "1");
                    mn.statement.setString(4, (crearpa.jtrespuB.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcB" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql
                            = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "2");
                    mn.statement.setString(4, (crearpa.jtrespuC.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcC" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "3");
                    mn.statement.setString(4, (crearpa.jtrespuD.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcD" + ex);
                }

                if (crearpa.jtrespuA.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuB.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuC.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuD.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (!crearpa.jtrespuA.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuB.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuC.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuD.getText().equals(crearpa.jtrespuCorre.getText())) {
                    crearpa.puto = 2;
                    JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
                }
            } else {
                new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
                crearpa.puto = 1;
            }
        }

        if (crearpa.ecca.numCate == 3) {

            if (!crearpa.jtpreg.getText().equals("") && !crearpa.jtrespuA.getText().equals("") && !crearpa.jtrespuB.getText().equals("")
                    && !crearpa.jtrespuC.getText().equals("") && !crearpa.jtrespuD.getText().equals("")
                    && !crearpa.jtrespuCorre.getText().equals("") && !crearpa.jtexplicaOpc.getText().equals("")
                    && !crearpa.jtexplicaObli.getText().equals("")) {
                crearpa.puto = 0;

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO preguntas(categoria, Nopregunta,pregunta)"
                            + "VALUES       (?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, crearpa.jtpreg.getText());

                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ds) {
                    System.out.println("1 dsa " + ds);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "0");
                    mn.statement.setString(4, (crearpa.jtrespuA.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcA" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "1");
                    mn.statement.setString(4, (crearpa.jtrespuB.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcB" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "2");
                    mn.statement.setString(4, (crearpa.jtrespuC.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcC" + ex);
                }

                try {

                    mn.conectar(crearpa.ecca.cma.ma.cda);
                    String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                            + " VALUES     (?,?,?,?,?,?)";

                    mn.statement = mn.connection.prepareStatement(sql);

                    mn.statement.setString(1, "" + crearpa.ecca.numCate);
                    mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
                    mn.statement.setString(3, "3");
                    mn.statement.setString(4, (crearpa.jtrespuD.getText()));
                    mn.statement.setString(5, "");
                    mn.statement.setString(6, "");
                    mn.statement.executeUpdate();
                    mn.desconectar();
                } catch (Exception ex) {
                    System.out.println("opcD" + ex);
                }

                if (crearpa.jtrespuA.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuB.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuC.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (crearpa.jtrespuD.getText().equals(crearpa.jtrespuCorre.getText())) {
                    rescorreAdmi();
                }

                if (!crearpa.jtrespuA.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuB.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuC.getText().equals(crearpa.jtrespuCorre.getText()) && !crearpa.jtrespuD.getText().equals(crearpa.jtrespuCorre.getText())) {
                    crearpa.puto = 2;
                    JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
                }
            } else {
                new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
                crearpa.puto = 1;
            }
        }

    }

    private void rescorreAdmi() {
        try {

            mn.conectar(crearpa.ecca.cma.ma.cda);
            String sql = "INSERT INTO respuestas(categoria, Nopregunta, opciones, respuesta, explicacion, explicacion_opc)"
                    + " VALUES     (?,?,?,?,?,?)";

            mn.statement = mn.connection.prepareStatement(sql);

            mn.statement.setString(1, "" + crearpa.ecca.numCate);
            mn.statement.setString(2, "" + (crearpa.ecca.maxjuego + 1));
            mn.statement.setString(3, "4");
            mn.statement.setString(4, (crearpa.jtrespuCorre.getText()));
            mn.statement.setString(5, crearpa.jtexplicaObli.getText());
            mn.statement.setString(6, crearpa.jtexplicaOpc.getText());
            mn.statement.executeUpdate();
            mn.desconectar();
        } catch (Exception ex) {
            System.out.println("opcD" + ex);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    public ModificarPreguntaAdmin modipa;

    public void ModeloModificarPregAdmin(ModificarPreguntaAdmin modipa) {
        this.modipa = modipa;

        if (modipa.npa.vpa.ecma.numCate == 1) {
            PonerpregAdmin();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
        if (modipa.npa.vpa.ecma.numCate == 2) {
            PonerpregAdmin();
        }

        if (modipa.npa.vpa.ecma.numCate == 3) {
            PonerpregAdmin();
        }

        if (modipa.npa.vpa.ecma.cma.ma.cda.nombre.getText().equals("")) {

            new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
        }

    }

    private void PonerpregAdmin() {

        if (!modipa.jtpreg.getText().equals("") && !modipa.jtrespuA.getText().equals("") && !modipa.jtrespuB.getText().equals("")
                && !modipa.jtrespuC.getText().equals("") && !modipa.jtrespuD.getText().equals("")
                && !modipa.jtrespuCorre.getText().equals("") && !modipa.jtexplicaOpc.getText().equals("")
                && !modipa.jtexplicaObli.getText().equals("")) {
            modipa.puto = 0;

            try {

                mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
                String sql = "Update preguntas set pregunta=? WHERE Nopregunta LIKE'"
                        + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modipa.jtpreg.getText());

                mn.statement.executeUpdate();
                mn.desconectar();

                System.out.println("actu p");
            } catch (Exception ex) {
                System.out.println(ex);
                new VentanaEmergente(null, true, "No se ha encontrado el codigo7");

            }

            //////////////////////////////////////////Opcion A////////////////////////////////////////////////////
            try {
                mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
                String sql = "Update respuestas set respuesta=? WHERE Nopregunta LIKE'"
                        + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate
                        + "'and opciones LIKE'" + 0 + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modipa.jtrespuA.getText());

                mn.statement.executeUpdate();
                mn.desconectar();
                System.out.println("actu A");

            } catch (Exception ex) {
                System.out.println("opcA" + ex);
            }

            //////////////////////////////////////////Opcion B////////////////////////////////////////////////////
            try {

                mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
                String sql = "Update respuestas set respuesta=? WHERE Nopregunta LIKE'"
                        + modipa.npa.Numpreg + "'and categoria LIKE '"
                        + modipa.npa.vpa.ecma.numCate + "'and opciones LIKE'" + 1 + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modipa.jtrespuB.getText());

                mn.statement.executeUpdate();
                mn.desconectar();

                System.out.println("actu b");
            } catch (Exception ex) {
                System.out.println("opcB" + ex);
            }

            //////////////////////////////////////////Opcion C////////////////////////////////////////////////////
            try {

                mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
                String sql = "Update respuestas set respuesta=? WHERE Nopregunta LIKE'"
                        + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate
                        + "'and opciones LIKE'" + 2 + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modipa.jtrespuC.getText());

                mn.statement.executeUpdate();
                mn.desconectar();
                System.out.println("actu c");
            } catch (Exception ex) {
                System.out.println("opcC" + ex);
            }

            //////////////////////////////////////////Opcion D////////////////////////////////////////////////////
            try {

                mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
                String sql = "Update respuestas set respuesta=? WHERE Nopregunta LIKE'" + modipa.npa.Numpreg
                        + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate
                        + "'and opciones LIKE'" + 3 + "'";

                mn.statement = mn.connection.prepareStatement(sql);

                mn.statement.setString(1, modipa.jtrespuD.getText());

                mn.statement.executeUpdate();
                System.out.println("actu d");
            } catch (Exception ex) {
                System.out.println("opcD" + ex);
            }

            //////////////////////////////////////////Opcion Corre////////////////////////////////////////////////////
            if (modipa.jtrespuA.getText().equals(modipa.jtrespuCorre.getText())) {
                modipa.puto = 0;
                rescorreModAdmin();
            }

            if (modipa.jtrespuB.getText().equals(modipa.jtrespuCorre.getText())) {
                modipa.puto = 0;
                rescorreModAdmin();
            }

            if (modipa.jtrespuC.getText().equals(modipa.jtrespuCorre.getText())) {
                modipa.puto = 0;
                rescorreModAdmin();
            }

            if (modipa.jtrespuD.getText().equals(modipa.jtrespuCorre.getText())) {
                modipa.puto = 0;
                rescorreModAdmin();
            }

            if (!modipa.jtrespuA.getText().equals(modipa.jtrespuCorre.getText()) && !modipa.jtrespuB.getText().equals(modipa.jtrespuCorre.getText()) && !modipa.jtrespuC.getText().equals(modipa.jtrespuCorre.getText()) && !modipa.jtrespuD.getText().equals(modipa.jtrespuCorre.getText())) {
                modipa.puto = 2;
                JOptionPane.showMessageDialog(null, "Verifique que la respuesta correcta se escuentra dentro de las opciones");
            }

        } else {
            new VentanaEmergente(null, true, "Algun campo no ha sido ingresado");
            modipa.puto = 1;
        }
    }

    private void rescorreModAdmin() {
        try {

            mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
            String sql = "Update respuestas set respuesta=?, explicacion=?,explicacion_opc=? WHERE Nopregunta LIKE'"
                    + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate
                    + "'and opciones LIKE'" + 4 + "'";

            mn.statement = mn.connection.prepareStatement(sql);

            mn.statement.setString(1, modipa.jtrespuCorre.getText());
            mn.statement.setString(2, modipa.jtexplicaObli.getText());
            mn.statement.setString(3, modipa.jtexplicaOpc.getText());

            mn.statement.executeUpdate();
            mn.desconectar();
            System.out.println("actu cor");
        } catch (Exception ex) {
            System.out.println("opcCorre" + ex);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    public void ModeloPonerPregAdmin(ModificarPreguntaAdmin modipa) {
        this.modipa = modipa;

        if (modipa.npa.vpa.ecma.numCate == 1) {
            PonerpregAdmi();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
        if (modipa.npa.vpa.ecma.numCate == 2) {
            PonerpregAdmi();
        }

        if (modipa.npa.vpa.ecma.numCate == 3) {
            PonerpregAdmi();
        }

        if (modipa.npa.vpa.ecma.cma.ma.cda.nombre.getText().equals("")) {

            new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
        }

    }

    private void PonerpregAdmi() {

        try {
            modipa.jtpreg.setText("");
            modipa.jtrespuA.setText("");
            modipa.jtrespuB.setText("");
            modipa.jtrespuC.setText("");
            modipa.jtrespuD.setText("");
            modipa.jtrespuCorre.setText("");
            modipa.jtexplicaOpc.setText("");
            modipa.jtexplicaObli.setText("");

            mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM preguntas WHERE Nopregunta LIKE'" + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate + "'");

            while (s.next()) {
                modipa.jtpreg.setText(s.getObject(3) + "");

            }
            mn.desconectar();
            s.close();
            if (modipa.npa.vpa.ecma.cma.ma.cda.nombre.getText().equals("")) {

                new VentanaEmergente(null, true, "No se ha encontrado el codigo1");
            }

        } catch (Exception ex) {
            System.out.println(ex);
            new VentanaEmergente(null, true, "No se ha encontrado el codigo2");

        }
        //////////////////////////////////////////Opcion A////////////////////////////////////////////////////
        try {
            mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate + "'and opciones LIKE'" + 0 + "'");

            while (s.next()) {
                modipa.jtrespuA.setText(s.getObject(4) + "");
            }
            mn.desconectar();
            s.close();
        } catch (Exception ex) {
            System.out.println("opcA" + ex);
        }

        //////////////////////////////////////////Opcion B////////////////////////////////////////////////////
        try {
            mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate + "'and opciones LIKE'" + 1 + "'");

            while (s.next()) {
                modipa.jtrespuB.setText(s.getObject(4) + "");
            }
            mn.desconectar();
            s.close();
        } catch (Exception ex) {
            System.out.println("opcB" + ex);
        }

        //////////////////////////////////////////Opcion C////////////////////////////////////////////////////
        try {
            mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate + "'and opciones LIKE'" + 2 + "'");

            while (s.next()) {
                modipa.jtrespuC.setText(s.getObject(4) + "");
            }
            mn.desconectar();
            s.close();
        } catch (Exception ex) {
            System.out.println("opcC" + ex);
        }

        //////////////////////////////////////////Opcion D////////////////////////////////////////////////////
        try {
            mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate + "'and opciones LIKE'" + 3 + "'");

            while (s.next()) {
                modipa.jtrespuD.setText(s.getObject(4) + "");
            }
            mn.desconectar();
            s.close();
        } catch (Exception ex) {
            System.out.println("opcD" + ex);
        }

        //////////////////////////////////////////Opcion Corre////////////////////////////////////////////////////
        try {
            mn.conectar(modipa.npa.vpa.ecma.cma.ma.cda);
            Statement d = mn.connection.createStatement();
            ResultSet s = d.executeQuery("SELECT * FROM respuestas WHERE Nopregunta LIKE'" + modipa.npa.Numpreg + "'and categoria LIKE '" + modipa.npa.vpa.ecma.numCate + "'and opciones LIKE'" + 4 + "'");

            while (s.next()) {
                modipa.jtrespuCorre.setText(s.getObject(4) + "");
                modipa.jtexplicaObli.setText(s.getObject(5) + "");
                modipa.jtexplicaOpc.setText(s.getObject(6) + "");
            }
            mn.desconectar();
            s.close();
        } catch (Exception ex) {
            System.out.println("opcCorre" + ex);
        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public VisuPreguntasAdmin vpa;

    public void ModeloVisuPregAdmin(VisuPreguntasAdmin vpa) {
        this.vpa = vpa;

        ////////////////////////////////////////////////////////////////////////////////////////////////////                
        if (vpa.ecma.numCate == 1) {
            EscogePreg();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
        if (vpa.ecma.numCate == 2) {
            EscogePreg();
        }

        if (vpa.ecma.numCate == 3) {
            EscogePreg();
        }

        if (vpa.ecma.cma.ma.cda.nombre.getText().equals("")) {

            new VentanaEmergente(null, true, "No se ha encontrado el nombre de la BD");
        }

    }

    private void EscogePreg() {

        try {
            mn.conectar(vpa.ecma.cma.ma.cda);
            Statement s = mn.connection.createStatement();
            ResultSet rs = s.executeQuery("select Nopregunta, pregunta from preguntas where categoria = '" + vpa.ecma.numCate + "'");

            //aqui añadir denuevo el modelo, con columna etc
            Object fila[] = new Object[2];
            while (rs.next()) {
                for (int i = 0; i < 2; i++) {
                    fila[i] = rs.getObject(i + 1);
                }

                vpa.modelo.addRow(fila);
            }

            while (rs.next()) {
                for (int i = 1; i <= 9; i++) //sin el bucle//
                //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                {
                    System.out.println(rs.getObject(i) + " ");
                }

                System.out.println();
            }

            mn.desconectar();
            rs.close();
        } catch (Exception e) {
            System.out.println("Error " + e);
        }

    }

    ////////////////////////////////////////////////////////////////////////////
    public Visu_EliAdmin vea;

    public void ModeloVisu_EliAdmin(Visu_EliAdmin vea) {
        this.vea = vea;

        if (vea.Mostrar_Eliminar == 1) {
            try {
                mn.conectar(vea.ma.cda);
                Statement s = mn.connection.createStatement();
                ResultSet rs = s.executeQuery("select correo, cod_cedu, nombre, apellido, cod_plan, nom_plan, fecha_naci from usuario");

                //aqui añadir denuevo el modelo, con columna etc
                Object fila[] = new Object[7];
                while (rs.next()) {
                    for (int i = 0; i < 7; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }

                    vea.modelo.addRow(fila);
                }

                while (rs.next()) {
                    for (int i = 1; i <= 9; i++) //sin el bucle//
                    //System.out.println(rs.getObject(1)+ "  " +rs.getObject(2)+  "  " +rs.getObject(3));
                    {
                        System.out.println(rs.getObject(i) + " ");
                    }

                    System.out.println();
                }
                mn.desconectar();
                rs.close();

            } catch (Exception e) {
                System.out.println("Error " + e);
            }

        }

        if (vea.Mostrar_Eliminar == 2) {

            String k;
            String p = "";

            for (int i = 0; i < vea.modelo.getRowCount(); i++) {

                k = vea.modelo.getValueAt(i, 7) + "";

                if (k.equals("true")) {
                    p = "" + (vea.modelo.getValueAt(i, 0));
                    System.out.println("correo: " + p);
                    vea.modelo.removeRow(i);

                    try {

                        mn.conectar(ca.tu.cl.cb);
                        String sql = "Delete from usuario where correo LIKE '" + p + "'";

                        mn.statement = mn.connection.prepareStatement(sql);

                        System.out.println("casi");
                        mn.statement.executeUpdate();
                        mn.desconectar();
                        System.out.println("borro");
                    } catch (Exception cv) {

                    }
                    i--;
                }
            }

        }

    }

    ////////////////////////////////////////////////////////////////////////////
}
