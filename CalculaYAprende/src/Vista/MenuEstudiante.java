package Vista;


import Controlador.ControladorEstudiante;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;

public class MenuEstudiante extends JFrame {

    public JButtonMenu jbIniJuego, jbPuntuacion, jbInstrucciones, jbcerrarSc;
    public CrearBD bd;
    String nombre2;
    JComboBox<String> jcTema;
    javax.swing.JLabel jLabel1;

    public MenuEstudiante(CrearBD bd) {

        this.bd = bd;

        setSize(750, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        crearGui();

        setVisible(true);

    }

    private void crearGui() {

        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/PrinciPri.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.setBounds(200, 25, 350, 250);
        add(jLabel1);

        jbIniJuego = new JButtonMenu(100, 300, 200, 50, "Inicio Juego");
        add(jbIniJuego);

        jbPuntuacion = new JButtonMenu(450, 300, 200, 50, "Puntuacion");
        add(jbPuntuacion);

        jbInstrucciones = new JButtonMenu(100, 400, 200, 50, "Instrucciones");
        add(jbInstrucciones);

        jbcerrarSc = new JButtonMenu(450, 400, 200, 50, "Cerrar sesion");
        add(jbcerrarSc);

        ControladorEstudiante cca = new ControladorEstudiante();
        cca.ControladorMenuEstudiante(this);

        jbIniJuego.addActionListener(cca);
        jbPuntuacion.addActionListener(cca);
        jbInstrucciones.addActionListener(cca);
        jbcerrarSc.addActionListener(cca);

        System.out.println("usu cya: " + bd.UsuActual);

        jcTema = new JComboBox<String>();
        jcTema.setToolTipText("Cambia el look de Gamifi");
        jcTema.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jcTema.addItem("Escoge tu look favorito");
        jcTema.addItem("Aero");
        jcTema.addItem("Aluminium");
        jcTema.addItem("Fast");
        jcTema.addItem("McWin");

        jcTema.setBounds(500, 0, 150, 25);
        jcTema.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                evento_jcTema();
            }
        });
        add(jcTema);
    }

    protected void aplicarLook(String look) {
        try {
            if (look.equals("Aluminium")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
            }
            if (look.equals("Aero")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            }
            if (look.equals("McWin")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
            }
            if (look.equals("Fast")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
            }

            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al tratar de cargar el paquete 'com.jtattoo'.\n\n"
                    + "Se tomara el LookAndFeel por defecto.", "Error de paquete", JOptionPane.WARNING_MESSAGE);

            JFrame.setDefaultLookAndFeelDecorated(true);
        }
    }

    protected void evento_jcTema() {
        String look = jcTema.getSelectedItem().toString();
        System.out.println(look);
        if (!look.equals("Escoge tu look favorito")) {
            aplicarLook(look);
        }
    }

}
