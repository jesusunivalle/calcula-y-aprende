/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;


import Controlador.ControladorProfesor;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CrearProfesor extends JFrame {

    public JTextField jtNomProfe, jtApeProfe, jtCodProfe, jtCorreoProfe;
    public JButtonMenu jbSiguiente;
    public TipUsu tu;
    public String e = "", f = "";
    public int puto = 0;

    public CrearProfesor(TipUsu tu) {

        this.tu = tu;

        setSize(500, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        crearGui();

        setVisible(true);

    }

    private void crearGui() {

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Profesor");
        jlt.setBounds(175, 50, 150, 50);
        add(jlt);

        jbSiguiente = new JButtonMenu(350, 350, 100, 50, "Sig");
        add(jbSiguiente);

        JLabel jlNomProfe = new JLabel("Nombre");
        jlNomProfe.setBounds(50, 125, 150, 20);
        add(jlNomProfe);
        jtNomProfe = new JTextField();
        jtNomProfe.setBounds(50, 150, 150, 30);
        add(jtNomProfe);

        JLabel jlApeProfe = new JLabel("Apellido");
        jlApeProfe.setBounds(300, 125, 150, 20);
        add(jlApeProfe);
        jtApeProfe = new JTextField();
        jtApeProfe.setBounds(300, 150, 150, 30);
        add(jtApeProfe);

        JLabel jlCodProfe = new JLabel("Cedula");
        jlCodProfe.setBounds(50, 225, 150, 20);
        add(jlCodProfe);
        jtCodProfe = new JTextField();
        jtCodProfe.setBounds(50, 250, 150, 30);
        add(jtCodProfe);

        JLabel jlCorreoProfe = new JLabel("Correo");
        jlCorreoProfe.setBounds(300, 225, 150, 20);
        add(jlCorreoProfe);
        jtCorreoProfe = new JTextField();
        jtCorreoProfe.setBounds(300, 250, 150, 30);
        add(jtCorreoProfe);

        ControladorProfesor ccp = new ControladorProfesor();
        ccp.ControladorCrearProfesor(this);
        jbSiguiente.addActionListener(ccp);

    }

}
