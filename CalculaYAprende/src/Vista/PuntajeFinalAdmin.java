/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorJuego;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

public class PuntajeFinalAdmin extends JFrame {

    public JButtonMenu jbSiguiente;
    public InicioJuegoAdmin ija;

    public PuntajeFinalAdmin(InicioJuegoAdmin ija) {
        this.ija = ija;
        setSize(500, 350);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        jbSiguiente = new JButtonMenu(350, 250, 100, 50, "sig");
        add(jbSiguiente);

        ControladorJuego cpfa = new ControladorJuego();
        cpfa.ControladorPuntajeFinalAdmin(this);
        jbSiguiente.addActionListener(cpfa);

        JLabelTitulo jltitu = new JLabelTitulo(this.getWidth(), "<html>" + "Puntuacion Final" + "</html>");
        jltitu.setBounds(100, 50, 300, 50);
        add(jltitu);

        JLabelTitulo jlexplica = new JLabelTitulo(this.getWidth(), "<html>" + ija.ca.ma.cda.puntuFi + "</html>");
        jlexplica.setBounds(150, 150, 200, 50);
        add(jlexplica);

    }

}
