/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorJuego;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.*;

public class TipUsu extends JFrame {

    public JButtonMenu jbAtras, jbSiguiente;
    public JComboBox jcTipoUser;
    public JLabel jLabel2;
    public CrearLogin cl;

    public TipUsu(CrearLogin cl) {

        this.cl = cl;

        setSize(550, 250);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        crearGui();

        setVisible(true);

    }

    private void crearGui() {

        jbAtras = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbAtras);

        jbSiguiente = new JButtonMenu(400, 50, 100, 50, "Sig");
        add(jbSiguiente);

        JLabel jlTipo = new JLabel("Tipo de usuario:");
        jlTipo.setBounds(200, 125, 150, 20);
        add(jlTipo);
        jcTipoUser = new JComboBox<String>();
        jcTipoUser.addItem("Admin");
        jcTipoUser.addItem("Profesor");
        jcTipoUser.addItem("Estudiante");
        jcTipoUser.setBounds(150, 150, 250, 30);
        add(jcTipoUser);

        ControladorJuego ctu = new ControladorJuego();
        ctu.ControladorTipUsu(this);
        jbAtras.addActionListener(ctu);
        jbSiguiente.addActionListener(ctu);

        ///////////////////////////////////////////////////////////////////////////////
    }

}
