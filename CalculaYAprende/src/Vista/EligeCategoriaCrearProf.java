/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;


import Controlador.ControladorProfesor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class EligeCategoriaCrearProf extends JFrame {

    public JButtonMenu jbsiguiente, jbatras;
    public JComboBox jcTipoUsu;
    public CrearModificar cm;
    public int numCate = 0;
    public int maxjuego;

    public EligeCategoriaCrearProf(CrearModificar cm) {
        this.cm = cm;
        setSize(450, 350);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Elige la categoria"/*this.getTile*/);
        jlt.setBounds(50, 50, 350, 50);
        add(jlt);

        /* JLabel jlTipo = new JLabel("Tipo de usuario:");
             jlTipo.setBounds(50, 75, 350, 75);
         add(jlTipo);*/
        jbsiguiente = new JButtonMenu(300, 250, 100, 50, "sig");
        add(jbsiguiente);

        jbatras = new JButtonMenu(50, 250, 100, 50, "Atras");
        add(jbatras);

        jcTipoUsu = new JComboBox<String>();
        jcTipoUsu.addItem("1");
        jcTipoUsu.addItem("2");
        jcTipoUsu.addItem("3");
        jcTipoUsu.setBounds(200, 150, 50, 30);
        add(jcTipoUsu);

        ControladorProfesor cecc = new ControladorProfesor();
        cecc.ControladorEligeCateCrearProf(this);
        jbsiguiente.addActionListener(cecc);
        jbatras.addActionListener(cecc);
    }
}
