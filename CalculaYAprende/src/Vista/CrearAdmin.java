/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorAdmin;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CrearAdmin extends JFrame {

    public JTextField jtNomAdmin, jtApeAdmin, jtCodAdmin, jtCorreoAdmin;
    public JButtonMenu jbSiguiente;
    public TipUsu tu;
    public String e = "", f = "";
    public int puto = 0;

    public CrearAdmin(TipUsu tu) {

        this.tu = tu;

        setSize(500, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        crearGui();

        setVisible(true);

    }

    private void crearGui() {

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Administrador");
        jlt.setBounds(175, 50, 175, 50);
        add(jlt);

        jbSiguiente = new JButtonMenu(350, 350, 100, 50, "Sig");
        add(jbSiguiente);

        JLabel jlNomAdmin = new JLabel("Nombre");
        jlNomAdmin.setBounds(50, 125, 150, 20);
        add(jlNomAdmin);
        jtNomAdmin = new JTextField();
        jtNomAdmin.setBounds(50, 150, 150, 30);
        add(jtNomAdmin);

        JLabel jlApeAdmin = new JLabel("Apellido");
        jlApeAdmin.setBounds(300, 125, 150, 20);
        add(jlApeAdmin);
        jtApeAdmin = new JTextField();
        jtApeAdmin.setBounds(300, 150, 150, 30);
        add(jtApeAdmin);

        JLabel jlCodAdmin = new JLabel("Cedula");
        jlCodAdmin.setBounds(50, 225, 150, 20);
        add(jlCodAdmin);
        jtCodAdmin = new JTextField();
        jtCodAdmin.setBounds(50, 250, 150, 30);
        add(jtCodAdmin);

        JLabel jlCorreoAdmin = new JLabel("Correo");
        jlCorreoAdmin.setBounds(300, 225, 150, 20);
        add(jlCorreoAdmin);
        jtCorreoAdmin = new JTextField();
        jtCorreoAdmin.setBounds(300, 250, 150, 30);
        add(jtCorreoAdmin);

        ControladorAdmin cca = new ControladorAdmin();
        cca.ControladorCrearAdmin(this);
        jbSiguiente.addActionListener(cca);
    }

}
