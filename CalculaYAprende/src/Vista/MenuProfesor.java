/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorProfesor;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

public class MenuProfesor extends JFrame {

    public JButtonMenu jbvisuEstu, jbpuntuacion, jbcrearModi, jbcerrarSesion, jbInstrucciones;
    public CrearBD cb;
    JComboBox<String> jcTema;
    javax.swing.JLabel jLabel1;

    public MenuProfesor(CrearBD cb) {
        this.cb = cb;

        setSize(650, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/Princi.jpeg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.setBounds(180, 25, 280, 200);
        add(jLabel1);

        jbvisuEstu = new JButtonMenu(100, 250, 200, 50, "Ver estudiantes");
        add(jbvisuEstu);

        jbpuntuacion = new JButtonMenu(350, 250, 200, 50, "Puntuacion");
        add(jbpuntuacion);

        jbcrearModi = new JButtonMenu(100, 350, 200, 50, "Crear/Modificar");
        add(jbcrearModi);

        jbcerrarSesion = new JButtonMenu(350, 350, 200, 50, "cerrar sesion");
        add(jbcerrarSesion);

        jbInstrucciones = new JButtonMenu(0, 0, 100, 50, "Instrucciones");
        add(jbInstrucciones);

        ControladorProfesor cmp = new ControladorProfesor();
        cmp.ControladorMenuProfesor(this);
        jbvisuEstu.addActionListener(cmp);
        jbpuntuacion.addActionListener(cmp);
        jbcrearModi.addActionListener(cmp);
        jbcerrarSesion.addActionListener(cmp);
        jbInstrucciones.addActionListener(cmp);

        jcTema = new JComboBox<String>();
        jcTema.setToolTipText("Cambia el look de Gamifi");
        jcTema.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jcTema.addItem("Escoge tu look favorito");
        jcTema.addItem("Aero");
        jcTema.addItem("Aluminium");
        jcTema.addItem("Fast");
        jcTema.addItem("McWin");

        jcTema.setBounds(500, 0, 150, 25);
        jcTema.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                evento_jcTema();
            }
        });
        add(jcTema);

    }

    protected void aplicarLook(String look) {
        try {
            if (look.equals("Aluminium")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
            }
            if (look.equals("Aero")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            }
            if (look.equals("McWin")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
            }
            if (look.equals("Fast")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
            }

            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al tratar de cargar el paquete 'com.jtattoo'.\n\n"
                    + "Se tomara el LookAndFeel por defecto.", "Error de paquete", JOptionPane.WARNING_MESSAGE);

            JFrame.setDefaultLookAndFeelDecorated(true);
        }
    }

    protected void evento_jcTema() {
        String look = jcTema.getSelectedItem().toString();
        System.out.println(look);
        if (!look.equals("Escoge tu look favorito")) {
            aplicarLook(look);
        }
    }
}
