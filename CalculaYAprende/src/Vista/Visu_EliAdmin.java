package Vista;

import Controlador.ControladorAdmin;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class Visu_EliAdmin extends JFrame {

    public JButtonMenu jbEliminar, jbatras, jbTodos;
    //public CalculaYAprende mp; 
    public int Numcategoria = 0;
    public JTable tabla;
    public DefaultTableModel modelo;
    public MenuAdmin ma;
    public int Mostrar_Eliminar = 0;

    public Visu_EliAdmin(MenuAdmin ma) {

        super(" Visualizar_Eliminar ");
        this.ma = ma;

        setSize(750, 550);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Visualizar_Eliminar"/*this.getTile*/);
        add(jlt);

        ////////////////////////////////////////////////////////////////////
        modelo = new DefaultTableModel() {

            public Class getColumnClass(int colum) {

                if (colum == 7) {

                    return Boolean.class;

                }
                return Object.class;
            }

            public boolean isCellEditable(int fila, int colum) {

                if (colum == 7) {

                    return true;

                }
                return false;
            }
        };
        Dimension n = new Dimension(480, 200);

        tabla = new JTable(modelo);

        modelo.addColumn("Correo");
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Apellido");
        modelo.addColumn("Cod_Plan");
        modelo.addColumn("Nombre_Plan");
        modelo.addColumn("Fecha_Nacimiento");
        modelo.addColumn("Eliminar");

        JScrollPane js = new JScrollPane(tabla, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        c.add(js);

        jbatras = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbatras);

        jbTodos = new JButtonMenu(550, 400, 150, 50, "Mostrar Usuarios");
        add(jbTodos);

        jbEliminar = new JButtonMenu(550, 400, 150, 50, "Eliminar Usuarios");
        add(jbEliminar);

        ControladorAdmin cvea = new ControladorAdmin();
        cvea.ControladorVisu_EliAdmin(this);
        jbatras.addActionListener(cvea);
        jbTodos.addActionListener(cvea);
        jbEliminar.addActionListener(cvea);

        ///////////////////////////////////////////////////////
    }

}
