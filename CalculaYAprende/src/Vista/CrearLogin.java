/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;


import Controlador.ControladorJuego;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class CrearLogin extends JFrame {

    public JButtonMenu jbCrearU, jbLogin, jbSalir;
    public CrearBD cb;
    javax.swing.JLabel jLabel1;
    JComboBox<String> jcTema;

    public CrearLogin(CrearBD cb) {

        this.cb = cb;

        setSize(500, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        crearGui();

        setVisible(true);

    }

    private void crearGui() {

        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/Princi.jpeg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.setBounds(110, 25, 280, 200);
        add(jLabel1);

        jbCrearU = new JButtonMenu(50, 250, 150, 50, "Crear Usuario");
        add(jbCrearU);

        jbLogin = new JButtonMenu(300, 250, 150, 50, "Loguearse");
        add(jbLogin);

        jbSalir = new JButtonMenu(175, 350, 150, 50, "Salir");
        add(jbSalir);

        ControladorJuego ccl = new ControladorJuego();
        ccl.ControladorCrearLogin(this);
        jbCrearU.addActionListener(ccl);
        jbLogin.addActionListener(ccl);
        jbSalir.addActionListener(ccl);

        jcTema = new JComboBox<String>();
        jcTema.setToolTipText("Cambia el look de Gamifi");
        jcTema.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jcTema.addItem("Escoge tu look favorito");
        jcTema.addItem("Aero");
        jcTema.addItem("Aluminium");
        jcTema.addItem("Fast");
        jcTema.addItem("McWin");

        jcTema.setBounds(350, 0, 150, 25);
        jcTema.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                evento_jcTema();
            }
        });
        add(jcTema);

    }

    protected void aplicarLook(String look) {
        try {
            if (look.equals("Aluminium")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
            }
            if (look.equals("Aero")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            }
            if (look.equals("McWin")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
            }
            if (look.equals("Fast")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
            }

            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al tratar de cargar el paquete 'com.jtattoo'.\n\n"
                    + "Se tomara el LookAndFeel por defecto.", "Error de paquete", JOptionPane.WARNING_MESSAGE);

            JFrame.setDefaultLookAndFeelDecorated(true);
        }
    }

    protected void evento_jcTema() {
        String look = jcTema.getSelectedItem().toString();
        System.out.println(look);
        if (!look.equals("Escoge tu look favorito")) {
            aplicarLook(look);
        }
    }

}
