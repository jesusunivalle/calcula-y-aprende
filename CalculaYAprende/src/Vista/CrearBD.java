/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorJuego;
import java.awt.Container;
import java.awt.FlowLayout;
import java.io.File;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CrearBD extends JFrame {

    public JButton guardarxd;
    public File base = new File("Base de datos.txt");
    public File guardar = new File("CrearBD.txt");
    // public String baseactual="";
    public int cont = 0;
    public JTextField nombre;
    public int Respuesta = 0;
    public String SopcionA = "", SopcionB = "", SopcionC = "", SopcionD = "", Srespuesta = "", Spregunta = "", SExplicaObli = "", SExplicaOpc = "";
    public int resCorre = 0, resIncorre = 0, numPreg = 0;
    public int puntuTempCorre = 0, puntuTempIncorre = 0, puntuFi = 0, puntuTemp = 0;
    public String UsuActual = "";
    public int maxjuego;
    public String HoraIni, Horafin;
    public String HoraIniSesion, HorafinSesion;
    public String usuconectado;

    public CrearBD() {

        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        JLabel l1 = new JLabel("<html>Nombre de la Base de datos a Crear <br> (Recuerda que puedes utilizar una ya creada.)"
                + "</html>");

        nombre = new JTextField(13);
        guardarxd = new JButton("Guardar");

        c.add(l1);
        c.add(nombre);
        c.add(guardarxd);

        setSize(280, 160);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setVisible(true);

        ControladorJuego ccb = new ControladorJuego();
        ccb.ControladorCrearBD(this);
        guardarxd.addActionListener(ccb);

    }

    public static void main(String[] args) {
        new CrearBD();
    }

}
