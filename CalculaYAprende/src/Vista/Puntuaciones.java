package Vista;

import Controlador.ControladorJuego;
import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class Puntuaciones extends JFrame {

    public JButtonMenu jbCategoria1, jbCategoria2, jbCategoria3, jbatras;
    public MenuEstudiante mp;
    public int Numcategoria = 0;
    public JTable tabla;
    public DefaultTableModel modelo;

    public Puntuaciones(MenuEstudiante mp) {
        super(" Puntuaciones ");
        this.mp = mp;

        setSize(700, 550);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Puntuaciones"/*this.getTile*/);
        add(jlt);

        ////////////////////////////////////////////////////////////////////
        modelo = new DefaultTableModel() {

            public Class getColumnClass(int colum) {

                if (colum == 8) {

                    return Boolean.class;

                }
                return Object.class;
            }

            public boolean isCellEditable(int fila, int colum) {

                if (colum == 8) {

                    return true;

                }
                return false;
            }
        };

        tabla = new JTable(modelo);

        modelo.addColumn("Codigo");
        modelo.addColumn("Puntuacion");
        modelo.addColumn("Rango_Edad");
        modelo.addColumn("Respu_Corre");
        modelo.addColumn("Respu_Incorre");
        modelo.addColumn("Correo_Usu");
        modelo.addColumn("HoraIni");
        modelo.addColumn("HoraFin");

    

        JScrollPane js = new JScrollPane(tabla, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        c.add(js);

            TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(5).setPreferredWidth(200);
        columnModel.getColumn(7).setPreferredWidth(190);
        columnModel.getColumn(6).setPreferredWidth(190);
        
        jbatras = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbatras);

        jbCategoria1 = new JButtonMenu(50, 400, 150, 50, "1 Categoria");
        add(jbCategoria1);

        jbCategoria2 = new JButtonMenu(300, 400, 150, 50, "2 Categoria");
        add(jbCategoria2);

        jbCategoria3 = new JButtonMenu(550, 400, 150, 50, "3 Categoria");
        add(jbCategoria3);

        ControladorJuego cpu = new ControladorJuego();
        cpu.ControladorPuntuacionEstu(this);
        jbatras.addActionListener(cpu);
        jbCategoria1.addActionListener(cpu);
        jbCategoria2.addActionListener(cpu);
        jbCategoria3.addActionListener(cpu);

        ///////////////////////////////////////////////////////
    }

}
