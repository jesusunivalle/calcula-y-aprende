/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorJuego;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class PuntuacionProfesor extends JFrame {

    public JButtonMenu jbCategoria1, jbCategoria2, jbCategoria3, jbatras, jbTodos;
    //public CalculaYAprende mp; 
    public int Numcategoria = 0;
    public JTable tabla;
    public DefaultTableModel modelo;
    public MenuProfesor menup;

    public PuntuacionProfesor(MenuProfesor menup) {
        super(" Puntuaciones ");
        this.menup = menup;

        setSize(700, 550);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Puntuaciones"/*this.getTile*/);
        add(jlt);

        ////////////////////////////////////////////////////////////////////
        modelo = new DefaultTableModel() {

            public Class getColumnClass(int colum) {

                if (colum == 8) {

                    return Boolean.class;

                }
                return Object.class;
            }

            public boolean isCellEditable(int fila, int colum) {

                if (colum == 8) {

                    return true;

                }
                return false;
            }
        };
        Dimension n = new Dimension(480, 200);

        tabla = new JTable(modelo);

        modelo.addColumn("Codigo");
        modelo.addColumn("Puntuacion");
        modelo.addColumn("Rango_Edad");
        modelo.addColumn("Respu_Corre");
        modelo.addColumn("Respu_Incorre");
        modelo.addColumn("Correo_Usu");
        modelo.addColumn("HoraIni");
        modelo.addColumn("HoraFin");

        JScrollPane js = new JScrollPane(tabla, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        c.add(js);

        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(5).setPreferredWidth(200);
        columnModel.getColumn(7).setPreferredWidth(190);
        columnModel.getColumn(6).setPreferredWidth(190);

        jbatras = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbatras);

        jbCategoria1 = new JButtonMenu(50, 400, 150, 50, "1 Categoria");
        add(jbCategoria1);

        jbCategoria2 = new JButtonMenu(300, 400, 150, 50, "2 Categoria");
        add(jbCategoria2);

        jbCategoria3 = new JButtonMenu(550, 400, 150, 50, "3 Categoria");
        add(jbCategoria3);

        jbTodos = new JButtonMenu(550, 400, 150, 50, "Todos");
        add(jbTodos);

        ControladorJuego cpp = new ControladorJuego();
        cpp.ControladorPuntuacionProfesor(this);
        jbatras.addActionListener(cpp);
        jbCategoria1.addActionListener(cpp);
        jbCategoria2.addActionListener(cpp);
        jbCategoria3.addActionListener(cpp);
        jbTodos.addActionListener(cpp);

        ///////////////////////////////////////////////////////
    }

}
