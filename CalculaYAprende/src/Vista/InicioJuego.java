package Vista;

import Controlador.ControladorJuego;
import Modelo.ModeloJuego;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class InicioJuego extends JFrame {

    public JButtonMenu jbOpcA, jbOpcB, jbOpcC, jbOpcD, jbAyuda;
    public CategoriasEstu c;
    public String matep[];
    public String mater[][];
    public int maxjuego, minjuego = 1;
    public int num;
    JComboBox<String> jcTema;

    public InicioJuego(CategoriasEstu c) {

        super(" Categorias ");
        this.c = c;

        setSize(750, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        ModeloJuego mmpe = new ModeloJuego();
        mmpe.ModeloMaxPregEstu(this);
        num = 0 + (int) Math.floor(Math.random() * (maxjuego - minjuego + 1) + (minjuego));
        ModeloJuego mij = new ModeloJuego();

        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        JLabelTitulo1 jlNom = new JLabelTitulo1(this.getWidth(), "<html>" + c.mp.bd.Spregunta + "</html>");
        add(jlNom);

        jbAyuda = new JButtonMenu(600, 0, 150, 50, "Explicacion");
        add(jbAyuda);

        jbOpcA = new JButtonMenu(100, 300, 200, 50, "A: " + c.mp.bd.SopcionA);
        add(jbOpcA);
        //  System.out.println("AINI: "+ c.mp.bd.SopcionA);
        jbOpcB = new JButtonMenu(450, 300, 200, 50, "B: " + c.mp.bd.SopcionB);
        add(jbOpcB);
        jbOpcC = new JButtonMenu(100, 400, 200, 50, "C: " + c.mp.bd.SopcionC);
        add(jbOpcC);
        jbOpcD = new JButtonMenu(450, 400, 200, 50, "D: " + c.mp.bd.SopcionD);
        add(jbOpcD);


        ControladorJuego cij = new ControladorJuego();
        cij.ControladorInicioJuego(this);
        jbOpcA.addActionListener(cij);
        jbOpcB.addActionListener(cij);
        jbOpcC.addActionListener(cij);
        jbOpcD.addActionListener(cij);
        jbAyuda.addActionListener(cij);

        jcTema = new JComboBox<String>();
        jcTema.setToolTipText("Cambia el look de Gamifi");
        jcTema.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jcTema.addItem("Escoge tu look favorito");
        jcTema.addItem("Aero");
        jcTema.addItem("Aluminium");
        jcTema.addItem("Fast");
        jcTema.addItem("McWin");

        jcTema.setBounds(450, 0, 150, 25);
        jcTema.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                evento_jcTema();
            }
        });
        add(jcTema);
    }

    protected void aplicarLook(String look) {
        try {
            if (look.equals("Aluminium")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
            }
            if (look.equals("Aero")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            }
            if (look.equals("McWin")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
            }
            if (look.equals("Fast")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
            }

            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al tratar de cargar el paquete 'com.jtattoo'.\n\n"
                    + "Se tomara el LookAndFeel por defecto.", "Error de paquete", JOptionPane.WARNING_MESSAGE);

            JFrame.setDefaultLookAndFeelDecorated(true);
        }
    }

    protected void evento_jcTema() {
        String look = jcTema.getSelectedItem().toString();
        System.out.println(look);
        if (!look.equals("Escoge tu look favorito")) {
            aplicarLook(look);
        }
    }

}
