package Vista;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class JLabelExpliObli extends JLabel implements FormatoFuente {

    public JLabelExpliObli(int ancho, String texto) {

        setBounds(50, 250, 600, 100);
        setText(texto);
        setForeground(colorfuenteTitulo);
        setOpaque(true);
        setBackground(Color.white);
        setFont(new Font(fuenteTitulo, Font.BOLD, tamfuenteTitulo));
        setBorder(new EtchedBorder());
        setHorizontalAlignment(SwingConstants.CENTER);

    }
}
