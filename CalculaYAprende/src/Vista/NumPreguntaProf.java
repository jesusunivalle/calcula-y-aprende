/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorProfesor;
import java.awt.HeadlessException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class NumPreguntaProf extends JFrame {

    public JTextField jtnumPreg;
    public JButtonMenu jbsiguiente, jbatras;
    public VisuPreguntasProf vp;
    public int Numpreg = 0, puto;

    public NumPreguntaProf(VisuPreguntasProf vp) {

        this.vp = vp;
        setSize(450, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "<html>" + "Digite el numero de la pregunta a modificar" + "</html>"/*this.getTile*/);
        jlt.setBounds(50, 50, 350, 50);
        add(jlt);

        jbsiguiente = new JButtonMenu(300, 200, 100, 50, "Sig");
        add(jbsiguiente);

        jbatras = new JButtonMenu(50, 200, 100, 50, "Atras");
        add(jbatras);

        jtnumPreg = new JTextField();
        jtnumPreg.setBounds(150, 125, 150, 30);
        add(jtnumPreg);

        ControladorProfesor cnpp = new ControladorProfesor();
        cnpp.ControladorNumPregProf(this);
        jbsiguiente.addActionListener(cnpp);
        jbatras.addActionListener(cnpp);
    }

}
