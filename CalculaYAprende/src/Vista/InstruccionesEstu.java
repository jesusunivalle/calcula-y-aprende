/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorEstudiante;
import javax.swing.JFrame;

public class InstruccionesEstu extends JFrame {

    public JButtonMenu jbatras;
    public CrearBD bd;

    public InstruccionesEstu(CrearBD bd) {
        this.bd = bd;
        setSize(700, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "<html>" + "Instrucciones" + "</html>"/*this.getTile*/);
        jlt.setBounds(250, 50, 200, 50);
        add(jlt);
        JLabelTitulo jlt1 = new JLabelTitulo(this.getWidth(), "<html>" + "Este juego tiene como función base, desarrollar el aprendizaje de niños y jovenes en cuestion de las matematicas.<br>\n"
                + "Dentro de este juego podras encontrar problemas matematicos  de diferente tipo de nivel de dificultad.<br>\n"
                + "A medida que vas jugando vas aprendiendo, sin importar que falles o respondas correctamente.<br>\n"
                + "Podras encontrar desde una suma y resta hasta un minimo comun divisor (MCD) y poligonos.<br>\n"
                + "La jugabilidad consta de preguntas o problemas, los cuales tendran cuatro (4) opciones de respuesta y pequeñas pistas que iran ayudandote segun la pregunta en la que estés, entre mas respuestas correctas tengas mayor sera tu puntuación.<br>\n"
                + "¡VAMOS A JUGAR!" + "</html>"/*this.getTile*/);
        jlt1.setBounds(50, 150, 600, 300);
        add(jlt1);

        jbatras = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbatras);

        ControladorEstudiante cinst = new ControladorEstudiante();
        cinst.ControladorInstruccionesEstu(this);
        jbatras.addActionListener(cinst);

    }
}
