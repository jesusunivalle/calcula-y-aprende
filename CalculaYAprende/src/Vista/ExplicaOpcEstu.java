package Vista;


import Controlador.ControladorJuego;
import static Vista.FormatoFuente.fuenteBotonMenu;
import static Vista.FormatoFuente.tamfuenteBotonMenu;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ExplicaOpcEstu extends JFrame {

    public JButton jbOpcOpc;

    public InicioJuego ij;

    public ExplicaOpcEstu(InicioJuego ij) {

        this.ij = ij;

        setSize(700, 300);

        setLocationRelativeTo(null);
        //setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  Para que no se cierre
        setLayout(null);
        setResizable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        jbOpcOpc = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbOpcOpc);

        ControladorJuego ceo = new ControladorJuego();
        ceo.ControladorExplicaOpcEstu(this);
        jbOpcOpc.addActionListener(ceo);

        JLabelTitulo jltitu = new JLabelTitulo(this.getWidth(), "<html>" + "Explicacion" + "</html>");
        jltitu.setBounds(200, 50, 300, 50);
        add(jltitu);

        JLabelTitulo jlexplica = new JLabelTitulo(this.getWidth(), "<html>" + ij.c.mp.bd.SExplicaOpc + "</html>");
        jlexplica.setBounds(50, 150, 600, 100);
        add(jlexplica);

    }

}
