/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;

public class JButtonMenu extends JButton implements FormatoFuente {

    public JButtonMenu(int x, int y, int ancho, int alto, String texto) {

        setBounds(x, y, ancho, alto);//pasa acomodar el boton
        setText(texto);
        setBackground(Color.red);//color fondo
        setForeground(colorfuenteboton);//color letra
        setFont(new Font(fuenteBotonMenu, Font.PLAIN, tamfuenteBotonMenu));
    }

}
