/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorProfesor;
import Modelo.ModeloProfe;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ModificarPreguntaProf extends JFrame {

    public JButtonMenu jbAtras, jbsiguiente;
    public JTextField jtpreg, jtrespuA, jtrespuB, jtrespuC, jtrespuD, jtrespuCorre, jtexplicaOpc, jtexplicaObli;
    public NumPreguntaProf np;
    public int puto = 0;

    public ModificarPreguntaProf(NumPreguntaProf np) {
        this.np = np;
        setSize(550, 750);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();
        ModeloProfe mppp = new ModeloProfe();
        mppp.ModeloPonerPregPro(this);
        setVisible(true);

    }

    private void CrearGui() {

        jbsiguiente = new JButtonMenu(400, 650, 100, 50, "sig");
        add(jbsiguiente);

        jbAtras = new JButtonMenu(50, 650, 100, 50, "Atras");
        add(jbAtras);

        JLabel jlpreg = new JLabel("Pregunta");
        jlpreg.setBounds(230, 25, 150, 20);
        add(jlpreg);
        jtpreg = new JTextField();
        jtpreg.setBounds(50, 50, 450, 30);
        add(jtpreg);

        JLabel jrespuA = new JLabel("Opcion A");
        jrespuA.setBounds(50, 125, 150, 20);
        add(jrespuA);
        jtrespuA = new JTextField();
        jtrespuA.setBounds(50, 150, 200, 30);
        add(jtrespuA);

        JLabel jrespuB = new JLabel("Opcion B");
        jrespuB.setBounds(300, 125, 150, 20);
        add(jrespuB);
        jtrespuB = new JTextField();
        jtrespuB.setBounds(300, 150, 200, 30);
        add(jtrespuB);

        JLabel jrespuC = new JLabel("Opcion C");
        jrespuC.setBounds(50, 225, 150, 20);
        add(jrespuC);
        jtrespuC = new JTextField();
        jtrespuC.setBounds(50, 250, 200, 30);
        add(jtrespuC);

        JLabel jrespuD = new JLabel("Opcion D");
        jrespuD.setBounds(300, 225, 150, 20);
        add(jrespuD);
        jtrespuD = new JTextField();
        jtrespuD.setBounds(300, 250, 200, 30);
        add(jtrespuD);

        JLabel jlrespuCorre = new JLabel("Respuesta correcta");
        jlrespuCorre.setBounds(200, 325, 200, 20);
        add(jlrespuCorre);
        jtrespuCorre = new JTextField();
        jtrespuCorre.setBounds(150, 350, 250, 30);
        add(jtrespuCorre);

        JLabel jlexplicaOpc = new JLabel("Explicacion opcional");
        jlexplicaOpc.setBounds(200, 425, 200, 20);
        add(jlexplicaOpc);
        jtexplicaOpc = new JTextField();
        jtexplicaOpc.setBounds(50, 450, 450, 30);
        add(jtexplicaOpc);

        JLabel jlexplicaObli = new JLabel("Explicacion obligatoria");
        jlexplicaObli.setBounds(200, 525, 200, 20);
        add(jlexplicaObli);
        jtexplicaObli = new JTextField();
        jtexplicaObli.setBounds(50, 550, 450, 30);
        add(jtexplicaObli);

        ControladorProfesor cmpp = new ControladorProfesor();
        cmpp.ControladorModiPregPRo(this);
        jbAtras.addActionListener(cmpp);
        jbsiguiente.addActionListener(cmpp);
    }

}
