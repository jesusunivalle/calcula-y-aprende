/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;


import Controlador.ControladorJuego;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Logueo extends JFrame {

    public JTextField jtUsu, jtContra;
    public JButtonMenu jbSiguiente, jbatras;
    public CrearLogin cl;
    public int nivel = 0;
    public JLabel aviso;
    public int puto = 1;

    public Logueo(CrearLogin cl) {

        this.cl = cl;

        setSize(650, 250);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        crearGui();

        setVisible(true);

    }

    private void crearGui() {

        jbSiguiente = new JButtonMenu(500, 50, 100, 50, "Sig");
        add(jbSiguiente);
        jbatras = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbatras);

        JLabel jlApe = new JLabel("Correo:");
        jlApe.setBounds(200, 25, 150, 20);
        add(jlApe);
        jtUsu = new JTextField();
        jtUsu.setBounds(200, 50, 250, 30);
        add(jtUsu);

        JLabel jlNom = new JLabel("Codigo/Cedula:");
        jlNom.setBounds(200, 125, 150, 20);
        add(jlNom);
        jtContra = new JTextField();
        jtContra.setBounds(200, 150, 250, 30);
        add(jtContra);

        ControladorJuego clog = new ControladorJuego();
        clog.ControladorLogueo(this);
        jbSiguiente.addActionListener(clog);
        jbatras.addActionListener(clog);

    }

}
