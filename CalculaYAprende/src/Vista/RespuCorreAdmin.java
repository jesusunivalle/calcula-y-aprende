package Vista;

import Controlador.ControladorJuego;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.HeadlessException;
import javax.swing.JButton;
import javax.swing.JButton;
import javax.swing.JFrame;

public class RespuCorreAdmin extends JFrame {

    public JButton jbResCorre;

    InicioJuegoAdmin IJa;

    public RespuCorreAdmin(InicioJuegoAdmin IJa) {

        this.IJa = IJa;

        setSize(700, 250);

        setLocationRelativeTo(null);
        //setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  Para que no se cierre
        setLayout(null);
        setResizable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        jbResCorre = new JButtonMenu(550, 50, 100, 50, "Sig");
        add(jbResCorre);

        ControladorJuego crca = new ControladorJuego();
        crca.ControladorResCorreAdmin(this, IJa, IJa.ca.ma.cda);
        jbResCorre.addActionListener(crca);

        JLabelTitulo jltitu = new JLabelTitulo(this.getWidth(), "<html>" + "Respuesta Correcta" + "</html>");
        jltitu.setBounds(200, 50, 300, 50);
        add(jltitu);

        JLabelTitulo jlexplica = new JLabelTitulo(this.getWidth(), "<html>" + "Puntuacion: " + IJa.ca.ma.cda.puntuTemp + "</html>");
        jlexplica.setBounds(150, 150, 400, 50);
        add(jlexplica);

    }

}
