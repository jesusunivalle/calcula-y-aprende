package Vista;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class JLabelTitulo1 extends JLabel implements FormatoFuente {

    public JLabelTitulo1(int ancho, String texto) {

        setBounds(100, 50, 550, 200);
        setText(texto);
        setForeground(colorfuenteTitulo);
        setOpaque(true);
        setBackground(Color.white);
        setFont(new Font(fuenteTitulo, Font.BOLD, tamfuenteTitulo));
        setBorder(new EtchedBorder());
        setHorizontalAlignment(SwingConstants.CENTER);

    }
}
