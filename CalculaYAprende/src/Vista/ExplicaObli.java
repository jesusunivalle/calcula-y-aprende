package Vista;


import Controlador.ControladorJuego;
import static Vista.FormatoFuente.fuenteBotonMenu;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ExplicaObli extends JFrame {

    public JButton jbOblisiguiente;

    InicioJuego IJ;

    public ExplicaObli(InicioJuego IJ) {

        this.IJ = IJ;

        setSize(700, 400);

        setLocationRelativeTo(null);
        //setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  Para que no se cierre
        setLayout(null);
        setResizable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        jbOblisiguiente = new JButtonMenu(550, 50, 100, 50, "Sig");
        add(jbOblisiguiente);

        ControladorJuego ceo = new ControladorJuego();
        ceo.ControladorExplicaObliEstu(this, IJ, IJ.c.mp.bd);
        jbOblisiguiente.addActionListener(ceo);

        JLabelTitulo jltitu = new JLabelTitulo(this.getWidth(), "<html>" + "Respuesta Incorrecta" + "</html>");
        jltitu.setBounds(185, 50, 350, 50);
        add(jltitu);

        JLabelExpliObli jlExplicacion = new JLabelExpliObli(this.getWidth(), "<html>" + IJ.c.mp.bd.SExplicaObli + "</html>");
        add(jlExplicacion);

        JLabelPuntua jlpuntua = new JLabelPuntua(this.getWidth(), "<html>" + "Puntuacion: " + IJ.c.mp.bd.puntuTemp + "</html>");
        add(jlpuntua);
    }
}
