/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorAdmin;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class VisuPreguntasAdmin extends JFrame {

    public JButtonMenu jbatras, jbMostrar, jbsiguiente;
    public JTable tabla;
    public DefaultTableModel modelo;
    public EligeCategoriaModiAdmin ecma;

    public VisuPreguntasAdmin(EligeCategoriaModiAdmin ecma) {
        this.ecma = ecma;

        setSize(650, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Preguntas"/*this.getTile*/);
        add(jlt);

        ////////////////////////////////////////////////////////////////////
        modelo = new DefaultTableModel() {

            public Class getColumnClass(int colum) {

                if (colum == 2) {

                    return Boolean.class;

                }
                return Object.class;
            }

            public boolean isCellEditable(int fila, int colum) {

                if (colum == 2) {

                    return true;

                }
                return false;
            }
        };
        Dimension n = new Dimension(480, 200);

        tabla = new JTable(modelo);

        modelo.addColumn("N° Pregunta");
        modelo.addColumn("Pregunta");

        JScrollPane js = new JScrollPane(tabla, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        c.add(js);

        jbatras = new JButtonMenu(50, 500, 150, 50, "Atras");
        add(jbatras);

        jbMostrar = new JButtonMenu(200, 500, 150, 50, "Mostrar");
        add(jbMostrar);

        jbsiguiente = new JButtonMenu(400, 500, 150, 50, "Siguiente");
        add(jbsiguiente);

        ControladorAdmin cvpa = new ControladorAdmin();
        cvpa.ControladorVisuPreguntasAdmin(this);
        jbatras.addActionListener(cvpa);
        jbsiguiente.addActionListener(cvpa);
        jbMostrar.addActionListener(cvpa);

        ///////////////////////////////////////////////////////
    }

}
