/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorProfesor;
import javax.swing.JFrame;

public class CrearModificar extends JFrame {

    public JButtonMenu jbCrear, jbModificar, jbAtras;
    public MenuProfesor menup;

    public CrearModificar(MenuProfesor menup) {
        this.menup = menup;
        setSize(450, 250);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        JLabelTitulo jltitu = new JLabelTitulo(this.getWidth(), "Que deseas hacer: ");
        jltitu.setBounds(125, 10, 200, 40);
        add(jltitu);

        jbCrear = new JButtonMenu(50, 75, 150, 50, "Crear");
        add(jbCrear);

        jbModificar = new JButtonMenu(250, 75, 150, 50, "Modificar");
        add(jbModificar);

        jbAtras = new JButtonMenu(150, 150, 150, 50, "Atras");
        add(jbAtras);

        ControladorProfesor ccmp = new ControladorProfesor();
        ccmp.ControladorCrearModiProfe(this);
        jbCrear.addActionListener(ccmp);
        jbModificar.addActionListener(ccmp);
        jbAtras.addActionListener(ccmp);
    }

}
