package Vista;

import Controlador.ControladorEstudiante;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class CrearEstudiante extends JFrame {

    public JTextField jtNom, jtApe, jtCodEstu, jtCorreoEstu, jtCodPlan, jtNomPlan, jtFechaNaci;
    public JButtonMenu jbSiguiente;
    public TipUsu tu;
    public String e = "", f = "";
    public int puto = 0;

    public CrearEstudiante(TipUsu tu) {

        this.tu = tu;

        setSize(500, 650);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);

        crearGui();

        setVisible(true);

    }

    private void crearGui() {

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Estudiante");
        jlt.setBounds(175, 50, 150, 50);
        add(jlt);

        jbSiguiente = new JButtonMenu(350, 550, 100, 50, "Sig");
        add(jbSiguiente);

        JLabel jlNom = new JLabel("Nombre");
        jlNom.setBounds(50, 125, 50, 20);
        add(jlNom);
        jtNom = new JTextField();
        jtNom.setBounds(50, 150, 150, 30);
        add(jtNom);

        jtNom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtNomKeyTyped(evt);
            }
        });

        JLabel jlApe = new JLabel("Apellido");
        jlApe.setBounds(300, 125, 300, 20);
        add(jlApe);
        jtApe = new JTextField();
        jtApe.setBounds(300, 150, 150, 30);
        add(jtApe);

        JLabel jlCodEstu = new JLabel("Codigo estudiantil");
        jlCodEstu.setBounds(50, 225, 150, 20);
        add(jlCodEstu);
        jtCodEstu = new JTextField();
        jtCodEstu.setBounds(50, 250, 150, 30);
        add(jtCodEstu);

        JLabel jlCorreoEstu = new JLabel("Correo estudiantil");
        jlCorreoEstu.setBounds(300, 225, 150, 20);
        add(jlCorreoEstu);
        jtCorreoEstu = new JTextField();
        jtCorreoEstu.setBounds(300, 250, 150, 30);
        add(jtCorreoEstu);

        JLabel jlCodPlan = new JLabel("Codigo del plan");
        jlCodPlan.setBounds(50, 325, 150, 20);
        add(jlCodPlan);
        jtCodPlan = new JTextField();
        jtCodPlan.setBounds(50, 350, 150, 30);
        add(jtCodPlan);

        jtCodPlan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtCodPlanKeyTyped(evt);
            }
        });

        JLabel jlNomPlan = new JLabel("Nombre del plan");
        jlNomPlan.setBounds(300, 325, 150, 20);
        add(jlNomPlan);
        jtNomPlan = new JTextField();
        jtNomPlan.setBounds(300, 350, 150, 30);
        add(jtNomPlan);

        JLabel jlFechaNaci = new JLabel("Fecha de nacimiento");
        jlFechaNaci.setBounds(50, 425, 150, 20);
        add(jlFechaNaci);
        jtFechaNaci = new JTextField();
        jtFechaNaci.setBounds(50, 450, 150, 30);
        add(jtFechaNaci);

        ControladorEstudiante cce = new ControladorEstudiante();
        cce.ControladorCrearEstudiante(this);
        jbSiguiente.addActionListener(cce);

    }

    private void jtNomKeyTyped(java.awt.event.KeyEvent evt) {

        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();

            evt.consume();

            JOptionPane.showMessageDialog(null, "Ingresar solo letras");
        }

    }

    private void jtCodPlanKeyTyped(java.awt.event.KeyEvent evt) {

        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(null, "Ingresar solo numeros");
        }

    }

}
