package Vista;

import java.awt.Color;

public interface FormatoFuente {

    //atributos para la etiqueta del titulo
    String fuenteTitulo = "Arial";
    int tamfuenteTitulo = 18;
    Color colorfuenteTitulo = Color.black;

    //atributos para los botones de menu
    String fuenteBotonMenu = "Arial";
    int tamfuenteBotonMenu = 15;
    Color colorfuenteboton = Color.white;
}
