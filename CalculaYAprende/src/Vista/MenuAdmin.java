package Vista;

import Controlador.ControladorAdmin;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class MenuAdmin extends JFrame {

    public JButtonMenu jbinicioJu, jbpuntuAdmin, jbvisuJuga, jbcrearModi, jbcerrarsesion, jbInstrucciones;
    public CrearBD cda;
    JComboBox<String> jcTema;
    javax.swing.JLabel jLabel1;

    public MenuAdmin(CrearBD cda) {
        this.cda = cda;
        setSize(500, 550);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/Princi.jpeg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.setBounds(110, 25, 280, 200);
        add(jLabel1);

        jbinicioJu = new JButtonMenu(50, 250, 150, 50, "Inicio juego");
        add(jbinicioJu);

        jbpuntuAdmin = new JButtonMenu(300, 250, 150, 50, "Puntuacion");
        add(jbpuntuAdmin);

        jbvisuJuga = new JButtonMenu(50, 350, 150, 50, "<html>" + "Visualizar<br>Usuarios" + "</html>");
        add(jbvisuJuga);

        jbcrearModi = new JButtonMenu(300, 350, 150, 50, "Crear/Modificar");
        add(jbcrearModi);

        jbcerrarsesion = new JButtonMenu(300, 450, 150, 50, "Cerrar Sesion");
        add(jbcerrarsesion);

        jbInstrucciones = new JButtonMenu(50, 450, 150, 50, "Instrucciones");
        add(jbInstrucciones);

        ControladorAdmin cma = new ControladorAdmin();
        cma.ControladorMenuAdmin(this);
        jbinicioJu.addActionListener(cma);
        jbpuntuAdmin.addActionListener(cma);
        jbcrearModi.addActionListener(cma);
        jbcerrarsesion.addActionListener(cma);
        jbvisuJuga.addActionListener(cma);
        jbInstrucciones.addActionListener(cma);

        jcTema = new JComboBox<String>();
        jcTema.setToolTipText("Cambia el look de Gamifi");
        jcTema.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jcTema.addItem("Escoge tu look favorito");
        jcTema.addItem("Aero");
        jcTema.addItem("Aluminium");
        jcTema.addItem("Fast");
        jcTema.addItem("McWin");

        jcTema.setBounds(350, 0, 150, 25);
        jcTema.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                evento_jcTema();
            }
        });
        add(jcTema);

    }

    protected void aplicarLook(String look) {
        try {
            if (look.equals("Aluminium")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
            }
            if (look.equals("Aero")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            }
            if (look.equals("McWin")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
            }
            if (look.equals("Fast")) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
            }

            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al tratar de cargar el paquete 'com.jtattoo'.\n\n"
                    + "Se tomara el LookAndFeel por defecto.", "Error de paquete", JOptionPane.WARNING_MESSAGE);

            JFrame.setDefaultLookAndFeelDecorated(true);
        }
    }

    protected void evento_jcTema() {
        String look = jcTema.getSelectedItem().toString();
        System.out.println(look);
        if (!look.equals("Escoge tu look favorito")) {
            aplicarLook(look);
        }
    }
}
