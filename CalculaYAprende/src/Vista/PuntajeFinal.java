/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorJuego;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

public class PuntajeFinal extends JFrame {

    public JButtonMenu jbSiguiente;
    public InicioJuego ij;

    public PuntajeFinal(InicioJuego ij) {
        this.ij = ij;
        setSize(500, 350);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  //Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        jbSiguiente = new JButtonMenu(350, 250, 100, 50, "sig");
        add(jbSiguiente);

        ControladorJuego cpf = new ControladorJuego();
        cpf.ControladorPuntajeFinal(this);
        jbSiguiente.addActionListener(cpf);

        JLabelTitulo jltitu = new JLabelTitulo(this.getWidth(), "<html>" + "Puntuacion Final" + "</html>");
        jltitu.setBounds(100, 50, 300, 50);
        add(jltitu);

        JLabelTitulo jlexplica = new JLabelTitulo(this.getWidth(), "<html>" + ij.c.mp.bd.puntuFi + "</html>");
        jlexplica.setBounds(150, 150, 200, 50);
        add(jlexplica);

    }
}
