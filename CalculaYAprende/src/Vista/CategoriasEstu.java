package Vista;


import Controlador.ControladorJuego;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.*;

public class CategoriasEstu extends JFrame {

    //public MenuPrincipal mp;
    public JButtonMenu jbCategoria1, jbCategoria2, jbCategoria3, jbatras;
    public MenuEstudiante mp;
    public int Numcategoria = 0;
    javax.swing.JLabel jLabel1, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6, jLabel7, jLabel8, jLabel9;

    public CategoriasEstu(MenuEstudiante mp) {

        super(" Inicio Juego ");
        this.mp = mp;

        setSize(750, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Para que no se cierre
        setLayout(null);
        setResizable(false);
        CrearGui();

        setVisible(true);

    }

    private void CrearGui() {

        JLabelTitulo jlt = new JLabelTitulo(this.getWidth(), "Categoria Edades"/*this.getTile*/);
        add(jlt);

        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/mas.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.setBounds(350, 150, 66, 50);
        add(jLabel1);
        jLabel2 = new javax.swing.JLabel();
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/masmenos.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");
        jLabel2.setBounds(416, 150, 118, 50);
        add(jLabel2);
        jLabel3 = new javax.swing.JLabel();
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/menos.jpg"))); // NOI18N
        jLabel3.setText("jLabel2");
        jLabel3.setBounds(534, 150, 66, 50);
        add(jLabel3);

        jLabel4 = new javax.swing.JLabel();
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/multi.jpg"))); // NOI18N
        jLabel4.setText("jLabel1");
        jLabel4.setBounds(350, 275, 45, 50);
        add(jLabel4);

        jLabel5 = new javax.swing.JLabel();
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/masmenos.jpg"))); // NOI18N
        jLabel5.setText("jLabel2");
        jLabel5.setBounds(416, 275, 118, 50);
        add(jLabel5);
        jLabel6 = new javax.swing.JLabel();
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/divi.jpg"))); // NOI18N
        jLabel6.setText("jLabel2");
        jLabel6.setBounds(555, 275, 45, 50);
        add(jLabel6);

        jLabel7 = new javax.swing.JLabel();
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/geo.jpg"))); // NOI18N
        jLabel7.setText("jLabel1");
        jLabel7.setBounds(350, 400, 88, 50);
        add(jLabel7);
        jLabel8 = new javax.swing.JLabel();
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/todo.jpg"))); // NOI18N
        jLabel8.setText("jLabel2");
        jLabel8.setBounds(465, 400, 67, 50);
        add(jLabel8);
        jLabel9 = new javax.swing.JLabel();
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/mcm.png"))); // NOI18N
        jLabel9.setText("jLabel2");
        jLabel9.setBounds(550, 400, 50, 50);
        add(jLabel9);

        jbCategoria1 = new JButtonMenu(100, 150, 200, 50, "1 Categoria");
        add(jbCategoria1);

        jbCategoria2 = new JButtonMenu(100, 275, 200, 50, "2 Categoria");
        add(jbCategoria2);

        jbCategoria3 = new JButtonMenu(100, 400, 200, 50, "3 Categoria");
        add(jbCategoria3);

        jbatras = new JButtonMenu(50, 50, 100, 50, "Atras");
        add(jbatras);

        ControladorJuego cc = new ControladorJuego();
        cc.ControladorCategoriasEstu(this);

        jbCategoria1.addActionListener(cc);
        jbCategoria2.addActionListener(cc);
        jbCategoria3.addActionListener(cc);
        jbatras.addActionListener(cc);

    }
}
